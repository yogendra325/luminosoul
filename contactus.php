<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="UTF-8">
	<title>LuminoSoul</title>
	<meta content="" name="keywords">
	<meta content="" name="description">
	<meta content="" name="author">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	
	<link href="css/lightcase.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="fonts/font.css">
	<link rel="stylesheet" type="text/css" href="palyfair_Dispaly/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="amaranth/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="gentium_Basic/stylesheet.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="css/icofont.css" rel="stylesheet">
	<link href="css/animsition.min.css" rel="stylesheet">
	<link href="css/jquery.nstSlider.min.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/swiper.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/animate.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootsnav.css" rel="stylesheet">
	<link href="css/shortcode.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<script src="js/ga.js" type="text/javascript">
	</script>
</head>
<body class="demo-landing-page p-load-animation">
	<div class="style-switcher">
        <div class="switcher-toggle">
			<i class="fa fa-cog" aria-hidden="true"></i>
			<span>Options</span>
		</div>
        
       <div class="style-switcher-container">
            <div class="style-switcher-inner">
               
                <div class="optionpart">
                                <aside class="col-md-12 sidebar">
					<div class="m-left m-right sidebar-widget-wrapper">
						<div class="widget pdt20">
							<form role="search" method="get" class="search-form"> 
								<label>
									<span class="screen-reader-text">Search....</span> 
									<input type="search" class="search-field" placeholder="Search...." value="" name="s" autocomplete="off"> 
								</label>
								<input type="submit" class="search-submit" value="Search">
							</form>
						</div><!-- widget -->
						
						<div class="widget categorie-widget">
							<h3 class="widget-title">Sorting</h3>
							<!-- <ul>
								<li><a href="#">Delhi Public Sschool</a></li>
								<li><a href="#">D.A.V Sschool</a></li>
								<li><a href="#">Greenfield Public School</a></li>
								<li><a href="#">Loreto Convent School</a></li>
							</ul> -->
							<div class="checkbox">
								<label><input type="checkbox" value="">Delhi Public Sschool</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">D.A.V Sschool</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Greenfield Public School</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Loreto Convent School</label>
							</div>
						
						</div><!-- widget -->
						
						<!-- widget -->
						
						<div class="widget categorie-widget">
							<h3 class="widget-title">Medium:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Hindi</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">English</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Board:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">ICSE</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">CBSE</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">OPEN</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">OTHER</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Boarding:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Day school</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Boarding</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Both</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">School type:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Boys</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Grils</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">co-ed</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Nationality:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">International</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">National</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Categories</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Graphic Design</label><span style="padding-left: 5px;">(3)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Web Design</label><span style="padding-left: 5px;">(5)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Photography</label><span style="padding-left: 5px;">(2)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Marketing</label><span style="padding-left: 5px;">(4)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Business Consult</label><span style="padding-left: 5px;">(1)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Domain &amp; Hosting</label><span style="padding-left: 5px;">(3)</span>
							</div>
						</div><!-- widget -->
						
						<div class="widget tag-widget">
							<h3 class="widget-title">TAGS</h3>
							<div class="tagcloud">
								<a href="#">web</a>
								<a href="#">graphic</a>
								<a href="#">design</a>
								<a href="#">marketing</a>
								<a href="#">seo</a>
								<a href="#">logo</a>
							</div>
						</div><!-- widget -->
					</div><!-- sidebar-widget-wrapper -->
				</aside>   
                   
                </div>
            </div>
        </div>
    </div>
	
	<?php include("header.php"); ?>
	<script src="respjs/contactUs.js"></script>
<section class="carousel-new-s" style="background-color: ;">
	<div class="p-caption demo-banner-content-inner js-tilt" style="transform: perspective(1000px) rotateX(0deg) rotateY(0deg); will-change: transform;">			
	<h3 class="text-white"  style="color: #ffffff;font-size: 50px;">NORMAL </h3>
	
	  </div>
	<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="3000" data-pause="">
    <!-- Wrapper for slides -->
    <div class="carousel-inner block ">
      <div class="item active">
        <img src="images/slide-1.JPG" class="slide_img" alt="Los Angeles">
       
                 
      </div>

      <div class="item">
        <img src="images/slide-2.jpg" class="slide_img" alt="Chicago">
       
      </div>
    
      <div class="item">
        <img src="images/slide-3.jpg" class="slide_img" alt="New york">

      </div>
    </div>

    <!-- Left and right controls -->
      <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  </div>


</section>

	<!-- <div class="container-fluid mt_sidebar" style="padding: 0px;">
		<div class="row mainrow">
			

		</div>
		
       
	</div> -->
	<section class="contact">
		<div class="contact-form-area s-padding">
			<div class="container">
				<div class="row">
					<div class="m-left m-right">
						<div class="col-md-5 col-sm-5">
							<div class="contact-row">
								<h3 class="contact-title">ABOUT<span class="light"> BLOG</span></h3>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
								<p>Lorem Ipsum has been the industry's standard  char dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it  make a type specimen book.</p>
								<ul class="social-profile ">
									<li><a class="social-hex medium" href="#">
										<i class="fa fa-facebook"></i>
									</a></li>
									<li><a class="social-hex medium" href="#">
										<i class="fa fa-behance"></i>
									</a></li>
									<li><a class="social-hex medium" href="#">
										<i class="fa fa-dribbble"></i>
									</a></li>
									<li><a class="social-hex medium" href="#">
										<i class="fa fa-pinterest"></i>
									</a></li>
								</ul>
								<!-- social-profile -->
							</div>
							<div class="contact-row">
								<div class="contact-box">
									<h4 class="title">Email :</h4>
									<span>omendrapratap25@gmail.com</span>
									<span>skillscandy@gmail.com</span>
								</div><!-- contact-box -->
								<div class="contact-box">
									<h4 class="title">PHONE :</h4>
									<span>+919454332797 </span>
									<span>+917017734526</span>
								</div><!-- contact-box -->
								<div class="contact-box">
									<h4 class="title">ADDRESS :</h4>
									<span>B Block, sector 62 NOIDA </span>
									<span>INDIA</span>
								</div><!-- contact-box -->
							</div><!-- contact-row -->
						</div>
						<div class="col-md-7 col-sm-7">
							<div class="contact-form-wrapper m-right m-left">
			                   
			                <form action="" method="post" id="contactform">
    							<div class="row">
    								
  									<div class="col-sm-12">

		<div class="input-group">
		<span class="input-group-addon"><i class="fa fa-user fa fa-yellow" aria-hidden="true"></i></span>
		<input type="text" class="" name="fname" placeholder="First Name" required >
		</div>

					<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user fa fa-yellow" aria-hidden="true"></i></span>
					<input type="text" class="" name="lname" placeholder="Last Name" required >
					</div>


								
    									
    								</div>
    								
    								<div class="col-sm-12">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-phone fa-yellow" aria-hidden="true"></i></span>
									

									<input type="text" class="" name="phone" placeholder="Phone" required >
								</div>
    									
    								</div>
    								<div class="col-sm-12">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa  fa-envelope fa-yellow" aria-hidden="true"></i></span>
									<input type="email" class="" name="email" placeholder="email" required >
								</div>
    									
    								</div>
    								
    								<div class="col-sm-12">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-comments fa fa-yellow" aria-hidden="true"></i></span>
									<textarea row="2" id="subject" name="subject" placeholder="Write something.." style="margin: 0;color: #000" required></textarea>
								</div>
    									
    								</div>
    								
    								<div class="col-sm-12" style="margin-top: 20px;">
									<div id="contactMSG"></div>
    									<button style="" class="btn btn1">Submit</button>
    								</div>
    								
    							</div>
	
  							</form>
			               </div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- contact-form-area -->
		<div class="map-acco-wrapper">
			<div class="panel-group map-accordion" id="accordion" role="tablist" aria-multiselectable="true">
		    	<div class="panel panel-default">
		    	    <div class="panel-heading bg-blue text-center" role="tab" id="headingOne">
		    	      	<h4 class="panel-title">
		    	        	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq1" aria-expanded="false" aria-controls="faq1" class="collapsed">
		    	          		LOCATE US <strong>MAP</strong>
		    	          		<span class="acco-collapse"><i class="fa fa-arrow-down fa-22"></i></span>
		    	        	</a>
		    	      	</h4>
		    	    </div><!-- panel-heading -->
		    	    <div id="faq1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
		    	      	<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14011.171323662391!2d77.0473905!3d28.6059912!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc15d793aacb72697!2sDelhi+Public+School!5e0!3m2!1sen!2sin!4v1523018963347" width="100%" height="430" frameborder="0" style="border:0" allowfullscreen></iframe>
		    	    </div><!-- panel-collapse -->
		    	</div><!-- panel -->
		    </div><!-- map-accordion -->
		</div><!-- map-acco-wrapper -->
	</section>

	<?php include("footer.php"); ?>
<a href="#" class="scroll-top"><i class="fa fa-arrow-up"></i></a>
	<script src="js/jquery-2.2.3.min.js">
	</script>
	<script src="js/bootstrap.min.js">
	</script> 
	<script src="js/waypoints.min.js">
	</script>
	<script src="js/jquery.easing.1.3.js">
	</script> 
	<script src="js/validator.min.js">
	</script>
	<script src="js/owl.carousel.js">
	</script> 
	<script src="js/owl.carousel2.thumbs.js">
	</script>
	<script src="js/jquery.nav.js">
	</script> 
	<script src="js/jquery.stellar.min.js">
	</script>
	<script src="js/wow.min.js">
	</script> 
	<script src="js/lightcase.js">
	</script>
	<script src="js/scrolloverflow.min.js">
	</script> 
	<script src="js/smooth-scroll.min.js">
	</script>
	<script src="js/jquery.fullpage.min.js">
	</script> 
	<script src="js/jquery.events.touch.js">
	</script>
	<script src="js/jquery.infinitescroll.min.js">
	</script> 
	<script src="js/jquery.lazyload.min.js">
	</script>
	<script src="js/swiper.min.js">
	</script> 
	<script src="js/parallax.min.js">
	</script>
	<script src="js/masonry.pkgd.min.js">
	</script> 
	<script src="js/shuffle.min.js">
	</script>
	<script src="js/animsition.min.js">
	</script> 
	<script src="js/swiper.min.js">
	</script>
	<script src="js/jquery.nstSlider.js">
	</script> 
	<script src="js/jquery.countdown.min.js">
	</script>
	<script src="js/jquery.counterup.min.js">
	</script> 
	<script src="js/bootsnav.js">
	</script>
	<script src="js/tilt.jquery.min.js">
	</script> 
	<script src="js/custom.js">
	</script>


	<script type="text/javascript">
	var infinitymanualmesonary=$(".infinityselctor-manaul-mesonary");infinitymanualmesonary.infinitescroll({navSelector:".infinity-manaul-links",nextSelector:".infinity-manaul-links a:first",itemSelector:".infinity-item-manaul",loading:{msgText:"Loading more posts...",finishedMsg:"Sorry, no more posts.",},errorCallback:function(){$(".post-load").css("display","none")}},function(a){var b=$(a).css("opacity",0);b.imagesLoaded(function(){b.animate({opacity:1});infinitymanualmesonary.masonry("appended",b,true)})});$(window).unbind(".infscr");$(".post-load").click(function(){infinitymanualmesonary.infinitescroll("retrieve");return false});
	</script> 
	<script type="text/javascript">
	$(".js-tilt").tilt({maxTilt:20,perspective:1000,easing:"cubic-bezier(.03,.98,.52,.99)",scale:1,speed:300,transition:true,axis:null,reset:true,glare:false,maxGlare:1,});
	</script>
	
	<script>
	$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
	<script type="text/javascript">
		$(document).ready(function(){

			$("#1").show();
            $("#2").hide();
            $("#3").hide();
       
        $("#timeline").click(function(){
            $("#1").show();
            $("#2").hide();
            $("#3").hide();
        });

        $("#description").click(function(){
        	$("#1").hide();
            $("#2").show();
            $("#3").hide();
        });
        $("#details").click(function(){
        	$("#1").hide();
            $("#2").hide();
            $("#3").show();
        });

    });
	</script>
	<script>
$(document).ready(function() {
$(".tablinks").click(function () {
    $(".tablinks").removeClass("active");
    // $(".tab").addClass("active"); // instead of this do the below 
    $(this).addClass("active");   
});
});
</script>
</body>
</html>
