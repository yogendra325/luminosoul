<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="UTF-8">
	<title>LuminoSoul</title>
	<meta content="" name="keywords">
	<meta content="" name="description">
	<meta content="" name="author">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	
	<link href="css/lightcase.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="fonts/font.css">
	<link rel="stylesheet" type="text/css" href="palyfair_Dispaly/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="amaranth/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="gentium_Basic/stylesheet.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="css/icofont.css" rel="stylesheet">
	<link href="css/animsition.min.css" rel="stylesheet">
	<link href="css/jquery.nstSlider.min.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/swiper.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/animate.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootsnav.css" rel="stylesheet">
	<link href="css/shortcode.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<script src="js/ga.js" type="text/javascript">
	</script>
</head>
<body class="demo-landing-page p-load-animation">
	<div class="style-switcher">
        <div class="switcher-toggle">
			<i class="fa fa-cog" aria-hidden="true"></i>
			<span>Options</span>
		</div>
        
       <div class="style-switcher-container">
            <div class="style-switcher-inner">
               
                <div class="optionpart">
                <aside class="col-md-12 sidebar">
					<div class="m-left m-right sidebar-widget-wrapper">
						<div class="widget pdt20">
							<form role="search" method="get" class="search-form"> 
								<label>
									<span class="screen-reader-text">Search....</span> 
									<input type="search" class="search-field" placeholder="Search...." value="" name="s" autocomplete="off"> 
								</label>
								<input type="submit" class="search-submit" value="Search">
							</form>
						</div><!-- widget -->
						
						<div class="widget categorie-widget">
							<h3 class="widget-title">Sorting</h3>
							<!-- <ul>
								<li><a href="#">Delhi Public Sschool</a></li>
								<li><a href="#">D.A.V Sschool</a></li>
								<li><a href="#">Greenfield Public School</a></li>
								<li><a href="#">Loreto Convent School</a></li>
							</ul> -->
							<div class="checkbox">
								<label><input type="checkbox" value="">Delhi Public Sschool</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">D.A.V Sschool</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Greenfield Public School</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Loreto Convent School</label>
							</div>
						
						</div><!-- widget -->
						
						<!-- widget -->
						
						<div class="widget categorie-widget">
							<h3 class="widget-title">Medium:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Hindi</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">English</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Board:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">ICSE</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">CBSE</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">OPEN</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">OTHER</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Boarding:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Day school</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Boarding</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Both</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">School type:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Boys</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Grils</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">co-ed</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Nationality:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">International</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">National</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Categories</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Graphic Design</label><span style="padding-left: 5px;">(3)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Web Design</label><span style="padding-left: 5px;">(5)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Photography</label><span style="padding-left: 5px;">(2)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Marketing</label><span style="padding-left: 5px;">(4)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Business Consult</label><span style="padding-left: 5px;">(1)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Domain &amp; Hosting</label><span style="padding-left: 5px;">(3)</span>
							</div>
						</div><!-- widget -->
						
						<div class="widget tag-widget">
							<h3 class="widget-title">TAGS</h3>
							<div class="tagcloud">
								<a href="#">web</a>
								<a href="#">graphic</a>
								<a href="#">design</a>
								<a href="#">marketing</a>
								<a href="#">seo</a>
								<a href="#">logo</a>
							</div>
						</div><!-- widget -->
					</div><!-- sidebar-widget-wrapper -->
				</aside>
                   
                   
                </div>
            </div>
        </div>
    </div>
	
	<?php include("header.php"); ?>

<section class="carousel-new-s" style="background-color: ;">
	<div class="p-caption demo-banner-content-inner js-tilt" style="transform: perspective(1000px) rotateX(0deg) rotateY(0deg); will-change: transform;">			
	<h3 class="text-white"  style="color: #ffffff;font-size: 50px;">NORMAL </h3>
	
	  </div>
	<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="3000" data-pause="">
    
    <!-- Wrapper for slides -->
    <div class="carousel-inner block ">
      <div class="item active">
        <img src="images/slide-1.JPG" class="slide_img" alt="Los Angeles">
       
                 
      </div>

      <div class="item">
        <img src="images/slide-2.jpg" class="slide_img" alt="Chicago">
       
      </div>
    
      <div class="item">
        <img src="images/slide-3.jpg" class="slide_img" alt="New york">

      </div>
    </div>

    <!-- Left and right controls -->
      <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  </div>


</section>

	<div class="container-fluid mt_sidebar" style="padding: 0px;">
		<div class="row mainrow pl0">
			<!-- <div class="col-sm-1"></div> -->
			<div class="col-sm-10 pl0">
				<div class="e-main-content-area">
                <section class="portfolio-about-me bg-sky-blue pl70 pdr30">
                    <div class="m-left m-right column-align-middle">
                        <div class="col-md-6 col-sm-6">
                            <div class="person-info">
                                <div class="text-container-title">
                                    <h2 class="c-blue">Robert <span class="light">Pattinson</span></h2>
                                    <h4 class="c-blue"><span class="light">FOUENDER</span>/CEO</h4>
                                </div><!-- text-container-title -->
                                <blockquote>Lorem Ipsum is simply dummy text of printing and type ting inotry Lorem standard dummy text.</blockquote>
                                <p>ever since the 1500s, when an unknown printer took a galley of type and scibble it to make a type specimen book. vi centurie but also the leap into electronic to typeseting, remaining essential changed. It wasti popuised in the 1960s which onthe sheet like Lorem ton Ipsum.</p>
                                <a class="btn" href="#">My Projects</a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="person-img">
                                <img class="img-responsive" src="images/img46.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </section>
                <section class="skills s-padding pl70 pdr30">
                    <div class="text-container-title">
                        <h2 class="c-blue"><span class="light">My Skills &amp; Experience</span></h2>
                        <p>There are many variations of passages of Lorem Ipsum available but <br>alteration in some form by injected humoures</p>
                    </div>
                    <div class="col-md-2 col-sm-3">
                        <div class="excite-p-bar-container">
                            <div class="excite-p-bar bar-circle c-blue" data-progress-bar-style="circle" data-stroke-width="10" data-trail-width="10" data-progress-percent="0.95" data-duration="1000" data-display-value="true" data-trail-color="#ff712b" data-id="1" style="position: relative;">
                                <label class="excite-p-bar-label">Photoshop</label>
                            <svg viewBox="0 0 100 100"><path d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90" stroke="#ff712b" stroke-width="10" fill-opacity="0"></path><path d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90" stroke="" stroke-width="10" fill-opacity="0" style="stroke-dasharray: 282.783, 282.783; stroke-dashoffset: 14.1392;"></path></svg><div class="progressbar-text" style="color: rgb(51, 101, 138);">95%</div></div><!-- excite-p-bar -->
                        </div><!-- excite-p-bar-container -->
                    </div>
                    <div class="col-md-2 col-sm-3">
                        <div class="excite-p-bar-container">
                            <div class="excite-p-bar bar-circle c-blue" data-progress-bar-style="circle" data-stroke-width="10" data-trail-width="10" data-progress-percent="0.85" data-duration="1100" data-display-value="true" data-trail-color="#ff712b" data-id="2" style="position: relative;">
                                <label class="excite-p-bar-label">Illustrator</label>
                            <svg viewBox="0 0 100 100"><path d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90" stroke="#ff712b" stroke-width="10" fill-opacity="0"></path><path d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90" stroke="" stroke-width="10" fill-opacity="0" style="stroke-dasharray: 282.783, 282.783; stroke-dashoffset: 42.4175;"></path></svg><div class="progressbar-text" style="color: rgb(51, 101, 138);">85%</div></div><!-- excite-p-bar -->
                        </div><!-- excite-p-bar-container -->
                    </div> 
                    <div class="col-md-2 col-sm-3">
                        <div class="excite-p-bar-container">
                            <div class="excite-p-bar bar-circle c-blue" data-progress-bar-style="circle" data-stroke-width="10" data-trail-width="10" data-progress-percent="0.50" data-duration="1200" data-display-value="true" data-trail-color="#ff712b" data-id="3" style="position: relative;">
                                <label class="excite-p-bar-label">Indesign</label>
                            <svg viewBox="0 0 100 100"><path d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90" stroke="#ff712b" stroke-width="10" fill-opacity="0"></path><path d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90" stroke="" stroke-width="10" fill-opacity="0" style="stroke-dasharray: 282.783, 282.783; stroke-dashoffset: 141.392;"></path></svg><div class="progressbar-text" style="color: rgb(51, 101, 138);">50%</div></div><!-- excite-p-bar -->
                        </div><!-- excite-p-bar-container -->
                    </div>
                    <div class="col-md-2 col-sm-3">
                        <div class="excite-p-bar-container">
                            <div class="excite-p-bar bar-circle c-blue" data-progress-bar-style="circle" data-stroke-width="10" data-trail-width="10" data-progress-percent="0.65" data-duration="1300" data-display-value="true" data-trail-color="#ff712b" data-id="4" style="position: relative;">
                                <label class="excite-p-bar-label">principle</label>
                            <svg viewBox="0 0 100 100"><path d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90" stroke="#ff712b" stroke-width="10" fill-opacity="0"></path><path d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90" stroke="" stroke-width="10" fill-opacity="0" style="stroke-dasharray: 282.783, 282.783; stroke-dashoffset: 98.974;"></path></svg><div class="progressbar-text" style="color: rgb(51, 101, 138);">65%</div></div><!-- excite-p-bar -->
                        </div><!-- excite-p-bar-container -->
                    </div>
                    <div class="col-md-2 col-sm-3">
                        <div class="excite-p-bar-container">
                            <div class="excite-p-bar bar-circle c-blue" data-progress-bar-style="circle" data-stroke-width="10" data-trail-width="10" data-progress-percent="0.75" data-duration="1400" data-display-value="true" data-trail-color="#ff712b" data-id="5" style="position: relative;">
                                <label class="excite-p-bar-label">Sketch</label>
                            <svg viewBox="0 0 100 100"><path d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90" stroke="#ff712b" stroke-width="10" fill-opacity="0"></path><path d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90" stroke="" stroke-width="10" fill-opacity="0" style="stroke-dasharray: 282.783, 282.783; stroke-dashoffset: 70.6958;"></path></svg><div class="progressbar-text" style="color: rgb(51, 101, 138);">75%</div></div><!-- excite-p-bar -->
                        </div><!-- excite-p-bar-container -->
                    </div>
                    <div class="col-md-2 col-sm-3">
                        <div class="excite-p-bar-container">
                            <div class="excite-p-bar bar-circle c-blue" data-progress-bar-style="circle" data-stroke-width="10" data-trail-width="10" data-progress-percent="0.70" data-duration="1500" data-display-value="true" data-trail-color="#ff712b" data-id="6" style="position: relative;">
                                <label class="excite-p-bar-label">Corel</label>
                            <svg viewBox="0 0 100 100"><path d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90" stroke="#ff712b" stroke-width="10" fill-opacity="0"></path><path d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90" stroke="" stroke-width="10" fill-opacity="0" style="stroke-dasharray: 282.783, 282.783; stroke-dashoffset: 84.8349;"></path></svg><div class="progressbar-text" style="color: rgb(51, 101, 138);">70%</div></div><!-- excite-p-bar -->
                        </div><!-- excite-p-bar-container -->
                    </div>
                </section>
                
            </div>	
  
			</div>

		
	
			<div class="col-md-2 sidebar pr0 pdl25" style="margin-left:;">
					<div class="m-left m-right sidebar-widget-wrapper" style="margin-top: 0px;">
						
						
						<div class="widget calendar-widget">
							<table id="wp-calendar">
								<caption>JANUARY 2017</caption>
								<thead>
									<tr>
										<th scope="col" title="Monday">S</th>
										<th scope="col" title="Tuesday">S</th>
										<th scope="col" title="Wednesday">M</th>
										<th scope="col" title="Thursday">T</th>
										<th scope="col" title="Friday">W</th>
										<th scope="col" title="Saturday">T</th>
										<th scope="col" title="Sunday">F</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<td>1</td>
										<td>2</td>
										<td>3</td>
										<td>4</td>
										<td>5</td>
										<td>6</td>
									</tr>
									<tr>
										<td id="today">7</td>
										<td>8</td>
										<td>9</td>
										<td><a href="#" aria-label="Posts published on March 4, 2017">10</a></td>
										<td>11</td>
										<td>12</td>
										<td>13</td>
									</tr>
									<tr>
										<td>14</td>
										<td><a href="#" class="highlight test_tool" aria-label="Posts published on March 4, 2017" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Event on this date!">15</a></td>
										<td>16</td>
										<td>17</td>
										<td>18</td>
										<td>19</td>
										<td>20</td>
									</tr>
									<tr>
										<td>21</td>
										<td>22</td>
										<td>23</td>
										<td>24</td>
										<td>25</td>
										<td>26</td>
										<td>27</td>
									</tr>
									<tr>
										<td>28</td>
										<td>29</td>
										<td>30</td>
										<td>31</td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
						
						<div class="widget recent-comment-widget">
							<h3 class="widget-title">Recent Blog</h3>
							<ul class="f-widget-recent-entries">
									<li><a href="#">What’s The Theory of Design ?</a></li>
									<li><a href="#">Latest News For Design 2017</a></li>
									<li><a href="#">Design Trend 2017</a></li>
									<li><a href="#">Latest News For Design</a></li>
									<li><a href="#">Principle &amp; The Theory of Design</a></li>
									<li><a href="#">Recent Design Trend &amp; Ideas</a></li>
								</ul>
						</div>
						<div class="widget recent-comment-widget">
							<h3 class="widget-title">News Feeds</h3>
							<ul class="recentcomments">
								<li><a href="#">The standard Lorem Ipsum passage</a> By <span class="comment-author-link"><a href="http://example.org/" rel="external nofollow" class="url">Excite</a></span></li>
								<li><a href="#">The standard Lorem Ipsum used since</a> By <span class="comment-author-link"><a href="http://example.org/" rel="external nofollow" class="url">The 1500s</a></span></li>
								<li><span class="comment-author-link"><a href="http://example.org/" rel="external nofollow" class="url">Standard</a></span> By <a href="#">Lorem written by Cicero in 45 Bc</a></li>
							</ul>
						</div>
					</div><!-- sidebar-widget-wrapper -->
			</div>

		</div>
		<section class="s-padding pl70 pdr30 bg-14">
                    <div class="text-container-title c-white">
                        <h2 class="c-blue"><span class="light">Trustable Clients</span></h2>
                        <p>There are many variations of passages of Lorem Ipsum available but <br>alteration in some form by injected humoures</p>
                    </div>
                    <ul class="clients-logo owl-carousel no-nav owl-loaded owl-drag">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-1044px, 0px, 0px); transition: all 0s ease 0s; width: 4177px;">
                    <div class="owl-item cloned" style="width: 208.8px;"><li class="client-logo-item">
                    	<img src="images/brand-12.png" alt=""></li>
                    </div>
                    	<div class="owl-item cloned" style="width: 208.8px;"><li class="client-logo-item">
                    		<img src="images/brand-13.png" alt=""></li>
                    	</div>
                    	<div class="owl-item cloned" style="width: 208.8px;"><li class="client-logo-item"><img src="images/brand-14.png" alt=""></li>
                    	</div>
                    	<div class="owl-item cloned" style="width: 208.8px;"><li class="client-logo-item"><img src=".images/brand-15.png" alt=""></li>
                    	</div>
                    	<div class="owl-item cloned" style="width: 208.8px;"><li class="client-logo-item"><img src="images/brand-16.png" alt=""></li>
                    	</div>
                    	<div class="owl-item active" style="width: 208.8px;"><li class="client-logo-item"><img src="images/brand-12.png" alt=""></li>
                    	</div>
                    	<div class="owl-item active" style="width: 208.8px;"><li class="client-logo-item"><img src="images/brand-13.png" alt=""></li>
                    	</div>
                    	<div class="owl-item active" style="width: 208.8px;"><li class="client-logo-item"><img src="images/brand-14.png" alt=""></li>
                    	</div>
                    	<div class="owl-item active" style="width: 208.8px;"><li class="client-logo-item"><img src="images/brand-15.png" alt=""></li>
                    	</div>
                    	<div class="owl-item active" style="width: 208.8px;"><li class="client-logo-item"><img src="images//brand-16.png" alt=""></li>
                    	</div>
                    	
                    	
                    	</div></div>
                    	<div class="owl-nav">
                    		<div class="owl-prev"><i class="icofont icofont-rounded-left"></i></div><div class="owl-next"><i class="icofont icofont-rounded-right"></i></div>
                    	</div>
                    		<div class="owl-dots"><div class="owl-dot active"><span></span></div><div class="owl-dot"><span></span></div></div>
                    		<div class="owl-thumbs"></div>
                    	</ul>
                </section>
                <section class="c-to-a s-padding pl70 pdr30 overlay page-title-bg-3" data-overlay="8">
                    <div class="m-right m-left column-align-middle">
                        <div class="col-md-8">
                            <div class="float-area-content">
                                <h2>Do You Have Any Interesting Project?</h2>
                                <p>It is a long established fact that a reader will be the readable content layout.</p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="float-area-btn float-right ">
                                <a href="#" class="btn btn1">CONTACT ME</a>
                            </div>
                        </div>
                    </div>
                </section><!-- call to action -->
	</div>


	<?php include("footer.php"); ?>
<a href="#" class="scroll-top"><i class="fa fa-arrow-up"></i></a>
	<script src="js/jquery-2.2.3.min.js">
	</script>
	<script src="js/bootstrap.min.js">
	</script> 
	<script src="js/waypoints.min.js">
	</script>
	<script src="js/jquery.easing.1.3.js">
	</script> 
	<script src="js/validator.min.js">
	</script>
	<script src="js/owl.carousel.js">
	</script> 
	<script src="js/owl.carousel2.thumbs.js">
	</script>
	<script src="js/jquery.nav.js">
	</script> 
	<script src="js/jquery.stellar.min.js">
	</script>
	<script src="js/wow.min.js">
	</script> 
	<script src="js/lightcase.js">
	</script>
	<script src="js/scrolloverflow.min.js">
	</script> 
	<script src="js/smooth-scroll.min.js">
	</script>
	<script src="js/jquery.fullpage.min.js">
	</script> 
	<script src="js/jquery.events.touch.js">
	</script>
	<script src="js/jquery.infinitescroll.min.js">
	</script> 
	<script src="js/jquery.lazyload.min.js">
	</script>
	<script src="js/swiper.min.js">
	</script> 
	<script src="js/parallax.min.js">
	</script>
	<script src="js/masonry.pkgd.min.js">
	</script> 
	<script src="js/shuffle.min.js">
	</script>
	<script src="js/animsition.min.js">
	</script> 
	<script src="js/swiper.min.js">
	</script>
	<script src="js/jquery.nstSlider.js">
	</script> 
	<script src="js/jquery.countdown.min.js">
	</script>
	<script src="js/jquery.counterup.min.js">
	</script> 
	<script src="js/bootsnav.js">
	</script>
	<script src="js/tilt.jquery.min.js">
	</script> 
	<script src="js/custom.js">
	</script>


	<script type="text/javascript">
	var infinitymanualmesonary=$(".infinityselctor-manaul-mesonary");infinitymanualmesonary.infinitescroll({navSelector:".infinity-manaul-links",nextSelector:".infinity-manaul-links a:first",itemSelector:".infinity-item-manaul",loading:{msgText:"Loading more posts...",finishedMsg:"Sorry, no more posts.",},errorCallback:function(){$(".post-load").css("display","none")}},function(a){var b=$(a).css("opacity",0);b.imagesLoaded(function(){b.animate({opacity:1});infinitymanualmesonary.masonry("appended",b,true)})});$(window).unbind(".infscr");$(".post-load").click(function(){infinitymanualmesonary.infinitescroll("retrieve");return false});
	</script> 
	<script type="text/javascript">
	$(".js-tilt").tilt({maxTilt:20,perspective:1000,easing:"cubic-bezier(.03,.98,.52,.99)",scale:1,speed:300,transition:true,axis:null,reset:true,glare:false,maxGlare:1,});
	</script>
	

	<script>
	$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
	<script type="text/javascript">
		$(document).ready(function(){

			$("#1").show();
            $("#2").hide();
            $("#3").hide();
       
        $("#timeline").click(function(){
            $("#1").show();
            $("#2").hide();
            $("#3").hide();
        });

        $("#description").click(function(){
        	$("#1").hide();
            $("#2").show();
            $("#3").hide();
        });
        $("#details").click(function(){
        	$("#1").hide();
            $("#2").hide();
            $("#3").show();
        });

    });
	</script>
	<script>
$(document).ready(function() {
$(".tablinks").click(function () {
    $(".tablinks").removeClass("active");
    // $(".tab").addClass("active"); // instead of this do the below 
    $(this).addClass("active");   
});
});
</script>
</body>
</html>
