<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="UTF-8">
	<title>LuminoSoul</title>
	<meta content="" name="keywords">
	<meta content="" name="description">
	<meta content="" name="author">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	
	<link href="css/lightcase.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="fonts/font.css">
	<link rel="stylesheet" type="text/css" href="palyfair_Dispaly/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="amaranth/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="gentium_Basic/stylesheet.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="css/icofont.css" rel="stylesheet">
	<link href="css/animsition.min.css" rel="stylesheet">
	<link href="css/jquery.nstSlider.min.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/swiper.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/animate.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootsnav.css" rel="stylesheet">
	<link href="css/shortcode.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<script src="js/ga.js" type="text/javascript">
	</script>
</head>
<body class="demo-landing-page p-load-animation">
	<div class="style-switcher">
        <div class="switcher-toggle">
			<i class="fa fa-cog" aria-hidden="true"></i>
			<span>Options</span>
		</div>
        
       <div class="style-switcher-container">
            <div class="style-switcher-inner">
               
                <div class="optionpart">
                                <aside class="col-md-12 sidebar">
					<div class="m-left m-right sidebar-widget-wrapper">
						<div class="widget pdt20">
							<form role="search" method="get" class="search-form"> 
								<label>
									<span class="screen-reader-text">Search....</span> 
									<input type="search" class="search-field" placeholder="Search...." value="" name="s" autocomplete="off"> 
								</label>
								<input type="submit" class="search-submit" value="Search">
							</form>
						</div><!-- widget -->
						
						<div class="widget categorie-widget">
							<h3 class="widget-title">Sorting</h3>
							<!-- <ul>
								<li><a href="#">Delhi Public Sschool</a></li>
								<li><a href="#">D.A.V Sschool</a></li>
								<li><a href="#">Greenfield Public School</a></li>
								<li><a href="#">Loreto Convent School</a></li>
							</ul> -->
							<div class="checkbox">
								<label><input type="checkbox" value="">Delhi Public Sschool</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">D.A.V Sschool</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Greenfield Public School</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Loreto Convent School</label>
							</div>
						
						</div><!-- widget -->
						
						<!-- widget -->
						
						<div class="widget categorie-widget">
							<h3 class="widget-title">Medium:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Hindi</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">English</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Board:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">ICSE</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">CBSE</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">OPEN</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">OTHER</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Boarding:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Day school</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Boarding</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Both</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">School type:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Boys</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Grils</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">co-ed</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Nationality:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">International</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">National</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Categories</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Graphic Design</label><span style="padding-left: 5px;">(3)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Web Design</label><span style="padding-left: 5px;">(5)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Photography</label><span style="padding-left: 5px;">(2)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Marketing</label><span style="padding-left: 5px;">(4)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Business Consult</label><span style="padding-left: 5px;">(1)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Domain &amp; Hosting</label><span style="padding-left: 5px;">(3)</span>
							</div>
						</div><!-- widget -->
						
						<div class="widget tag-widget">
							<h3 class="widget-title">TAGS</h3>
							<div class="tagcloud">
								<a href="#">web</a>
								<a href="#">graphic</a>
								<a href="#">design</a>
								<a href="#">marketing</a>
								<a href="#">seo</a>
								<a href="#">logo</a>
							</div>
						</div><!-- widget -->
					</div><!-- sidebar-widget-wrapper -->
				</aside>
                   
                   
                </div>
            </div>
        </div>
    </div>
	
	<?php include("header.php"); ?>

<section class="carousel-new-s" style="background-color: ;">
	<div class="p-caption demo-banner-content-inner js-tilt" style="transform: perspective(1000px) rotateX(0deg) rotateY(0deg); will-change: transform;">			
	<h3 class="text-white"  style="color: #ffffff;font-size: 50px;">NORMAL </h3>
	
	  </div>
	<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="3000" data-pause="">
    
    <!-- Wrapper for slides -->
    <div class="carousel-inner block ">
      <div class="item active">
        <img src="images/slide-1.JPG" class="slide_img" alt="Los Angeles">
       
                 
      </div>

      <div class="item">
        <img src="images/slide-2.jpg" class="slide_img" alt="Chicago">
       
      </div>
    
      <div class="item">
        <img src="images/slide-3.jpg" class="slide_img" alt="New york">

      </div>
    </div>

    <!-- Left and right controls -->
      <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  </div>


</section>
	
	<section class="blog-2 content s-padding">
		<div class="container">
			<div class="row">
				<div class="blog-g-wrapper infinityselctor m-left m-right">
					<div class="col-md-4 col-sm-6 infinity-item">
						<article class="e-blog-post card-layout">
							<div class="e-blog-post-inner">
								<div class="e-blog-post-thumb">
									<a href="#">
										<img src="images/posts/post-4.jpg" alt="">
									</a>
									<span class="e-blog-post-meta-category">
										<a href="#">WEB DESIGN</a>
										<a href="#">GRAPHIC</a>
										<a href="#">DESIGN</a>
									</span><!-- blog-post-meta-category -->
								</div><!-- blog-post-thumb -->
								<div class="e-blog-post-des">
									<h3 class="e-blog-post-title"><a href="#">Highly Experienced Team</a></h3>
									<div class="e-blog-post-meta">
										<span class="e-blog-post-meta-author">POSTED BY: <a href="#">ADMIN</a></span>
										<span class="e-blog-post-meta-time">26 JANUARY - 2017</span>
										<span class="e-blog-post-meta-tags">
											<i class="icofont icofont-tag"></i>
											<a href="#">Business</a>,
											<a href="#">Design</a>,
											<a href="#">social</a>
										</span>
									</div><!-- blog-post-meta -->
									<div class="e-blog-post-content">
										<p>Here are many variations of passages of Lorem Ipsum available, but the majority have suffered in some form by injected humour or randomise words ecsetrea of passages of Lorem</p>
									</div><!-- blog-post-content -->
									<a class="e-blog-post-more" href="#">READ MORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</div><!-- blog-post-des -->
							</div><!-- blog-post-inner -->
						</article><!-- blog-post -->
					</div>
					<div class="col-md-4 col-sm-6 infinity-item">
						<article class="e-blog-post card-layout">
							<div class="e-blog-post-inner">
								<div class="e-blog-post-thumb">
									<a href="#">
										<img src="images/posts/post-4.jpg" alt="">
									</a>
									<span class="e-blog-post-meta-category">
										<a href="#">WEB DESIGN</a>
										<a href="#">GRAPHIC</a>
										<a href="#">DESIGN</a>
									</span><!-- blog-post-meta-category -->
								</div><!-- blog-post-thumb -->
								<div class="e-blog-post-des">
									<h3 class="e-blog-post-title"><a href="#">Highly Experienced Team</a></h3>
									<div class="e-blog-post-meta">
										<span class="e-blog-post-meta-author">POSTED BY: <a href="#">ADMIN</a></span>
										<span class="e-blog-post-meta-time">26 JANUARY - 2017</span>
										<span class="e-blog-post-meta-tags">
											<i class="icofont icofont-tag"></i>
											<a href="#">Business</a>,
											<a href="#">Design</a>,
											<a href="#">social</a>
										</span>
									</div><!-- blog-post-meta -->
									<div class="e-blog-post-content">
										<p>Here are many variations of passages of Lorem Ipsum available, but the majority have suffered in some form by injected humour or randomise words ecsetrea of passages of Lorem</p>
									</div><!-- blog-post-content -->
									<a class="e-blog-post-more" href="#">READ MORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</div><!-- blog-post-des -->
							</div><!-- blog-post-inner -->
						</article><!-- blog-post -->
					</div>
					<div class="col-md-4 col-sm-6 infinity-item">
						<article class="e-blog-post card-layout">
							<div class="e-blog-post-inner">
								<div class="e-blog-post-thumb">
									<a href="#">
										<img src="images/posts/post-4.jpg" alt="">
									</a>
									<span class="e-blog-post-meta-category">
										<a href="#">WEB DESIGN</a>
										<a href="#">GRAPHIC</a>
										<a href="#">DESIGN</a>
									</span><!-- blog-post-meta-category -->
								</div><!-- blog-post-thumb -->
								<div class="e-blog-post-des">
									<h3 class="e-blog-post-title"><a href="#">Highly Experienced Team</a></h3>
									<div class="e-blog-post-meta">
										<span class="e-blog-post-meta-author">POSTED BY: <a href="#">ADMIN</a></span>
										<span class="e-blog-post-meta-time">26 JANUARY - 2017</span>
										<span class="e-blog-post-meta-tags">
											<i class="icofont icofont-tag"></i>
											<a href="#">Business</a>,
											<a href="#">Design</a>,
											<a href="#">social</a>
										</span>
									</div><!-- blog-post-meta -->
									<div class="e-blog-post-content">
										<p>Here are many variations of passages of Lorem Ipsum available, but the majority have suffered in some form by injected humour or randomise words ecsetrea of passages of Lorem</p>
									</div><!-- blog-post-content -->
									<a class="e-blog-post-more" href="#">READ MORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</div><!-- blog-post-des -->
							</div><!-- blog-post-inner -->
						</article><!-- blog-post -->
					</div>
					<div class="col-md-4 col-sm-6 infinity-item">
						<article class="e-blog-post card-layout">
							<div class="e-blog-post-inner">
								<div class="e-blog-post-thumb">
									<a href="#">
										<img src="images/posts/post-4.jpg" alt="">
									</a>
									<span class="e-blog-post-meta-category">
										<a href="#">WEB DESIGN</a>
										<a href="#">GRAPHIC</a>
										<a href="#">DESIGN</a>
									</span><!-- blog-post-meta-category -->
								</div><!-- blog-post-thumb -->
								<div class="e-blog-post-des">
									<h3 class="e-blog-post-title"><a href="#">Highly Experienced Team</a></h3>
									<div class="e-blog-post-meta">
										<span class="e-blog-post-meta-author">POSTED BY: <a href="#">ADMIN</a></span>
										<span class="e-blog-post-meta-time">26 JANUARY - 2017</span>
										<span class="e-blog-post-meta-tags">
											<i class="icofont icofont-tag"></i>
											<a href="#">Business</a>,
											<a href="#">Design</a>,
											<a href="#">social</a>
										</span>
									</div><!-- blog-post-meta -->
									<div class="e-blog-post-content">
										<p>Here are many variations of passages of Lorem Ipsum available, but the majority have suffered in some form by injected humour or randomise words ecsetrea of passages of Lorem</p>
									</div><!-- blog-post-content -->
									<a class="e-blog-post-more" href="#">READ MORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</div><!-- blog-post-des -->
							</div><!-- blog-post-inner -->
						</article><!-- blog-post -->
					</div>
					<div class="col-md-4 col-sm-6 infinity-item">
						<article class="e-blog-post card-layout">
							<div class="e-blog-post-inner">
								<div class="e-blog-post-thumb">
									<a href="#">
										<img src="images/posts/post-4.jpg" alt="">
									</a>
									<span class="e-blog-post-meta-category">
										<a href="#">WEB DESIGN</a>
										<a href="#">GRAPHIC</a>
										<a href="#">DESIGN</a>
									</span><!-- blog-post-meta-category -->
								</div><!-- blog-post-thumb -->
								<div class="e-blog-post-des">
									<h3 class="e-blog-post-title"><a href="#">Highly Experienced Team</a></h3>
									<div class="e-blog-post-meta">
										<span class="e-blog-post-meta-author">POSTED BY: <a href="#">ADMIN</a></span>
										<span class="e-blog-post-meta-time">26 JANUARY - 2017</span>
										<span class="e-blog-post-meta-tags">
											<i class="icofont icofont-tag"></i>
											<a href="#">Business</a>,
											<a href="#">Design</a>,
											<a href="#">social</a>
										</span>
									</div><!-- blog-post-meta -->
									<div class="e-blog-post-content">
										<p>Here are many variations of passages of Lorem Ipsum available, but the majority have suffered in some form by injected humour or randomise words ecsetrea of passages of Lorem</p>
									</div><!-- blog-post-content -->
									<a class="e-blog-post-more" href="#">READ MORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</div><!-- blog-post-des -->
							</div><!-- blog-post-inner -->
						</article><!-- blog-post -->
					</div>
					<div class="col-md-4 col-sm-6 infinity-item">
						<article class="e-blog-post card-layout">
							<div class="e-blog-post-inner">
								<div class="e-blog-post-thumb">
									<a href="#">
										<img src="images/posts/post-4.jpg" alt="">
									</a>
									<span class="e-blog-post-meta-category">
										<a href="#">WEB DESIGN</a>
										<a href="#">GRAPHIC</a>
										<a href="#">DESIGN</a>
									</span><!-- blog-post-meta-category -->
								</div><!-- blog-post-thumb -->
								<div class="e-blog-post-des">
									<h3 class="e-blog-post-title"><a href="#">Highly Experienced Team</a></h3>
									<div class="e-blog-post-meta">
										<span class="e-blog-post-meta-author">POSTED BY: <a href="#">ADMIN</a></span>
										<span class="e-blog-post-meta-time">26 JANUARY - 2017</span>
										<span class="e-blog-post-meta-tags">
											<i class="icofont icofont-tag"></i>
											<a href="#">Business</a>,
											<a href="#">Design</a>,
											<a href="#">social</a>
										</span>
									</div><!-- blog-post-meta -->
									<div class="e-blog-post-content">
										<p>Here are many variations of passages of Lorem Ipsum available, but the majority have suffered in some form by injected humour or randomise words ecsetrea of passages of Lorem</p>
									</div><!-- blog-post-content -->
									<a class="e-blog-post-more" href="#">READ MORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</div><!-- blog-post-des -->
							</div><!-- blog-post-inner -->
						</article><!-- blog-post -->
					</div>
					<div class="col-md-4 col-sm-6 infinity-item">
						<article class="e-blog-post card-layout">
							<div class="e-blog-post-inner">
								<div class="e-blog-post-thumb">
									<a href="#">
										<img src="images/posts/post-4.jpg" alt="">
									</a>
									<span class="e-blog-post-meta-category">
										<a href="#">WEB DESIGN</a>
										<a href="#">GRAPHIC</a>
										<a href="#">DESIGN</a>
									</span><!-- blog-post-meta-category -->
								</div><!-- blog-post-thumb -->
								<div class="e-blog-post-des">
									<h3 class="e-blog-post-title"><a href="#">Highly Experienced Team</a></h3>
									<div class="e-blog-post-meta">
										<span class="e-blog-post-meta-author">POSTED BY: <a href="#">ADMIN</a></span>
										<span class="e-blog-post-meta-time">26 JANUARY - 2017</span>
										<span class="e-blog-post-meta-tags">
											<i class="icofont icofont-tag"></i>
											<a href="#">Business</a>,
											<a href="#">Design</a>,
											<a href="#">social</a>
										</span>
									</div><!-- blog-post-meta -->
									<div class="e-blog-post-content">
										<p>Here are many variations of passages of Lorem Ipsum available, but the majority have suffered in some form by injected humour or randomise words ecsetrea of passages of Lorem</p>
									</div><!-- blog-post-content -->
									<a class="e-blog-post-more" href="#">READ MORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</div><!-- blog-post-des -->
							</div><!-- blog-post-inner -->
						</article><!-- blog-post -->
					</div>
					<div class="col-md-4 col-sm-6 infinity-item">
						<article class="e-blog-post card-layout">
							<div class="e-blog-post-inner">
								<div class="e-blog-post-thumb">
									<a href="#">
										<img src="images/posts/post-5.jpg" alt="">
									</a>
									<span class="e-blog-post-meta-category">
										<a href="#">DESIGN</a>
										<a href="#">wordpress</a>
									</span><!-- blog-post-meta-category -->
								</div><!-- blog-post-thumb -->
								<div class="e-blog-post-des">
									<h3 class="e-blog-post-title"><a href="#">This Is Example Post One</a></h3>
									<div class="e-blog-post-meta">
										<span class="e-blog-post-meta-author">POSTED BY: <a href="#">ADMIN</a></span>
										<span class="e-blog-post-meta-time">26 JANUARY - 2017</span>
										<span class="e-blog-post-meta-tags">
											<i class="icofont icofont-tag"></i>
											<a href="#">Business</a>,
											<a href="#">Design</a>,
											<a href="#">social</a>
										</span>
									</div><!-- blog-post-meta -->
									<div class="e-blog-post-content">
										<p>Here are many variations of passages of Lorem Ipsum available, but the majority have suffered in some form by injected humour or randomise words ecsetrea of passages of Lorem</p>
									</div><!-- blog-post-content -->
									<a class="e-blog-post-more" href="#">READ MORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</div><!-- blog-post-des -->
							</div><!-- blog-post-inner -->
						</article><!-- blog-post -->
					</div>
					<div class="col-md-4 col-sm-6 infinity-item">
						<article class="e-blog-post card-layout">
							<div class="e-blog-post-inner">
								<div class="e-blog-post-thumb">
									<a href="#">
										<img src="images/posts/post-6.jpg" alt="">
									</a>
									<span class="e-blog-post-meta-category">
										<a href="#">DESIGN</a>
										<a href="#">social</a>
										<a href="#">photography</a>
									</span><!-- blog-post-meta-category -->
								</div><!-- blog-post-thumb -->
								<div class="e-blog-post-des">
									<h3 class="e-blog-post-title"><a href="#">Example Blog Post For</a></h3>
									<div class="e-blog-post-meta">
										<span class="e-blog-post-meta-author">POSTED BY: <a href="#">ADMIN</a></span>
										<span class="e-blog-post-meta-time">26 JANUARY - 2017</span>
										<span class="e-blog-post-meta-tags">
											<i class="icofont icofont-tag"></i>
											<a href="#">Business</a>,
											<a href="#">Design</a>,
											<a href="#">social</a>
										</span>
									</div><!-- blog-post-meta -->
									<div class="e-blog-post-content">
										<p>Here are many variations of passages of Lorem Ipsum available, but the majority have suffered in some form by injected humour or randomise words ecsetrea of passages of Lorem</p>
									</div><!-- blog-post-content -->
									<a class="e-blog-post-more" href="#">READ MORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</div><!-- blog-post-des -->
							</div><!-- blog-post-inner -->
						</article><!-- blog-post -->
					</div>
					
				</div>
			</div>
		</div>
	</section>
	
	<?php include("footer.php"); ?>
<a href="#" class="scroll-top"><i class="fa fa-arrow-up"></i></a>
	<script src="js/jquery-2.2.3.min.js">
	</script>
	<script src="js/bootstrap.min.js">
	</script> 
	<script src="js/waypoints.min.js">
	</script>
	<script src="js/jquery.easing.1.3.js">
	</script> 
	<script src="js/validator.min.js">
	</script>
	<script src="js/owl.carousel.js">
	</script> 
	<script src="js/owl.carousel2.thumbs.js">
	</script>
	<script src="js/jquery.nav.js">
	</script> 
	<script src="js/jquery.stellar.min.js">
	</script>
	<script src="js/wow.min.js">
	</script> 
	<script src="js/lightcase.js">
	</script>
	<script src="js/scrolloverflow.min.js">
	</script> 
	<script src="js/smooth-scroll.min.js">
	</script>
	<script src="js/jquery.fullpage.min.js">
	</script> 
	<script src="js/jquery.events.touch.js">
	</script>
	<script src="js/jquery.infinitescroll.min.js">
	</script> 
	<script src="js/jquery.lazyload.min.js">
	</script>
	<script src="js/swiper.min.js">
	</script> 
	<script src="js/parallax.min.js">
	</script>
	<script src="js/masonry.pkgd.min.js">
	</script> 
	<script src="js/shuffle.min.js">
	</script>
	<script src="js/animsition.min.js">
	</script> 
	<script src="js/swiper.min.js">
	</script>
	<script src="js/jquery.nstSlider.js">
	</script> 
	<script src="js/jquery.countdown.min.js">
	</script>
	<script src="js/jquery.counterup.min.js">
	</script> 
	<script src="js/bootsnav.js">
	</script>
	<script src="js/tilt.jquery.min.js">
	</script> 
	<script src="js/custom.js">
	</script>


	<script type="text/javascript">
	var infinitymanualmesonary=$(".infinityselctor-manaul-mesonary");infinitymanualmesonary.infinitescroll({navSelector:".infinity-manaul-links",nextSelector:".infinity-manaul-links a:first",itemSelector:".infinity-item-manaul",loading:{msgText:"Loading more posts...",finishedMsg:"Sorry, no more posts.",},errorCallback:function(){$(".post-load").css("display","none")}},function(a){var b=$(a).css("opacity",0);b.imagesLoaded(function(){b.animate({opacity:1});infinitymanualmesonary.masonry("appended",b,true)})});$(window).unbind(".infscr");$(".post-load").click(function(){infinitymanualmesonary.infinitescroll("retrieve");return false});
	</script> 
	<script type="text/javascript">
	$(".js-tilt").tilt({maxTilt:20,perspective:1000,easing:"cubic-bezier(.03,.98,.52,.99)",scale:1,speed:300,transition:true,axis:null,reset:true,glare:false,maxGlare:1,});
	</script>
	


	<script>
	$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
	<script type="text/javascript">
		$(document).ready(function(){

			$("#1").show();
            $("#2").hide();
            $("#3").hide();
       
        $("#timeline").click(function(){
            $("#1").show();
            $("#2").hide();
            $("#3").hide();
        });

        $("#description").click(function(){
        	$("#1").hide();
            $("#2").show();
            $("#3").hide();
        });
        $("#details").click(function(){
        	$("#1").hide();
            $("#2").hide();
            $("#3").show();
        });

    });
	</script>
	<script>
$(document).ready(function() {
$(".tablinks").click(function () {
    $(".tablinks").removeClass("active");
    // $(".tab").addClass("active"); // instead of this do the below 
    $(this).addClass("active");   
});
});
</script>
</body>
</html>
