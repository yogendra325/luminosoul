<?php
include("../lib/config.php");
include("../lib/db.php");
include("../lib/infocular.php");
include("../lib/apiCommon.php");
include("../lib/sendMail.php");
include("../lib/commonFunction.php");
include("../lib/getApi.php");

function getAdmissionQuestion(){
            $JSON_DATA=file_get_contents("php://input");
            $INPUT = json_decode($JSON_DATA);
            $db=new Database();
            $db->query('SELECT * FROM usr_question WHERE usr_id=:usr_id');
           $db->bind(':usr_id',$INPUT->USR_ID);
          $DATA=$db->resultset();
          if(sizeof($DATA)>0){
              $i=1;
              $HTML="";
            foreach($DATA as $ROW)
            {

            $QUESTION=getFullQuestionById($ROW['q_id']);
            $HTML.= '<div class="row" id="admissionQuestion'.$ROW['ID'].'">
            <div class="col-sm-2">
            '.$i.'
            </div>

            <div class="col-sm-8">
            '.$QUESTION['question'].'
            </div>
            <div class="col-sm-2" onclick="removeadmissionQuestion('.$ROW['ID'].')">
            Remove
            </div>
            </div>';
            $i++;
            }
        
                $resp=array("DATA"=>$HTML,
                "MSG"=>"Question Return",
                "ERROR"=>"NONE" );
                echo json_encode($resp);
          }
          else{
            $resp=array("DATA"=>null,
            "MSG"=>"No Question Is Added",
            "ERROR"=>"YES" );
            echo json_encode($resp);
          }
          
}

#################################################
###############################################
function removeAdmissionQuestion()
{
    $JSON_DATA=file_get_contents("php://input");
    $INPUT = json_decode($JSON_DATA);
    $db=new Database();
    $db->query('DELETE from usr_question   WHERE ID=:ID  AND usr_id=:usr_id');
    $db->bind(':usr_id',$INPUT->USR_ID);
    $db->bind(':ID',$INPUT->Q_ID);
    if($db->execute()) {
        $resp=array("DATA"=>null,
                "MSG"=>"Question Removed",
                "ERROR"=>"NONE" );
                echo json_encode($resp);
    } 
    else {
        $resp=array("DATA"=>null,
        "MSG"=>"Something Went Wrong",
        "ERROR"=>"YES" );
        echo json_encode($resp);
    }
}
#################################################
###############################################
function getAllAdmissionQuestion()
{
    $JSON_DATA=file_get_contents("php://input");
    $INPUT = json_decode($JSON_DATA);

    $db=new Database();
    $db->query('SELECT * FROM iquestion WHERE sts=:sts AND type=:type');
   $db->bind(':sts',1);
   $db->bind(':type','ADMISSION');
   $DATA=$db->resultset();
  $HTML="";
  if(sizeof($DATA)>0){
      $i=1;
    foreach($DATA as $ROW)
    {

        if(questionIsInUserList($ROW['ID'],$INPUT->USR_ID)){
                $HTML.= '<div class="row" id="admissionQuestionList'.$ROW['ID'].'">
                <div class="col-sm-2">
                '.$i.'</div>

                <div class="col-sm-8" >
                '.$ROW['question'].'
                </div>
                <div class="col-sm-2" onclick="addThisToForm('.$ROW['ID'].')" >
                Add To Form
                </div>
                </div>
                </div>';
                $i++;
        }
    
   
    }
    $resp=array("DATA"=>$HTML,
                "MSG"=>"Question Return",
                "ERROR"=>"NONE" );
                echo json_encode($resp);
  }
  else{
        $resp=array("DATA"=>null,
        "MSG"=>"No Question Is There",
        "ERROR"=>"YES" );
        echo json_encode($resp);

  }
  
}


############################################
##############################################
function addThisQuestionToMyFOrm()
{
    $JSON_DATA=file_get_contents("php://input");
    $INPUT = json_decode($JSON_DATA);
    $db=new Database();
    $db->query('INSERT INTO   usr_question (q_id,usr_id) VALUES (:q_id,:usr_id)');
   $db->bind(':q_id',$INPUT->Q_ID);
   $db->bind(':usr_id',$INPUT->USR_ID);
  if($db->execute()){

    $resp=array("DATA"=>null,
    "MSG"=>" Question Is Added",
    "ERROR"=>"NONE" );
    echo json_encode($resp);
   }
    else{
    $resp=array("DATA"=>null,
        "MSG"=>"Something Went Wrong",
        "ERROR"=>"YES" );
        echo json_encode($resp);
  }
}

##################################################
###########################################
function addThisQuestionToDatabase(){
    $JSON_DATA=file_get_contents("php://input");
    $INPUT = json_decode($JSON_DATA); 
    $db=new Database();
    $i=new infocular();
    $db->query('INSERT INTO  iquestion 
                                    (question,
                                     usr_id,
                                     type,
                                     usr_ip,
                                     sts
                                    )
                                 VALUES (
                                         :question,
                                         :usr_id,
                                         :type,
                                         :usr_ip,
                                         :sts)');
    $db->bind(':question',$INPUT->QUESTION);
    $db->bind(':usr_id',$INPUT->USR_ID);
    $db->bind(':type',"ADMISSION");
    $db->bind(':usr_ip',$i->getIP());
    $db->bind(':sts',0);
   if($db->execute()){
     // $DATA['FNAME']=$FNAME;
    // $DATA['LNAME']=$LNAME;
    // $DATA['PHONE']=$PHONE;
    // $DATA['EMAIL']=$EMAIL;
    // $DATA['SUBJECT']=$SUBJECT;
    // sendMail("NEW_QUESTION",$DATA);

     $resp=array("DATA"=>null,
     "MSG"=>" Question Is Added",
     "ERROR"=>"NONE" );
     echo json_encode($resp);
    }
     else{
     $resp=array("DATA"=>null,
         "MSG"=>"Something Went Wrong",
         "ERROR"=>"YES" );
         echo json_encode($resp);
   }
    
}

##################################################
###########################################
function getSchoolDetails(){
    
    $JSON_DATA=file_get_contents("php://input");
    $INPUT = json_decode($JSON_DATA);

    $HTML=school_details($INPUT->USR_ID);
            $resp=array("DATA"=>$HTML,
            "MSG"=>"Deatils Return",
            "ERROR"=>"NONE" );
            echo json_encode($resp);
           
  
}

##################################################
###########################################
function editSchoolDetails(){
    
    $JSON_DATA=file_get_contents("php://input");
    $INPUT = json_decode($JSON_DATA);
            $db=new Database();
            $db->query('SELECT * FROM school_details WHERE usr_id=:usr_id');
            $db->bind(':usr_id',$INPUT->USR_ID);
            $DATA=$db->single(); 
                    if($DATA==false) {
                            
                       $HTML='<div class="col-sm-12 article">
				
                       <h4>Edit Details</h4>
                                   <form action="" method="post" id="schoolDetailform">
                                       <div class="row">
                                           
                                           
           <div class="col-sm-12">
           <div class="input-group">
           <textarea row="2" id="s_about" name="about" placeholder="About School" style="margin: 0;color: #000"></textarea>
           </div>
           </div>
       
               
               
           <div class="col-sm-6">
           <div class="input-group">
           <input type="text" id="s_name" name="name" placeholder="School name">
           </div>
           </div>
                   <div class="col-sm-6">
                   <div class="input-group">
                   <input type="text" id="s_type" name="type" placeholder="School Type">
                   </div>
                   </div>
                   
                   
                   <div class="col-sm-6">
                   <div class="input-group">
                   <input type="text" id="s_area" name="area" placeholder="School Area">
                   </div>
                   </div>
                   
                   <div class="col-sm-6">
                   <div class="input-group">
                   <input type="text" id="s_bedroom" name="bedroom" placeholder="Bedrooms">
                   </div>
                   </div>
                   
                   <div class="col-sm-6">
                   <div class="input-group">
                   <input type="text" id="s_food" name="food" placeholder="Food Facility">
                   </div>
                   </div>
                   
                   <div class="col-sm-6">
                   <div class="input-group">
                   <input type="text" id="s_rent" name="rent" placeholder="Rent Payment">
                   </div>
                   </div>
                   
                   <div class="col-sm-6">
                   <div class="input-group">
                   <input type="text" id="s_for" name="available_for" placeholder="Availabel For">
                   </div>
                   </div>
                   
                   
                                   
                                               
                   <div class="col-sm-12" style="margin-top: 20px;">
                   <input type="button" value="Save" class="btn btn1" onclick="saveSchoolDetails()">
                                           </div>
                                           
                                       </div>
                                   
       
                                     </form>
                               </div>';
                            
                        $resp=array("DATA"=>$HTML,
                        "MSG"=>"Deatils Return",
                        "ERROR"=>"NONE" );
                        echo json_encode($resp);
                    } 
                    else{

                        $HTML='<div class="col-sm-12 article">
				
                        <h4>Edit Details</h4>
                                    <form action="" method="post" id="schoolDetailform">
                                        <div class="row">
                                            
                                            
            <div class="col-sm-12">
            <div class="input-group">
            <textarea row="2" id="s_about" name="about" placeholder="About School" style="margin: 0;color: #000">'.$DATA['about'].'</textarea>
            </div>
            </div>
        
                
                
            <div class="col-sm-6">
            <div class="input-group">
            <input type="text" id="s_name" name="name" placeholder="School name" value="'.$DATA['name'].'">
            </div>
            </div>
                    <div class="col-sm-6">
                    <div class="input-group">
                    <input type="text" id="s_type" name="type" placeholder="School Type" value="'.$DATA['type'].'">
                    </div>
                    </div>
                    
                    
                    <div class="col-sm-6">
                    <div class="input-group">
                    <input type="text" id="s_area" name="area" placeholder="School Area" value="'.$DATA['area'].'">
                    </div>
                    </div>
                    
                    <div class="col-sm-6">
                    <div class="input-group">
                    <input type="text" id="s_bedroom" name="bedroom" placeholder="Bedrooms" value="'.$DATA['bedroom'].'">
                    </div>
                    </div>
                    
                    <div class="col-sm-6">
                    <div class="input-group">
                    <input type="text" id="s_food" name="food" placeholder="Food Facility" value="'.$DATA['food'].'">
                    </div>
                    </div>
                    
                    <div class="col-sm-6">
                    <div class="input-group">
                    <input type="text" id="s_rent" name="rent" placeholder="Rent Payment" value="'.$DATA['rent_pyament'].'">
                    </div>
                    </div>
                    
                    <div class="col-sm-6">
                    <div class="input-group">
                    <input type="text" id="s_for" name="available_for" placeholder="Availabel For" value="'.$DATA['available_for'].'">
                    </div>
                    </div>
                    
                    
                                    
                                                
                    <div class="col-sm-12" style="margin-top: 20px;">
                                            

                                                <input type="button" value="Save" class="btn btn1" onclick="saveSchoolDetails()">
                                            </div>
                                            
                                        </div>
                                    
        
                                      </form>
                                </div>';
                            
                        $resp=array("DATA"=>$HTML,
                        "MSG"=>"Deatils Return",
                        "ERROR"=>"NONE" );
                        echo json_encode($resp);
                       
                    }
  
}
###########################################
###########################################
function school_details($ID)
{
    $db=new Database();
    $db->query('SELECT * FROM school_details WHERE usr_id=:usr_id');
    $db->bind(':usr_id',$ID);
    $DATA=$db->single(); 
            if($DATA==false) {
                    
                $HTML='<h3>School details</h3>
                    <button type="button" class="btn btn-primary round_btn" onclick="editSchoolDetails()"><i class="fa fa-pencil"></i></button>
                   
                    <h4>About</h4>
                    <p class="pt20">
                        About School
                    </p>
                    <div class="col-lg-12 pdr0 detail-part">
                        <div class="col-sm-4 col-xs-6 pl0 pt20">
                            <h4>Type</h4>
                            <h5>School Type</h5>
                        </div>
                        <div class="col-sm-4 col-xs-6 pt20">
                            <h4>School ID</h4>
                            <h5>'.$ID.'</h5>
                        </div>
                        <div class="col-sm-4 col-xs-6 pt20">
                            <h4>School Area</h4>
                            <h5>Area</h5>
                        </div>
                        <div class="col-sm-4 col-xs-6 pl0 pt20">
                            <h4>NO. of Bedrooms</h4>
                            <h5>Number</h5>
                        </div>
                        <div class="col-sm-4 col-xs-6 pt20">
                            <h4>Food Preference</h4>
                            <h5>Food</h5>
                        </div>
                        <div class="col-sm-4 col-xs-6 pt20">
                            <h4>Rent Payment</h4>
                            <h5>REnt</h5>
                        </div>
                        <div class="col-sm-4 col-xs-6 pl0 pt20">
                            <h4>Available for</h4>
                            <h5>Available For</h5>
                        </div>
                        <div class="clearfix"></div>
                        
                    </div>';
                    
                    return $HTML;
            } 
            else{

                $HTML='<h3>School details</h3>
                    <button type="button" class="btn btn-primary round_btn" onclick="editSchoolDetails()"><i class="fa fa-pencil"></i></button>
                   
                    <h4>About</h4>
                    <p class="pt20">
                        '.$DATA['about'].'
                    </p>
                    <div class="col-lg-12 pdr0 detail-part">
                        <div class="col-sm-4 col-xs-6 pl0 pt20">
                            <h4>Type</h4>
                            <h5>'.$DATA['type'].'</h5>
                        </div>
                        <div class="col-sm-4 col-xs-6 pt20">
                            <h4>School ID</h4>
                            <h5>'.$ID.'</h5>
                        </div>
                        <div class="col-sm-4 col-xs-6 pt20">
                            <h4>School Area</h4>
                            <h5>'.$DATA['area'].'</h5>
                        </div>
                        <div class="col-sm-4 col-xs-6 pl0 pt20">
                            <h4>NO. of Bedrooms</h4>
                            <h5>'.$DATA['bedroom'].'</h5>
                        </div>
                        <div class="col-sm-4 col-xs-6 pt20">
                            <h4>Food Preference</h4>
                            <h5>'.$DATA['food'].'</h5>
                        </div>
                        <div class="col-sm-4 col-xs-6 pt20">
                            <h4>Rent Payment</h4>
                            <h5>'.$DATA['rent_pyament'].'</h5>
                        </div>
                        <div class="col-sm-4 col-xs-6 pl0 pt20">
                            <h4>Available for</h4>
                            <h5>'.$DATA['available_for'].'</h5>
                        </div>
                        <div class="clearfix"></div>
                        
                    </div>';
                    
                return $HTML;
               
            }

           
}

        ##################################################################
        ##################################################################
            function saveSchoolDetail()
            {
                $JSON_DATA=file_get_contents("php://input");
                $INPUT = json_decode($JSON_DATA);
                        $db=new Database();
                        $db->query('SELECT * FROM school_details WHERE usr_id=:usr_id');
                        $db->bind(':usr_id',$INPUT->USR_ID);
                        $DATA=$db->single();   
                        if($DATA==false){
                        $db->query('insert into school_details  
                                    ( usr_id,
                                      name,
                                      about,
                                      type,
                                      area,
                                      bedroom,
                                      food,
                                      rent_pyament,
                                      available_for

                                    ) 
                                    values(
                                        :usr_id,
                                      :name,
                                      :about,
                                      :type,
                                      :area,
                                      :bedroom,
                                      :food,
                                      :rent_pyament,
                                      :available_for
                                    )
                        ');
                        $db->bind(':usr_id',$INPUT->USR_ID);
                        $db->bind(':name',$INPUT->NAME);
                        $db->bind(':about',$INPUT->ABOUT);
                        $db->bind(':type',$INPUT->TYPE);
                        $db->bind(':area',$INPUT->AREA);
                        $db->bind(':bedroom',$INPUT->USR_ID);
                        $db->bind(':food',$INPUT->FOOD);
                        $db->bind(':rent_pyament',$INPUT->RENT);
                        $db->bind(':available_for',$INPUT->FOR);
                                if($db->execute()) {

                                    $data['SCHOOL_NAME']=$INPUT->NAME;
                                    $resp=array("DATA"=>$data,
                                    "MSG"=>"Updated",
                                    "ERROR"=>"NONE" );
                                    echo json_encode($resp);

                                }
                                else{

                                   
                                    $resp=array("DATA"=>null,
                                    "MSG"=>"Something Went Wrong",
                                    "ERROR"=>"ERROR" );
                                    echo json_encode($resp);

                                }
                        }
                        else {


                            $db->query('update school_details  set
                                        usr_id=:usr_id,
                                        name=:name,
                                        about=:about,
                                        type=:type,
                                        area=:area,
                                        bedroom=:bedroom,
                                        food=:food,
                                        rent_pyament=:rent_pyament,
                                        available_for=:available_for
                                   WHERE 
                                   usr_id=:usr_id
                        ');
                        $db->bind(':usr_id',$INPUT->USR_ID);
                        $db->bind(':name',$INPUT->NAME);
                        $db->bind(':about',$INPUT->ABOUT);
                        $db->bind(':type',$INPUT->TYPE);
                        $db->bind(':area',$INPUT->AREA);
                        $db->bind(':bedroom',$INPUT->USR_ID);
                        $db->bind(':food',$INPUT->FOOD);
                        $db->bind(':rent_pyament',$INPUT->RENT);
                        $db->bind(':available_for',$INPUT->FOR);
                        if($db->execute()) {

                            $data['SCHOOL_NAME']=$INPUT->NAME;
                            $resp=array("DATA"=>$data,
                            "MSG"=>"Updated",
                            "ERROR"=>"NONE" );
                            echo json_encode($resp);

                                }
                                else{
                                    
                                    $resp=array("DATA"=>null,
                                    "MSG"=>"Something Went Wrong",
                                    "ERROR"=>"ERROR" );
                                    echo json_encode($resp);
                                    
                                }

                        }
            }

    ###############################################
    #################################################
            function addMedia() {

                $db=new Database();
                
                if ($_FILES['files']['name'][0]!='') {
                    $myFile = $_FILES['files'];
                    $fileCount = count($myFile["name"]);
                    for ($i = 0; $i < $fileCount; $i++) {
                       
                        $path = $_FILES['files']['name'][$i];
                        $ext = pathinfo($path, PATHINFO_EXTENSION);

                        $IMAGE = array("JPG", "jpg");
                        if (in_array($ext, $IMAGE)) {
                             if(move_uploaded_file($_FILES['files']['tmp_name'][$i],"../uploads/". $_FILES["files"]['name'][$i]))
                             {
                                $db->query('INSERT into usr_upload
                                                                 (usr_id ,
                                                                 file_nme,
                                                                 file_type,
                                                                 album
                                                                  )
                                                         values (:usr_id ,
                                                                :file_nme,
                                                                :file_type ,
                                                                :album) ');
                                $db->bind(':usr_id',$_POST['USR_ID']);
                                $db->bind(':file_nme',$_FILES["files"]['name'][$i]);
                                $db->bind(':file_type',"IMAGE");
                                $db->bind(':album',$_POST['ALBUM']);
                                $db->execute();
                             }
                           
                        }
                       }
                    
                }

                if($_POST['url']!=''){
                    $db->query('INSERT into usr_upload
                                        (usr_id ,
                                        file_nme,
                                        file_type,
                                        album
                                        )
                                values (:usr_id ,
                                        :file_nme,
                                        :file_type ,
                                        :album) ');
                    $db->bind(':usr_id',$_POST['USR_ID']);
                    $db->bind(':file_nme',$_POST['url']);
                    $db->bind(':file_type',"URL");
                    $db->bind(':album',$_POST['ALBUM']);
                    $db->execute();
                } 
                // print_r($_POST);
                
            }
            #####################################
            ######################################

            function  getAlbum() {

                $JSON_DATA=file_get_contents("php://input");
                $INPUT = json_decode($JSON_DATA);
               
                $db=new Database();
                $db->query('SELECT * FROM usr_album WHERE usr_id=:usr_id');
                $db->bind(':usr_id',$INPUT->USR_ID);
                $DATA=$db->resultset();
                
                
                $HTML='<div class="col-sm-3 pl0 pt20"><h4>Album List</h4><select id="allAlbumList" class="form-control" required><option  value="">Select Album</option>';
                if(sizeof($DATA)>0) {
                    foreach($DATA as $ROW)
                    {
                        $HTML.='<option value="'.$ROW['ID'].'">'.$ROW['alm_nme'].'</option>';
                    }
                   
                } 
                $HTML.='</select></div>';
                $db->query('SELECT * FROM usr_album WHERE usr_id=:usr_id');
                $db->bind(':usr_id',$INPUT->USR_ID);
                $DATA=$db->resultset();
                $HTML2='<div class="col-sm-3 pl0 pt20"><h4>Your Are Currently Watching</h4><select id="viewAlbumList" class="form-control" required onchange="viewMoreAlbum()">';
                if(sizeof($DATA)>0) {
                    foreach($DATA as $ROW)
                    {
                        $HTML2.='<option value="'.$ROW['ID'].'">'.$ROW['alm_nme'].'</option>';
                    }
                   
                } 
                $HTML3="<h4>No Media Found</h4>";

                $db->query('SELECT * FROM usr_album WHERE usr_id=:usr_id');
                $db->bind(':usr_id',$INPUT->USR_ID);
                $DATA=$db->resultset();
                if(sizeof($DATA)>0) {
                    $HTML3= getAllFileBelongToAlbum($DATA[0]['ID'],$INPUT->USR_ID);
                } else{
                    $HTML3="<h4>No Media Found</h4>";
                }

                $HTML2.='</select></div>';
                $resp=array("DATA"=>$HTML,
                "DATA2"=>$HTML2,
                "DATA3"=>$HTML3,
                "MSG"=>"List Return ",
                "ERROR"=>"NONE" );
                echo json_encode($resp);
            }
            #####################################
            ######################################
            function addAlbum()
            {
                $db=new Database();


                $db->query('INSERT into usr_album (usr_id , alm_nme) values (:usr_id ,:alm_nme) ');
                $db->bind(':usr_id',$_POST['USR_ID']);
                $db->bind(':alm_nme',$_POST['album_name']);
                if($db->execute()) {
                     $resp=array("DATA"=>null,
                    "MSG"=>"Album Added",
                    "ERROR"=>"NONE" );
                    echo json_encode($resp);

                }
                else{  
                     $resp=array("DATA"=>null,
                    "MSG"=>"Something Went Wrong",
                    "ERROR"=>"ERROR" );
                    echo json_encode($resp);

                }
            }

###################################################################
###################################################################
function getAllFileBelongToAlbum($ID,$USR_ID) {
    $db=new Database();
    $db->query('SELECT * FROM usr_upload WHERE usr_id=:usr_id AND album=:album');
    $db->bind(':usr_id',$USR_ID);
    $db->bind(':album',$ID);
    $DATA=$db->resultset();
    $HTML="<h4>No Media is There</h4>";
    if(sizeof($DATA)>0)
    {
        $HTML='<div class="feature-item-wrapper m-right m-left wow zoomIn" data-wow-duration="2s">';
   foreach( $DATA as $ROW) {
       if($ROW['file_type']=='IMAGE') {
        $HTML.='<div class="col-sm-4 pt20 wow zoomIn" data-wow-duration="1s" id="AL'.$ROW['ID'].'">
        <div class="portfolio-item style-6">
        <a class="portfolio-anchor" href="#">
        <div class="portfolio-img">
        <img src="uploads/'.$ROW['file_nme'].'" alt="">
        </div>
        </a>
        </div>
        <span onclick="deleteMedia('.$ROW['ID'].','.$ROW['usr_id'].')">DEL<span>
        </div>';
       }
       if($ROW['file_type']=='URL') {
        $HTML.='<div class="col-sm-4 pt20 wow zoomIn" data-wow-duration="1s" id="AL'.$ROW['ID'].'">
        <div class="embed-responsive embed-responsive-16by9">
        <iframe class="embed-responsive-item" src="'.$ROW['file_nme'].'" allowfullscreen></iframe>
        </div>
        <span onclick="deleteMedia('.$ROW['ID'].','.$ROW['usr_id'].')">DEL<span>
        </div>';
       }
    }
    $HTML.='</div>';
    }
    return $HTML;
}
function deleteFile()
{
    $JSON_DATA=file_get_contents("php://input");
                $INPUT = json_decode($JSON_DATA);
                

        $db=new Database();
            $db->query('DELETE from usr_upload   WHERE ID=:ID  AND usr_id=:usr_id');
            $db->bind(':usr_id',$INPUT->USR_ID);
            $db->bind(':ID',$INPUT->ID);
    if($db->execute()) {
        $resp=array("DATA"=>null,
                "MSG"=>"files Removed",
                "ERROR"=>"NONE" );
                echo json_encode($resp);
    } 
    else {
        $resp=array("DATA"=>null,
        "MSG"=>"Something Went Wrong",
        "ERROR"=>"YES" );
        echo json_encode($resp);
    }
}
#####################################################
#######################################################
function viewMoreAlbum() {
    $JSON_DATA=file_get_contents("php://input");
                $INPUT = json_decode($JSON_DATA);

                $HTML=getAllFileBelongToAlbum($INPUT->ID,$INPUT->USR_ID);
                $resp=array("DATA"=>$HTML,
                "MSG"=>"Filter",
                "ERROR"=>"NONE" );
                echo json_encode($resp);
}
#####################################################
#######################################################
function makeAPost() {
      if($_POST['POST_TEXT']!=''){
        $db=new Database();
    $db->query('INSERT into usr_post
                        (usr_id ,
                        POST_TEXT,
                        POST_VIDEO
                        )
                values (:usr_id ,
                        :POST_TEXT,
                        :POST_VIDEO 
                        ) ');
    $db->bind(':usr_id',$_POST['USR_ID']);
    $db->bind(':POST_TEXT',$_POST['POST_TEXT']);
    $db->bind(':POST_VIDEO',$_POST['POST_VIDEO']);
    $db->execute();
    $ID=$db->lastInsertId();
    if($ID>0) {

        if ($_FILES['POST_IMAGES']['name'][0]!='') {
            $myFile = $_FILES['POST_IMAGES'];
            $fileCount = count($myFile["name"]);
            for ($i = 0; $i < $fileCount; $i++) {
               
                $path = $_FILES['POST_IMAGES']['name'][$i];
                $ext = pathinfo($path, PATHINFO_EXTENSION);

                $IMAGE = array("JPG", "jpg");
                if (in_array($ext, $IMAGE)) {
                     if(move_uploaded_file($_FILES['POST_IMAGES']['tmp_name'][$i],"../uploads/". $_FILES["POST_IMAGES"]['name'][$i]))
                     {
                        $db->query('INSERT into usr_upload
                                                         (usr_id ,
                                                         file_nme,
                                                         file_type,
                                                         up_for,
                                                         album
                                                          )
                                                 values (:usr_id ,
                                                        :file_nme,
                                                        :file_type ,
                                                        :up_for ,
                                                        :album) ');
                        $db->bind(':usr_id',$_POST['USR_ID']);
                        $db->bind(':file_nme',$_FILES["POST_IMAGES"]['name'][$i]);
                        $db->bind(':file_type',"IMAGE");
                        $db->bind(':up_for',"POST");
                        $db->bind(':album', $ID);
                        $db->execute();
                     }
                   
                }
               }
            
        }

    }
} 
         $resp=array("DATA"=>null,
        "MSG"=>"Post Return Sucessfully",
        "ERROR"=>"NONE" );
        echo json_encode($resp);
}
#################################################################
#################################################################
function getTimeline() {
        $JSON_DATA=file_get_contents("php://input");
        $INPUT = json_decode($JSON_DATA);

        $db=new Database();
        $db->query('SELECT * FROM usr_post WHERE usr_id=:usr_id ORDER BY ID DESC');
        $db->bind(':usr_id',$INPUT->USR_ID);
        
        $DATA=$db->resultset();
        if(sizeof($DATA)>0) {
            $HTML='';
            foreach($DATA as $ROW) {
                
                $HTML.='<div class="e-timeline-block infinity-item">
                <div class="e-timeline-content ">
                <div class="e-timeline-content-inner">
                <div class="e-timeline-date">
                <span>'.$ROW['POST_DATE'].'</span>

                </div>
                <div class="e-timeline-dot">
                <div class="e-timeline-dot-inner">
                <span></span>
                </div>
                </div>
                <div class="box_blog">
                <div class="col-sm-7 pl0">
                <div class="e-blog-post-des">
                
                <div class="e-blog-post-content pt10">
                <p class="mb10">'.$ROW['POST_TEXT'].'</p>
                </div><!-- blog-post-content -->
                <a class="e-blog-post-more" href="#">READ MORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>

                <div class="col-lg-12 pdr0 share-blog  mtb20">
                <div class="col-xs-4 pl0">
                <a href="#"><h6><i class="fa fa-star-o"></i>Star</h6></a>
                </div>
                <div class="col-xs-4 pl0">
                <a href="#"><h6><i class="fa fa-comment-o"></i>Comments</h6></a>
                </div>
                <div class="col-xs-4">
                <a href="#"><h6><i class="fa fa-share"></i>Share</h6></a>
                </div>
                </div>
                </div>
                </div>';

                $HTML.=getPostMedia($ROW['ID'],$ROW['usr_id']);

                    $HTML.='</div>
                    <div class="clearfix"></div>
                    </div>
                    </div>
                    </div>';
            }
            $resp=array("DATA"=>$HTML,
            "MSG"=>"Timeline Return",
            "ERROR"=>"NONE" );
            echo json_encode($resp);
        }
        
        
}

####################################################################
###################################################################
function getPostMedia($ID,$USR_ID) {
    $db=new Database();
    $db->query('SELECT * FROM usr_upload WHERE usr_id=:usr_id AND album=:album AND up_for=:up_for');
    $db->bind(':usr_id',$USR_ID);
    $db->bind(':album',$ID);
    $db->bind(':up_for','POST');
    $DATA=$db->resultset();
$HTML='';
    if(sizeof($DATA)>1) { 
        $HTML.='<div class="col-sm-5">
        <div class="e-blog-post-thumb">
                
            <div id="blogCarousel'.$ID.'" class="carousel slide" data-ride="carousel" data-interval="3000" data-pause="">

                <div class="carousel-inner">';
        $i=0;
        foreach($DATA as $ROW)
        {
           if($i==0) {
            $HTML.='<div class="item active">
            <img src="uploads/'.$ROW['file_nme'].'" alt="'.$ROW['file_nme'].'" style="width:100%;">
            </div>';
           } 
            else {

                $HTML.='<div class="item">
            <img src="uploads/'.$ROW['file_nme'].'" alt="'.$ROW['file_nme'].'" style="width:100%;">
            </div>'; 

           }
            $i++;
             }
             $HTML.='</div>

                
                <a class="left carousel-control" href="#blogCarousel'.$ID.'" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#blogCarousel'.$ID.'" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                  <span class="sr-only">Next</span>
                </a>
            </div> 
            
            </div>
        </div>';
       

    }
    if(sizeof($DATA)==1) { 
        $HTML.='<div class="col-sm-5">
        <div class="e-blog-post-thumb">
            <a href="#">
                <img src="uploads/'.$DATA[0]['file_nme'].'" alt="">
            </a>
            
        </div>
    </div>';

    }
    
    return $HTML;
}

?>