
<?php
include("../lib/config.php");
include("../lib/db.php");
include("../lib/infocular.php");
include("../lib/apiCommon.php");
include("../lib/sendMail.php");
include("../lib/getApi.php");
    
    ############################################
    ############################################
    function contactUs() {
        $JSON_DATA=file_get_contents("php://input");
        $INPUT = json_decode($JSON_DATA);
        $FNAME= $INPUT[0]->value;
        $LNAME= $INPUT[1]->value;
        $PHONE= $INPUT[2]->value;
        $EMAIL= $INPUT[3]->value;
        $SUBJECT= $INPUT[4]->value;
         $db=new Database();
        $db->query('INSERT INTO
                      icontact
                   (
                    fname,
                    lname,
                    email,
                    phone,
                    msg,
                    type
                   )
                   VALUES
                   (
                       :fname,
                       :lname,
                       :email,
                       :phone,
                       :msg,
                       :type
                   )
               ');
       $db->bind(':fname',$FNAME);
       $db->bind(':lname',$LNAME);
       $db->bind(':email',$EMAIL); 
       $db->bind(':phone',$PHONE); 
       $db->bind(':msg',$SUBJECT);
       $db->bind(':type',"QUERY");
      
        if($db->execute()){  
            $DATA['FNAME']=$FNAME;
            $DATA['LNAME']=$LNAME;
            $DATA['PHONE']=$PHONE;
            $DATA['EMAIL']=$EMAIL;
            $DATA['SUBJECT']=$SUBJECT;
            sendMail("QUERY",$DATA);
            
                $resp=array("DATA"=>null,
                            "MSG"=>"We Received Your Query , We Contact As Soon As",
                            "ERROR"=>"NONE" );
                echo json_encode($resp);      
        }
    }


     ############################################
    ############################################
    function subscription() {
        $JSON_DATA=file_get_contents("php://input");
        $INPUT = json_decode($JSON_DATA);
         $EMAIL= $INPUT[0]->value;
         $db=new Database();
        $db->query('INSERT INTO
                      icontact
                   (
                    email,
                    type
                   )
                   VALUES
                   (
                       :email,
                       :type
                   )
               ');
       $db->bind(':email',$EMAIL); 
       $db->bind(':type',"SUBSCRIPTION");
      
        if($db->execute()){  
           
            $DATA['EMAIL']=$EMAIL;
            // sendMail("SUBSCRIBE",$DATA);
                $resp=array("DATA"=>null,
                            "MSG"=>"You have successfully subscribe",
                            "ERROR"=>"NONE" );
                echo json_encode($resp);      
        }
    }





?>
