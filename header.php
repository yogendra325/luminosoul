<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="respjs/userAccount.js"></script>
  <script src="respjs/commonFunction.js"></script>
<header class="header header-v1 black sticky bg-white no-padding" id="fixed">
		<div class="header-bottom">
			<nav class="navbar navbar-default bootsnav">
				
				<div class="container">
					<div class="navbar-header">
						<button class="navbar-toggle collapsed" data-target="#navbar-menu" data-toggle="collapse" type="button"><span class="m-h-box"><span class="m-h-inner"></span></span></button>
						<div class="navbar-brand dark" style="position: absolute;">
							<img alt="" src="images/logo-black.png">
						</div>
						<div class="navbar-brand light">
							<img alt="" src="images/logo-black.png">
						</div>
					</div>
					<div class="collapse navbar-collapse" id="navbar-menu">
						<ul class="nav navbar-nav navbar-right menu_option" data-in="" data-out="fadeOut" id="primary-menu">
							<li class=" menu-item menu-item-has-children">
								<a   href="index.php">Home</a>
								
							</li>
							<li class="megamenu-fw menu-item menu-item-has-children menu-item-has-mega-menu">
								<a href="index.php#about">About Us</a>
								
							</li>
							<li class="megamenu-fw menu-item menu-item-has-children menu-item-has-mega-menu">
								<a href="index.php#service">Services</a>
								
							</li>
							

						

							<li class="menu-item menu-item-has-children">
								<a class="dropdown-toggle p-load-link" data-toggle="dropdown" href="index.php#blog">Blog</a>
								
							</li>
							
							<li class="menu-item menu-item-has-children">
								<a class="dropdown-toggle p-load-link" data-toggle="dropdown"  href="index.php#contact">Contact Us</a>
								
							</li>
							<div class="menu_button">
							<button class="register btn btn-sign btn-top"  data-toggle="modal" data-target="#signModal"><i style="" class="fa fa-sign-in"></i>Sign In</button>
							<button class="register btn btn-sign btn-top1" style="" data-toggle="modal" data-target="#registerModal">Register</button>
						</div>
						</ul>
					</div>
				</div>
			</nav>
		</div>
    </header>
    
    <!-- modal for login -->
    <div class="modal fade" id="signModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <form action="" method="post" id="signinform">
        <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Signin Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                <div class="modal-body">
                <input class="" id="text" type="text" name="email" placeholder=" Your Email Or Phone" required> 

                <input class=""  id="password" type="password" name="password" placeholder="Your Password" required>   

                </div>
                <div class="modal-footer">
				<div id="LOGIN_MSG"></div>
				<a onclick="forgetPassword()">Forget Password ?</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Signin</button>
                </div>
        </div>
        </form>
        </div>
        </div>

		<!-- modal for login -->

<!-- modal for registration -->
		<div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <form action="" method="post" id="registerationform">
			<div class="modal-content">
			<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Registration  Form</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="VER_MODAL1">
			<span aria-hidden="true">&times;</span>
			</button>
			</div>
			<div class="modal-body">
			<input class="" id="email" type="text" name="email" placeholder="Email" required> 
			<input class="" id="username" type="text" name="username" placeholder="Username" required> 
			<input class="" id="phone" type="text" name="phone" placeholder=" Phone" required> 
			<input class=""  id="Password" type="password" name="password" placeholder=" Password" required>   

			</div>

			<div class="modal-footer">
			<div id="REGISTER_MSG"></div>
			<button type="button" class="btn btn-secondary" data-dismiss="modal" >Close</button>
			<button type="submit" class="btn btn-primary">Register</button>
			</div>
			</div>

        </form>
<style>
#otpVerificationform {
    display: none;
}
</style>
		<form action="" method="post" id="otpVerificationform">
			<div class="modal-content">
			<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">OTP VERFICATION  </h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close" id="VER_MODAL2">
			<span aria-hidden="true">&times;</span>
			</button>
			</div>
			<div class="modal-body">
			<input class="" id="email_OTP" type="text" name="email_OTP" placeholder="Email OTP " required> 
			<i class="fa fa-random" aria-hidden="true" onclick="sendEmailOTPAgain()"></i>
			<div id="email_OTP_MSG"></div>
			<input class="" id="phone_OTP" type="text" name="phone_OTP" placeholder="PHONE OTP" required> 
			<i class="fa fa-random" aria-hidden="true" onclick="sendPhoneOTPAgain()"></i>
			<div id="phone_OTP_MSG"></div>

			</div>
			
			<div class="modal-footer">
			<div id="OTP_MSG"></div>
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">Verify</button>
			</div>
			</div>

        </form>

		
        </div>
        </div>