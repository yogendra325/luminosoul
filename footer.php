<footer class="footer">
		<div class="footer-top footer-bg-mountain overlay s-padding">
			<div class="container">
				<div class="row">
					<!-- float-area -->
					<div class="f-widget-wrapper m-left m-right">
						<div class="col-md-3 col-sm-6">
							<div class="f-widget">
								<h4 class="f-widget-title">About Excite</h4>
								<p>Excite is an all-in-one HTML5 Responsive Mobile Friendly Bootstrap Template, with over 160++ HTML pages and complete bundled website with 20+ elements/shortcodes included; ready to use website bundle with 9+ demos covering different aspects of business.</p>
							</div><!-- f-widget -->
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="f-widget">
								<h4 class="f-widget-title">recent blog</h4>
								<ul class="f-widget-recent-entries">
									<li><a href="#">What’s The Theory of Design ?</a></li>
									<li><a href="#">Latest News For Design 2017</a></li>
									<li><a href="#">Design Trend 2017</a></li>
									<li><a href="#">Latest News For Design</a></li>
									<li><a href="#">Principle &amp; The Theory of Design</a></li>
									<li><a href="#">Recent Design Trend &amp; Ideas</a></li>
								</ul><!-- f-widget-blog -->
							</div><!-- f-widget -->
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="f-widget">
								<h4 class="f-widget-title">Twitter Widget</h4>
								<ul class="twitter-widget">
									<li class="twitter-widget-item">
										<a href="#">
											<div class="twitter-icon">
												<i class="fa fa-twitter"></i>
											</div>
											<div class="twitter-widget-content">
													<p>Duis autem eum #webcode.com dolor hendrerit in vulputate velit</p>
												<span>23 minute ago</span>
											</div>
										</a>
									</li><!-- twitter-widget-item -->
									<li class="twitter-widget-item">
										<a href="#">
											<div class="twitter-icon">
												<i class="fa fa-twitter"></i>
											</div>
											<div class="twitter-widget-content">
												<p>Duis autem eum #webcode.com dolor hendrerit in vulputate velit</p>
												<span>3 Months ago</span>
											</div>
										</a>
									</li><!-- twitter-widget-item -->
								</ul><!-- twitter-widget -->
							</div><!-- f-widget -->
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="f-widget">
								<h4 class="f-widget-title">Instagram Widget</h4>
								
								<form class="subscribe" action="" method="post" id="subscriptionform">
									<input style="border: 0px !important;" type="email" name="email" placeholder="Your Email Address" required>
									<button><i class="fa fa-envelope"></i></button>
								</form>
							</div>
						</div>
					</div>
				</div><!-- f-widget-wrapper -->
			</div>
		</div><!-- footer-top -->
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="copyright">&copy; Copyright - <a href="javascript:void(0)">by LuminoSoul</a> and <a href="javascript:void(0)">Managed by Parallax Programmers Pvt. Ltd.</a> 
                	</p>
                	<ul class="social-profile white">
                		<li><a class="social-hex" href=""><i class="fa fa-facebook"></i></a></li>
                		<li><a class="social-hex" href=""><span class="fa fa-behance"></span></a></li>
                		<li><a class="social-hex" href=""><span class="fa fa-twitter"></span></a></li>
                		
                	</ul><!-- social-profile -->
				</div>
			</div>
		</div><!-- footer-bottom -->
	</footer>