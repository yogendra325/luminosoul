<?php

/**************************************************/
function iattach($arr,$proj_id,$sub_id="",$u_id,$for){
	$db=new Database();
	if(!empty($arr)){
			foreach($arr->orgName as $key=>$val){
				$orgName=$val;
				$newName=$arr->newName[$key];
				$iup_ext=pathinfo($newName, PATHINFO_EXTENSION);
			 	$size=$arr->size[$key];
				$db->query('INSERT INTO
					iupload
					(
						u_id,
                        proj_id,
						iup_prnt_id,
                        iup_sub_id,
						iup_type,
						iup_url,
						iup_size,
						iup_org_nme,
						iup_ext,
						iup_folder,
						iup_server,
						iup_IP
					)
					VALUES
					(
						:u_id,
                        :proj_id,
						:iup_prnt_id,
                        :iup_sub_id,
						:iup_type,
						:iup_url,
						:iup_size,
						:iup_org_nme,
						:iup_ext,
						:iup_folder,
						:iup_server,
						:iup_IP
					)
				');
				$db->bind(':u_id',$u_id);
                $db->bind(':proj_id',$proj_id);
				$db->bind(':iup_prnt_id',$proj_id);
                $db->bind(':iup_sub_id',$sub_id);
				$db->bind(':iup_type',$for);
				$db->bind(':iup_url',$newName);
				$db->bind(':iup_size',$size);
				$db->bind(':iup_org_nme',$orgName);
				$db->bind(':iup_ext',$iup_ext);
				$db->bind(':iup_folder',"attachments");
				$db->bind(':iup_server',$_SERVER["HTTP_HOST"]);
				$db->bind(':iup_IP',$_SERVER["REMOTE_ADDR"]);
                $db->execute();	
                
			}
		}
}
/**************************************************/



/**************************************************/
function iDelete(){
	
	$data=file_get_contents("php://input");
	$data = json_decode($data);	
	$u_id=$data->u_id;
	$table=$data->table;
	$field=$data->field;
	$primKey=$data->primKey;
	$db=new Database();
	$db->query('DELETE FROM 
							'.$table.' 
						WHERE 
							'.$field.'=:'.$field.'
						
					');

	$db->bind(':'.$field,$primKey);	
	if($db->execute()){
		echo json_encode(array(
				"msg"=>"Deleted Successfully!",
				"error"=>"none",
				"msg_type"=>"success"
			));
	}else{
		echo json_encode(array(
				"msg"=>"Delete not successfull!",
				"error"=>"error",
				"msg_type"=>"danger"
			));
	}
}
/**************************************************/


/**************************************************/
function iPriority($arr){
	$ipr_for=$arr["ipr_for"];
	$iproj_task=$arr["iproj_task"];
	$iproj_dev=$arr["iproj_dev"];
	$ipr_prnt_id=$arr["ipr_prnt_id"];	
    $proj_id=$arr["proj_id"];
	$u_id=$arr["u_id"];
	$db=new Database();
	$db->query('INSERT INTO
					ipriority
					(
						u_id,
                        proj_id,
						ipr_prnt_id,					
						ipr_for,
						iproj_dev,
						iproj_task
						
					)
					VALUES
					(
						:u_id,
                        :proj_id,
						:ipr_prnt_id,					
						:ipr_for,
						:iproj_dev,
						:iproj_task
					)
				');
	$db->bind(':u_id',$u_id);
    $db->bind(':proj_id',$proj_id);
	$db->bind(':ipr_prnt_id',$ipr_prnt_id);
	$db->bind(':ipr_for',$ipr_for);
	$db->bind(':iproj_dev',$iproj_dev);
	$db->bind(':iproj_task',$iproj_task);
	$db->execute();
}

/**************************************************/


/**************************************************/
function iCost($arr){

	$icost_type=$arr["icost_type"];
	$icost_rate_type=$arr["icost_rate_type"];
	$icost_rate=$arr["icost_rate"];
	$icost_for=$arr["icost_for"];
	$icost_prnt_id=$arr["icost_prnt_id"];
    $proj_id=$arr["proj_id"];
	$u_id=$arr["u_id"];
	$db=new Database();
	$db->query('INSERT INTO
					icost
					(
						u_id,
                        proj_id,
						icost_prnt_id,					
						icost_for,
						icost_type,
						icost_rate_type,
						icost_rate
					)
					VALUES
					(
						:u_id,
                        :proj_id,
						:icost_prnt_id,					
						:icost_for,
						:icost_type,
						:icost_rate_type,
						:icost_rate
					)
				');
	$db->bind(':u_id',$u_id);
    $db->bind(':proj_id',$proj_id);
	$db->bind(':icost_prnt_id',$icost_prnt_id);
	$db->bind(':icost_for',$icost_for);
	$db->bind(':icost_type',$icost_type);
	$db->bind(':icost_rate_type',$icost_rate_type);
	$db->bind(':icost_rate',$icost_rate);
	$db->execute();
	
}
/**************************************************/



/**************************************************/
function checkRecord($u_id,$tb){	
	
	$db=new Database();
	$db->query('SELECT COUNT(*) FROM '.$tb.' WHERE  u_id=:u_id');
	$db->bind(':u_id',$u_id);
	$count=$db->single();
	
	if($count["COUNT(*)"]==1){
		
		return true;
		
	}else{
		
		return false;
	}
}
/**************************************************/


/**************************************************/
function checkEmail($email){
	
	$db=new Database();
	$db->query('SELECT COUNT(*) FROM iuser WHERE  usr_email=:usr_email');
	$db->bind(':usr_email',$email);
	$count=$db->single();
	if($count["COUNT(*)"]>0){
		
		return true;
		
	}else{
		
		return false;
	}
}
/**************************************************/

function checkUsername($usr_nme){
	$db=new Database();
	$db->query('SELECT COUNT(*) FROM iuser WHERE  usr_nme=:usr_nme');
	$db->bind(':usr_nme',$usr_nme);
    $count=$db->single();
	if($count["COUNT(*)"]>0){
		
		return 1;
		
	}else{
		
		return 0;
	}
}
/**************************************************/

function checkPhone($phone){
	$db=new Database();
	$db->query('SELECT COUNT(*) FROM iuser WHERE  usr_ph=:usr_ph');
	$db->bind(':usr_ph',$phone);
    $count=$db->single();
	if($count["COUNT(*)"]>0){
		
		return 1;
		
	}else{
		
		return 0;
	}
}
/**************************************************/
function getID($email){
	
	$db=new Database();
	$db->query('SELECT u_id FROM iuser WHERE  usr_email=:usr_email');
	$db->bind(':usr_email',$email);
	$data=$db->single();
    return $data["u_id"];	
}
/**************************************************/



/**************************************************/
function updateStatus(){
    $data=file_get_contents("php://input");
    $data = json_decode($data);	
    $u_id=$data->u_id;
    $ists_sts=$data->ists_sts;
    $ists_prnt_id=$data->ists_prnt_id;
    $ists_for=$data->ists_for;
    $db=new Database();
    $db->query('INSERT INTO
                    istatus
                    (
                        u_id,
                        ists_sts,
                        ists_prnt_id,
                        ists_for,
                        ists_by
                    )
                    VALUES
                    (
                        :u_id,
                        :ists_sts,
                        :ists_prnt_id,
                        :ists_for,
                        :ists_by
                    )
                ');
    $db->bind(':u_id',$u_id);
    $db->bind(':ists_sts',$ists_sts);
    $db->bind(':ists_prnt_id',$ists_prnt_id);
    $db->bind(':ists_for',$ists_for);
    $db->bind(':ists_by',$u_id);
    if($db->execute()){
        //sendEmail TO client 
    }
}
/**************************************************/


/**************************************************/
function openTask($iproj_id,$taskFor){    
    $db=new Database();
    $db->query('SELECT 
                    count(*)                    
                FROM
                    itask
                WHERE 
                    itsk_prnt_id=:itsk_prnt_id
                    and
                    itsk_for=:itsk_for
                ');
    $db->bind(':itsk_prnt_id',$iproj_id);
    $db->bind(':itsk_for',$taskFor);
    $data=$db->single();
    return $data=$data["count(*)"];
   
  
}
/**************************************************/

/**************************************************/
function irelate($arr){
    $usr_prnt_id=$arr["usr_prnt_id"];
    $usr_child_id=$arr["usr_child_id"];
    $irel_nme=$arr["irel_nme"];
    $iby_id=$arr["iby_id"];
    $db=new Database();
    $db->query('INSERT INTO
                    usr_relation
                    (
                        usr_prnt_id,
                        usr_child_id,
                        irel_nme,
                        iby_id
                    )
                    VALUES
                    (
                        :usr_prnt_id,
                        :usr_child_id,
                        :irel_nme,
                        :iby_id
                    )
                ');
    $db->bind(':usr_prnt_id',$usr_prnt_id);
    $db->bind(':usr_child_id',$usr_child_id);
    $db->bind(':irel_nme',$irel_nme); 
    $db->bind(':iby_id',$iby_id);  
    if($db->execute()){
       return true;
    }
    
}
/**************************************************/


/**************************************************/
function getUserType($where,$cond){
    $db=new Database();   
    $db->query('SELECT usr_type from iuser where '.$where.'=:cond');    
    $db->bind(':cond',$cond);
    $data=$db->single();
    return $data["usr_type"];
    
}
/**************************************************/


/**************************************************/
function getuserInfo($u_id){
    $arr=array();
    $db=new Database();
    switch(getUserType('u_id',$u_id)){
            
                  case 'CLIENT'     :
        $db->query('SELECT                                        
                      usr_nme,
                      usr_email                        
                    FROM
                        iuser                  
                       
                    WHERE
                        u_id=:u_id
        ');
        $db->bind(':u_id',$u_id);
        $data=$db->single();                            
        $arr["client_nme"]=$data["usr_nme"];      
        $arr["client_email"]=$data["usr_email"];     
        return $arr;
        break;
       /************************************************/ 
        case 'CALLER'     :
        $db->query('SELECT                                        
                      usr_nme,
                      usr_email                        
                    FROM
                        iuser                  
                       
                    WHERE
                        u_id=:u_id
        ');
        $db->bind(':u_id',$u_id);
        $data=$db->single();                            
        $arr["caller_nme"]=$data["usr_nme"];      
        $arr["caller_email"]=$data["usr_email"];     
        return $arr;
        break;
       /************************************************/ 
        case 'INDIVIDUAL'  :

        $db->query('SELECT                                        
                    usr_info.usr_f_nme,                    
                    usr_info.usr_l_nme,
                    iuser.usr_email,
                    iuser.u_id
                FROM
                    usr_info
                LEFT JOIN
                    iuser
                ON
                  iuser.u_id=usr_info.u_id
                WHERE
                    iuser.u_id=:u_id
        ');
        $db->bind(':u_id',$u_id);
        $data=$db->single();                              
        $arr["fName"]=$data["usr_f_nme"];       
        $arr["lName"]=$data["usr_l_nme"];
        $arr["email"]=$data["usr_email"];   
        return $arr;
        break;
    }    
}
/**************************************************/


/**************************************************/
function iassign($arr){
    $u_id=$arr["u_id"];
    $iassign_for=$arr["iassign_for"];
    $iassign_to=$arr["iassign_to"];
    $iassign_by=$arr["iassign_by"];
    $iassign_prnt_id=$arr["iassign_prnt_id"];
    $iassign_title=$arr["iassign_title"];
    $attachments=$arr["attachments"];
    
    $db=new Database();
    $db->query('INSERT INTO
                iassign
                (
                   u_id,
                   iassign_for,
                   iassign_to,
                   iassign_by,
                   iassign_prnt_id
                )
                VALUES
                (
                    :u_id,
                    :iassign_for,
                    :iassign_to,
                    :iassign_by,
                    :iassign_prnt_id
                )
    ');
    $db->bind(':u_id',$u_id);
    $db->bind(':iassign_for',$iassign_for);
    $db->bind(':iassign_to',$iassign_to);
    $db->bind(':iassign_by',$iassign_by);
    $db->bind(':iassign_prnt_id',$iassign_prnt_id);
    if($db->execute()){
        
        switch($iassign_for){
            case 'PROJECT':
            newProjectEmail($arr);
            break;
                
            case 'TASK':
            newTaskEmail($arr);
            break;
        }
        
        
        
        
        return true;
    }else{
        return false;
    }
}

/**************************************************/


/**************************************************/
function getTaskInfo($id,$type){
    
    $db=new Database();
    switch($type){
        case 'PROJECT':
            $db->query('SELECT iproj_nme as P_TITLE,iproj_desc as DESCRIPTION FROM iproject WHERE iproj_id=:iproj_id');
            $db->bind(':iproj_id',$id);
            $data=$db->single();
            return $data;
            break;
        
        case 'TASK':
            
            $db->query('
                        SELECT 
                            itsk_nme as T_TITLE, 
                            itsk_desc as DESCRIPTION,
                            iproject.iproj_nme as P_TITLE
                        FROM 
                            itask 
                            
                        LEFT JOIN
                            iproject
                        ON
                            iproject.iproj_id=itask.proj_id
                        WHERE 
                            itsk_id=:itsk_id'
                      );
            $db->bind(':itsk_id',$id);
            $data=$db->single();
            return $data;
            break;
    }
}
/**************************************************/
/**************************************************/
function iChangeStatus(){
	
	$data=file_get_contents("php://input");
	$data = json_decode($data);	
   	$table=$data->table;
	$idF=$data->idF;
    $stsF=$data->stsF;
    $primKey=$data->primKey;
    $sts=$data->sts;
	
	$db=new Database();
      
	$db->query('UPDATE 
                    '.$table.' 
                SET
                    '.$stsF.'=:'.$stsF.'
                WHERE
                    '.$idF.'=:'.$idF.'
            ');
    $db->bind(':'.$idF,$primKey);
    $db->bind(':'.$stsF,$sts);
  
    
	if($db->execute()){
		echo json_encode(array(
				"msg"=>"Updated Successfully!",
                "error"=>"NONE",
                "msg_type"=>"success",
			));
	}else{
		echo json_encode(array(
				"msg"=>"Update not Successfully!",
                "error"=>"error",
                "msg_type"=>"unsuccess",
			));
	}
}
/**************************************************/


?>