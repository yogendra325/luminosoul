
<?php
include("../lib/config.php");
include("../lib/db.php");
include("../lib/infocular.php");
include("../lib/apiCommon.php");
include("../lib/getApi.php");
    
    ############################################
    ############################################
    function login() {

        $JSON_DATA=file_get_contents("php://input");
        $INPUT = json_decode($JSON_DATA);
        $db=new Database();
        $i=new infocular();
        $usr_email= $INPUT[0]->value;
        $usr_pass=$i->secureString($INPUT[1]->value);

        $db->query('SELECT COUNT(*) FROM iuser WHERE ( usr_email=:usr_email and usr_pass=:usr_pass  ) OR  ( usr_ph=:usr_email and usr_pass=:usr_pass  )');
        $db->bind(':usr_email',$usr_email);
        $db->bind(':usr_pass',$usr_pass);
		$count=$db->single();
		if($count["COUNT(*)"]==1){
            $db->query('SELECT * FROM iuser WHERE ( usr_email=:usr_email and usr_pass=:usr_pass  ) OR  ( usr_ph=:usr_email and usr_pass=:usr_pass  )');
            $db->bind(':usr_email',$usr_email);
            $db->bind(':usr_pass',$usr_pass);
            $data=$db->single();
            if($data['usr_sts']==1) {
                $resp=array("DATA"=>$data,
                    "MSG"=>"Login Successfully",
                    "ERROR"=>"NONE" );
                    echo json_encode($resp);
            }
            else{
                    $resp=array("DATA"=>null,
                    "MSG"=>"Your Account Is Not Activated Plz Contact To The Admin",
                    "ERROR"=>"YES" );
                    echo json_encode($resp);
            }
            
        }
        else{
           
            $resp=array("DATA"=>null,
                    "MSG"=>"No Record Found",
                    "ERROR"=>"YES" );
                    echo json_encode($resp);
        }
    }

function forgetPassword(){
        $obj = json_decode($_POST["myData"]);
        $usr_email=$obj->email;
        $db=new Database();
        $db->query('SELECT COUNT(*) FROM iuser WHERE  usr_email=:usr_email ');
        $db->bind(':usr_email',$usr_email);
		$count=$db->single();
		if($count["COUNT(*)"]==1){ 


            $db->query('SELECT * FROM iuser WHERE  usr_email=:usr_email ');
            $db->bind(':usr_email',$usr_email);
            $data=$db->single();

            if($data['usr_sts']==1) {
                $resp=array("DATA"=>null,
                "MSG"=>"You have An Email To Reset The Password Check It",
                "ERROR"=>"NONE" );
                echo json_encode($resp);
            }
            else{
                    $resp=array("DATA"=>null,
                    "MSG"=>"Your Account Is Not Activated Plz Contact To The Admin",
                    "ERROR"=>"YES" );
                    echo json_encode($resp);
            }
           
        }
        else{
            $resp=array("DATA"=>null,
            "MSG"=>"We have No Record with This Email",
            "ERROR"=>"YES" );
            echo json_encode($resp);
        }

}



function resetPassword(){
    $obj = json_decode($_POST["myData"]);
    $db=new Database();
    $i=new infocular();

        $db->query('SELECT COUNT(*) as total FROM iuser WHERE  usr_email=:usr_email ');
        $db->bind(':usr_email',$obj->email);
        $res=$db->single();
       if($res['total']==1) {

        $usr_pass=$i->secureString($obj->newPass);
        $db->query('UPDATE iuser set   usr_pass=:usr_pass  WHERE usr_email=:usr_email ');
        $db->bind(':usr_email',$obj->email);
        $db->bind(':usr_pass',$usr_pass);
        if($db->execute()) {
                    $resp=array("DATA"=>null,
                    "MSG"=>"Password Changed Successfully , Now You Can Login",
                    "ERROR"=>"NONE" );
                    echo json_encode($resp);
        }  else {
                    $resp=array("DATA"=>null,
                    "MSG"=>"Something Went Wrong",
                    "ERROR"=>"NONE" );
                    echo json_encode($resp);
        }

         


       }
       else{
        $resp=array("DATA"=>null,
            "MSG"=>"No Match Found For Update",
            "ERROR"=>"YES" );
            echo json_encode($resp);   
       }
   
}


########################################
#######################################

function register(){
    $obj = json_decode($_POST["myData"]);
    $db=new Database();
    $i=new infocular();
    if(checkEmail($obj->email)){
        $resp=array("DATA"=>null,
                    "MSG"=>"Email Is Already Is Register",
                    "ERROR"=>"YES" );
        echo json_encode($resp); 
        die();
    }

    if(checkUsername($obj->username)){
        $resp=array("DATA"=>null,
                    "MSG"=>"Username Is Already Is Register",
                    "ERROR"=>"YES" );
        echo json_encode($resp); 
        die();
    }
    if(checkPhone($obj->phone)){
        $resp=array("DATA"=>null,
                    "MSG"=>"Phone Is Already Is Register",
                    "ERROR"=>"YES" );
        echo json_encode($resp); 
        die();
    }

        $IP= $i->getIP();
    $usr_pass=$i->secureString($obj->password);
    $db->query('INSERT INTO
                       iuser
                    (
                        usr_nme,
                        usr_pass,
                        usr_email,
                        usr_ph,
                        usr_role,
                        usr_sts,
                        usr_ver,
                        usr_ip
                    )
                    VALUES
                    (
                        :usr_nme,
                        :usr_pass,
                        :usr_email,
                        :usr_ph,
                        :usr_role,
                        :usr_sts,
                        :usr_ver,
                        :usr_ip
                    )
                ');
        $db->bind(':usr_nme',$obj->username);
        $db->bind(':usr_pass',$usr_pass);
        $db->bind(':usr_email',$obj->email); 
        $db->bind(':usr_ph',$obj->phone); 
        $db->bind(':usr_role',"CLIENT");
        $db->bind(':usr_sts',0);
        $db->bind(':usr_ver',0); 
        $db->bind(':usr_ip',$IP); 
    if($db->execute()){
        $Last_Id= $db->lastInsertId(); 
         $db->query('INSERT INTO
                       usr_info
                    (
                        u_id,
                        usr_f_nme,
                        usr_m_nme,
                        usr_l_nme,
                        usr_gender
                    )
                    VALUES
                    (
                        :u_id,
                        :usr_f_nme,
                        :usr_m_nme,
                        :usr_l_nme,
                        :usr_gender
                    )
                ');
        $db->bind(':u_id',$Last_Id);
        $db->bind(':usr_f_nme',"null");
        $db->bind(':usr_m_nme',"null"); 
        $db->bind(':usr_l_nme',"null"); 
        $db->bind(':usr_gender',"null");
    if($db->execute()){ 
        $MAIL_OTP= rand(0,9).rand(0,9).rand(0,9).rand(0,9); 
        $PHONE_OTP= rand(0,9).rand(0,9).rand(0,9).rand(0,9); 
        // sendMail($obj->email,$MAIL_OTP);
        // sendOTP($obj->phone,$PHONE_OTP);
        $db->query('INSERT INTO
                      usr_act
                   (
                       u_id,
                       usr_email,
                       usr_mail_code,
                       usr_ph,
                       usr_ph_code
                   )
                   VALUES
                   (
                       :u_id,
                       :usr_email,
                       :usr_mail_code,
                       :usr_ph,
                       :usr_ph_code
                   )
               ');
       $db->bind(':u_id',$Last_Id);
       $db->bind(':usr_email',$obj->email);
       $db->bind(':usr_mail_code',$MAIL_OTP); 
       $db->bind(':usr_ph',$obj->phone); 
       $db->bind(':usr_ph_code',$PHONE_OTP);
        if($db->execute()){  
            $DATA['EMAIL']=$obj->email;
            $DATA['PHONE']=$obj->phone;
            
                $resp=array("DATA"=>$DATA,
                            "MSG"=>"We have Send OTP To Your Email and Phone Verify That",
                            "ERROR"=>"NONE" );
                echo json_encode($resp);      
        }
    }
    }
    else{
                 
    }
}
################################################
###############################################
    function sendEmailOTPAgain(){
        $obj = json_decode($_POST["myData"]);
        $db=new Database();
        $MAIL_OTP= rand(0,9).rand(0,9).rand(0,9).rand(0,9);
        // sendMail($obj->email,$MAIL_OTP);
        $db->query('UPDATE usr_act set   usr_mail_code=:usr_mail_code  WHERE usr_email=:usr_email ');
        $db->bind(':usr_email',$obj->email);
        $db->bind(':usr_mail_code',$MAIL_OTP);
        if($db->execute()) {
                $resp=array("DATA"=>$DATA,
                            "MSG"=>"Email Sended Again",
                            "ERROR"=>"NONE" );
                echo json_encode($resp);
        }
            
    }

################################################
###############################################

function sendPhoneOTPAgain(){
    $obj = json_decode($_POST["myData"]);
    $db=new Database();
    $PHONE_OTP= rand(0,9).rand(0,9).rand(0,9).rand(0,9);
    // sendOTP($obj->phone,$PHONE_OTP);
    $db->query('UPDATE usr_act set   usr_ph_code=:usr_ph_code  WHERE usr_ph=:usr_ph ');
    $db->bind(':usr_ph',$obj->phone);
    $db->bind(':usr_ph_code',$PHONE_OTP);
    if($db->execute()) {
            $resp=array("DATA"=>$DATA,
                        "MSG"=>"Phone OTP Sended Again",
                        "ERROR"=>"NONE" );
            echo json_encode($resp);
    }
        
}
################################################
###############################################

function verification(){
    $obj = json_decode($_POST["myData"]);
    $db=new Database();
//    print_r($obj);
//    email
//    emailOTP
//    phone
//    phoneOTP
   $db->query('SELECT COUNT(*) as total FROM usr_act WHERE  usr_email=:usr_email  and usr_mail_code=:usr_mail_code');
   $db->bind(':usr_mail_code',$obj->emailOTP);
   $db->bind(':usr_email',$obj->email);
   $res=$db->single();
  if($res['total']==0) {
    $resp=array("DATA"=>$DATA,
                "MSG"=>"Email OTP Not Matched",
                "ERROR"=>"YES" );
    echo json_encode($resp);
    exit(); 
  }

  $db->query('SELECT COUNT(*) as total FROM usr_act WHERE  usr_ph=:usr_ph  and usr_ph_code=:usr_ph_code');
   $db->bind(':usr_ph_code',$obj->phoneOTP);
   $db->bind(':usr_ph',$obj->phone);
   $res=$db->single();
  if($res['total']==0) {
    $resp=array("DATA"=>$DATA,
                "MSG"=>"Phone OTP Not Matched",
                "ERROR"=>"YES" );
    echo json_encode($resp);
    exit(); 
  }
 

  $db->query('UPDATE iuser set   usr_ver=:usr_ver  WHERE usr_ph=:usr_ph AND  usr_email=:usr_email');
    $db->bind(':usr_ph',$obj->phone);
    $db->bind(':usr_email',$obj->email);
    $db->bind(':usr_ver',1);
    if($db->execute()) {
            $resp=array("DATA"=>$DATA,
                        "MSG"=>"Verificatin Successfull Mail Sended To Admin For Activation Of Your Account",
                        "ERROR"=>"NONE" );
            echo json_encode($resp);
            $data=array("email"=>$obj->email,
                    "phone"=>$obj->phone );
                
            // mailToAdminForApproval();
    }
        
}



?>
