<?php
//error_reporting(1);




date_default_timezone_set(DEFAULT_TIME_ZONE);

class infocular{

	#############################################
	private $secureString;
	private $token=NULL;
	private $currentDomain=NULL;
	public $keys=NULL;
	public $values=NULL;
	public $qType=NULL; # query type // INSERT,UPDATE, DELETE
	public $fq=NULL; //# final query Strin
	public $fc=NULL; //final condition
	public $arr=NULL;
	public $wc=NULL;// where condition
	public $msg=NULL;
	public $error=NULL;
	public $lastID=NULL;
	public $qData=NULL; //result query Data;
	public $myjwt;
	public $validTokenMsg=null;
	public $exp=null;
	public $iss=null;
	public $token_type=null;
	public $DOMAIN=null;
	public $PUBLIC_KEY;
	public $counter=null;
	#############################################
	
	
	#############################################
    public function getIP(){
        
      
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
			$ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
    #############################################
    
	#############################################
	public function secureString($string,$salt=""){
		
		if(empty($string)){
			$this->error="STRIN PARAMETER MUST BE PROVIDED";
			return $this->error;
		}
		else{
		
			if(empty($salt)){
				$this->salt=SECRET_KEY;
			}
			$this->$string=$string;
			#STEP 1  CREATE MD5 OF THE PASS
			#STEP 2  CREATE HASH OF THE PASS
			#STEP 3  CREATE CRYPT OF THE PASS
			#STEP 4  CREATE HASH AGAIN WITH SALT_CONCATEDNATED OF THE PASS
			
			$this->secureString=md5($this->$string);
			$this->secureString=hash('sha512',$this->$string);
			$this->secureString=crypt($this->$string,$this->salt);
			$this->secureString=hash('sha512',$this->$string.$this->salt);
			return $this->secureString;
		}
	}
	#############################################
	

	
	
	#########################################
	public function genToken($payLoad,$secretKey){		
				
		$this->token=null;
		$this->token=array(
			#HS384
			'iat'  => date("Y-m-d"),         	// Issued at: time when the token was generated        	
        	'iss'  => DOMAIN,       	// Issuer			
        	'exp'  => date('Y-m-d',strtotime("+".TOKEN_VALID_DAY." day")),// Expire
        	'data' => $payLoad
		);
		return JWT::encode($this->token,base64_encode($secretKey),'HS256');	
	}
	#########################################
	
	######################
	/*************FUNCTION TO VALIDATE ISSUES JWT TOKEN STARTS************/
	public function validateToken($token){
		
		$this->myjwt=null;
		$this->exp=null;
		$this->iss=null;
		$this->token_type=null;
		$this->DOMAIN=null;
		$this->PUBLIC_KEY=null;
		$this->validTokenMsg=null;
		
		$this->myjwt=JWT::decode($token,base64_encode($this->secureString(SECRET_KEY)),array('HS256'));
		
		$this->exp=$this->myjwt->exp;
		$this->iss=$this->myjwt->iss;
		$this->token_type=$this->myjwt->data->token_type;
		$this->DOMAIN=$this->myjwt->data->DOMAIN;
		$this->PUBLIC_KEY=$this->myjwt->data->PUBLIC_KEY;
		
	
		if(time()>=strtotime($this->exp)){
			
			$this->validTokenMsg="EXPIRED";
			
		}else{
			
			if($this->token_type!="RESIDENCY" || $this->DOMAIN!=DOMAIN || $this->PUBLIC_KEY!=PUBLIC_KEY){
				
				 $this->validTokenMsg="INVALID";
				
			}else if($this->token_type=="RESIDENCY" && $this->DOMAIN==DOMAIN && $this->PUBLIC_KEY==PUBLIC_KEY){
				
				 $this->validTokenMsg="VALID";
			}
		}
		return $this->validTokenMsg;
		
	}
	/*************FUNCTION TO VALIDATE ISSUES JWT TOKEN ENDS************/
	######################
	
	
	#########################################
	public function checkDomain(){
		
		$this->currentDomain=$_SERVER['SERVER_NAME'];
		if(DOMAIN==$this->currentDomain){
				$this->currentDomain=null;	
				return "true";
			}else{
				$this->currentDomain=null;	
				return "false";
			}
	}
	#########################################
	
	
	#########################################
	public function bQuery($arr="",$tb,$qType,$cond=""){
		
		if(!empty($cond)){
			foreach($cond as $key=>$val){
				$this->wc.=$key."=:".$key." AND ";
			}
			$this->wc=substr($this->wc,0,-4);
		}
		
		if(sizeof($arr)>0){
			
			switch($qType){
					
				case "CREATE":
								foreach($arr as $key=>$val)
								{
									$this->keys.=$key.",";
									$this->values.=":".$key.",";				
								}
								$this->keys=substr($this->keys,0,-1);
								$this->values=substr($this->values,0,-1);
								$this->fq='INSERT INTO '.$tb.'('.$this->keys.') VALUES('.$this->values.')';
				
								$this->equery($arr,$this->fq,$qType,$cond);
								break;
					
					
				case "UPDATE":
								foreach($arr as $key=>$val)
								{
									$this->keys.=$key."=:".$key.",";
												
								}
								$this->keys=substr($this->keys,0,-1);
								if(empty($this->wc)){
									$this->fq='UPDATE '.$tb.' SET '.$this->keys;
								}else{
									$this->fq='UPDATE '.$tb.' SET '.$this->keys." WHERE ".$this->wc;
								}
				
								$this->equery($arr,$this->fq,$qType,$cond);
								break;
					
					
				case "READ":
								foreach($arr as $key=>$val)
								{
									$this->keys.=$val.",";
									$this->values.=$key."=:".$val." AND ";	
												
								}
								$this->keys=substr($this->keys,0,-1);
								$this->values=substr($this->values,0,-4);
					
								if(empty($this->wc)){
									$this->fq='SELECT '.$this->keys.' FROM '.$tb;
								}else{
									$this->fq='SELECT '.$this->keys.' FROM '.$tb." WHERE ".$this->wc;
								}	
								$this->equery($arr,$this->fq,$qType,$cond);
								break;	
					
					
				case "DELETE":
								
								$this->keys=substr($this->keys,0,-1);
					
								if(empty($this->wc)){
									$this->fq='DELETE FROM '.$tb;
								}else{
									$this->fq='DELETE FROM '.$tb." WHERE ".$this->wc;
								}		
								$this->equery('',$this->fq,$qType,$cond);
								$this->keys=NULL;
								$cond=NULL;
								$this->wc=NULL;
								break;	
			}
			
		$arr=NULL;
		$tb=NULL;
		$qType=NULL;
		$cond=NULL;
		$this->keys=NULL;
		$this->values=NULL;
		$this->fq=NULL;
		
		return array(			
			"error"=>$this->error,
			"last"=>$this->lastID,
			"data"=>$this->qData
		);
			
		}else{
			
			return "Empty Array Given";
		}

		
		
	}
	#########################################
	
	#########################################
	public function equery($arr="",$query,$qType,$cond=""){## executes the query and returns result;
		
		
		$this->db=new Database();
		$this->db->query($query);
		
		switch($qType){
				
			case "CREATE":				
							foreach($arr as $key=>$val){
				
								$this->db->bind(':'.$key,$val);

							}
				
							if($this->db->execute()){	
								$this->error="NONE";
								$this->lastID=$this->db->lastInsertId();
								
							}else{
								$this->error="SOMETHING WENT WRONT";
								$this->lastID=NULL;
							}
							break;
				
				
			case "READ":				
							foreach($cond as $key=>$val){
				
								$this->db->bind(':'.$key,$val);

							}
				
							if($this->db->execute()){	
								
								
								$this->qData=$this->db->resultset();
								$this->error="NONE";								
								
							}else{
								$this->error="SOMETHING WENT WRONT";
								$this->lastID=NULL;
							}
							break;
			
				
		
			case "DELETE":
		
							foreach($cond as $key=>$val){
				
								$this->db->bind(':'.$key,$val);

							}
				
							if($this->db->execute()){	
								$this->error="NONE";
								$this->lastID=NULL;
								
							}else{
								$this->error="SOMETHING WENT WRONT";
								$this->lastID=NULL;
							}
							break;
			
			case "UPDATE":
		
							foreach($arr as $key=>$val){
				
								$this->db->bind(':'.$key,$val);

							}
							foreach($cond as $key=>$val){
				
								$this->db->bind(':'.$key,$val);

							}
				
							if($this->db->execute()){	
								$this->error="NONE";								
								
							}else{
								$this->error="SOMETHING WENT WRONT";
								$this->lastID=NULL;
							}
							break;
		}
		
		$arr=NULL;
		$query=NULL;
		
	}
	#########################################
	
	
	#########################################
	function iCreate($arr,$tb){
		
		
		foreach($arr as $key=>$val)
		{
			$this->keys.=$key.",";
			$this->values.=":".$key.",";				
		}
		$this->keys=substr($this->keys,0,-1);
		$this->values=substr($this->values,0,-1);
		$this->fq='INSERT INTO '.$tb.'('.$this->keys.') VALUES('.$this->values.')';
		
		$this->db=new Database();
		$this->db->query($this->fq);
		foreach($arr as $key=>$val){
				
			$this->db->bind(':'.$key,$val);

		}

		if($this->db->execute()){	
			$this->error="NONE";
			$this->lastID=$this->db->lastInsertId();

		}else{
			$this->error="ERROR";
			$this->lastID=NULL;
		}
		$this->keys=NULL;
		$this->values=NULL;
		$this->fq=NULL;
		$arr=NULL;
		$tb=NULL;
		return (object) array(			
			"error"=>$this->error,
			"last"=>$this->lastID,
			"data"=>$this->qData,
			"last"=>$this->lastID
		);
		
	}
	#########################################
	
	#########################################
	public function check_uid($u_id){
		
		$this->db=new Database();
		$this->db->query("SELECT COUNT(*) FROM iuser WHERE  u_id=:u_id");	
		$this->db->bind(':u_id',$u_id);
		$this->counter=$this->db->single();

		if($this->counter["COUNT(*)"]==1){		
			$this->counter=null;
			return true;

		}else{

			return false;

		}
	}
	#########################################
	
}
	
?>