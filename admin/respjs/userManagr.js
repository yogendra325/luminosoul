
////////////////////////////////////////////
///////////////////////////////////////////

function acceptRequest(ID)
{  
  if (confirm("Do You Want Approve Request")) {
   
    var url = "resp/userManager.php/acceptRequest"; 
    var dataPost = { "ID": ID  };
    var dataString = JSON.stringify(dataPost);
    
    $.ajax({
       type: "POST",
       url: url,
       data: dataString , // serializes the form's elements.
       success: function(data){
      var JSON_DATA = JSON.parse(data);
      if(JSON_DATA.ERROR=="NONE") {
         $( "#"+ID ).fadeOut( "slow" ); 
       }  
       }
     });
  }

}
function rejectRequest(ID)
{
  
  if (confirm("Do You Want Reject Request")) {
   
    var url = "resp/userManager.php/rejectRequest"; 
    var dataPost = { "ID": ID  };
    var dataString = JSON.stringify(dataPost);
    
    $.ajax({
       type: "POST",
       url: url,
       data: dataString , // serializes the form's elements.
       success: function(data){
      var JSON_DATA = JSON.parse(data);
      if(JSON_DATA.ERROR=="NONE") {
         $( "#"+ID ).fadeOut( "slow" ); 
       }  
       }
     });
  }
  
 

}

////////////////////////////////////////////
///////////////////////////////////////////
