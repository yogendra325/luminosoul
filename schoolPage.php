<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="UTF-8">
	<title>LuminoSoul</title>
	<meta content="" name="keywords">
	<meta content="" name="description">
	<meta content="" name="author">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	
	<link href="css/lightcase.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="fonts/font.css">
	<link rel="stylesheet" type="text/css" href="palyfair_Dispaly/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="amaranth/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="gentium_Basic/stylesheet.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="css/icofont.css" rel="stylesheet">
	<link href="css/animsition.min.css" rel="stylesheet">
	<link href="css/jquery.nstSlider.min.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/swiper.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/animate.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootsnav.css" rel="stylesheet">
	<link href="css/shortcode.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<script src="js/ga.js" type="text/javascript">
	</script>
</head>
<body class="demo-landing-page p-load-animation" onload="getTimeline()">
	<div class="style-switcher">
        <div class="switcher-toggle">
			<i class="fa fa-cog" aria-hidden="true"></i>
			<span>Options</span>
		</div>
        
        <div class="style-switcher-container">
            <div class="style-switcher-inner">
               
                <div class="optionpart">
                                <aside class="col-md-12 sidebar">
					<div class="m-left m-right sidebar-widget-wrapper">
						<div class="widget pdt20">
							<form role="search" method="get" class="search-form"> 
								<label>
									<span class="screen-reader-text">Search....</span> 
									<input type="search" class="search-field" placeholder="Search...." value="" name="s" autocomplete="off"> 
								</label>
								<input type="submit" class="search-submit" value="Search">
							</form>
						</div><!-- widget -->
						
						<div class="widget categorie-widget">
							<h3 class="widget-title">Sorting</h3>
							<!-- <ul>
								<li><a href="#">Delhi Public Sschool</a></li>
								<li><a href="#">D.A.V Sschool</a></li>
								<li><a href="#">Greenfield Public School</a></li>
								<li><a href="#">Loreto Convent School</a></li>
							</ul> -->
							<div class="checkbox">
								<label><input type="checkbox" value="">Delhi Public Sschool</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">D.A.V Sschool</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Greenfield Public School</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Loreto Convent School</label>
							</div>
						
						</div><!-- widget -->
						
						<!-- widget -->
						
						<div class="widget categorie-widget">
							<h3 class="widget-title">Medium:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Hindi</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">English</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Board:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">ICSE</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">CBSE</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">OPEN</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">OTHER</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Boarding:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Day school</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Boarding</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Both</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">School type:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Boys</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Grils</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">co-ed</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Nationality:</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">International</label>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">National</label>
							</div>
						</div><!-- widget -->
						<div class="widget categorie-widget">
							<h3 class="widget-title">Categories</h3>
							
							<div class="checkbox">
								<label><input type="checkbox" value="">Graphic Design</label><span style="padding-left: 5px;">(3)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Web Design</label><span style="padding-left: 5px;">(5)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Photography</label><span style="padding-left: 5px;">(2)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Marketing</label><span style="padding-left: 5px;">(4)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Business Consult</label><span style="padding-left: 5px;">(1)</span>
							</div>
							<div class="checkbox">
								<label><input type="checkbox" value="">Domain &amp; Hosting</label><span style="padding-left: 5px;">(3)</span>
							</div>
						</div><!-- widget -->
						
						<div class="widget tag-widget">
							<h3 class="widget-title">TAGS</h3>
							<div class="tagcloud">
								<a href="#">web</a>
								<a href="#">graphic</a>
								<a href="#">design</a>
								<a href="#">marketing</a>
								<a href="#">seo</a>
								<a href="#">logo</a>
							</div>
						</div><!-- widget -->
					</div><!-- sidebar-widget-wrapper -->
				</aside>   
                   
                </div>
            </div>
        </div>
    </div>
	

<?php include("header.php"); ?>

<script src="respjs/clientService.js"></script>
<script src="respjs/blogService.js"></script>
<section class="carousel-new-s" style="background-color: ;">
	<div class="p-caption demo-banner-content-inner js-tilt" style="transform: perspective(1000px) rotateX(0deg) rotateY(0deg); will-change: transform;">			
	<h3 class="text-white"  style="color: #ffffff;font-size: 50px;">DELHI PUBLIC SCHOOL </h3>
	
	  </div>
	<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="3000" data-pause="">
    
    <!-- Wrapper for slides -->
    <div class="carousel-inner block ">
      <div class="item active">
        <img src="images/slide-1.JPG" class="slide_img" alt="Los Angeles">
       
                 
      </div>

      <div class="item">
        <img src="images/slide-2.jpg" class="slide_img" alt="Chicago">
       
      </div>
    
      <div class="item">
        <img src="images/slide-3.jpg" class="slide_img" alt="New york">

      </div>
    </div>

    <!-- Left and right controls -->
      <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  </div>


</section>
	<div class="container-fluid" style="padding: 0px;">
		<div class="row mainrow">
			<!-- <div class="col-sm-1"></div> -->
			<div class="col-sm-10">
			<div id='cssmenu' style="margin-top: 20px;">
				<ul>
				
   					<li class='tablinks active' id="timeline" onclick="getTimeline()"><a href='javascript:void(0)'>Timeline</a></li>
   					<li class='tablinks' id="details" onclick="getSchoolDetails()"><a href='javascript:void(0)'>Details</a></li>
   					<li class='tablinks' id="photos"  onclick="getAlbum()"><a href='javascript:void(0)'>Gallery</a></li>
   					<li class='tablinks' id="products"><a href='javascript:void(0)'>Products</a></li>
   					<li class='tablinks' id="appoinment"><a href='javascript:void(0)'>Book An Appoinment</a></li>
   					<li class='tablinks' id="admission"  onclick="getAdmissionQuestion()"><a href='javascript:void(0)'>Admission</a></li>
   					
   					<li class='tablinks' id="summercamp"><a href='javascript:void(0)'>Summer Camp</a></li>
   				</ul>
			</div>
	<section class="content s-padding sidebar-left" id="1">
		<div class="list-tab">
			<div class="row">
			
			<div class="row">
				 <form action="" method="post" id="postform">
				  
				<div class="col-md-4"> <input class="" id="POST_TEXT" type="text" name="POST_TEXT" placeholder="What Is In Your Mind" required></div>
				<div class="col-md-2"><input type="file"  id="POST_IMAGES" name="POST_IMAGES[]" multiple="multiple" accept="image/*" /></div>
				<div class="col-md-4"> <input class="" id="POST_VIDEO_URL" type="url" name="POST_VIDEO" placeholder="Your Video URL"></div>
				<div class="col-md-2"><button type="submit" class="btn btn-primary" id="POST_BUTTON">POST</button></div>
				<div id="POST_MSG"></div>
				</form>
				</div>
				
				
				<main class="col-md-12 main-content">
					<div class="e-timeline infinityselctor">
					

			<div id="timelineWall"></div>					
						
					</div>
					<div class="pagination infinity-links">
						<a href='blog-timeline-with-sidebar2.html' class="page-naumber">1</a>
					</div>
				</main>
				
			</div>
	</section>
	<div class="row" id="2" style="padding: 20px;">
						<div class="col-sm-12" style="width:100%;">

						
						
						
		
						<div id="school_details">
							
							
							</div>
  								
  									<h3 class="pt20">Detail</h3>
  									<!-- <hr style="border-top: 2px solid #3b74b8;    margin-top: 5px;"> -->
  									
  									
								
  									<br>
  									<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14011.171323662391!2d77.0473905!3d28.6059912!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc15d793aacb72697!2sDelhi+Public+School!5e0!3m2!1sen!2sin!4v1523018963347" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
  									<h3 style="margin-top:20px;">Comments</h3>
									<form action="/action_page.php" id="usrform">
  										<div class="input-group" style="margin-bottom: 0px;">
									<span class="input-group-addon"><i class="fa fa-comments fa fa-yellow" aria-hidden="true"></i></span>
									<textarea row="2" id="subject" name="subject" placeholder="Write something.." style="margin: 0;color: #000"></textarea>
								</div>
									<div class="text-right">
  										<!-- <button style="margin-left:30px" class="btn1">Submit</button> -->
  										<button style="margin-top: 20px;" class="btn btn1">Submit</button>
  									</div>
								</form>
								<hr>
								<div class="media">
									<div class="media-left">
										<img src="images/content-img/detail-2.jpg" alt="Lorem Pixel" class="media-object" style="width:60px">
									</div>
								<div class="media-body">
									<h4 class="media-heading e-comments-item-author">John</h4>
									<a href="#" class="e-comment-date"> 5 Feb / 2017</a>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquaLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
									<div class="col-md-6 col-sm-6 pdr0 share-blog  mtb20">
										<div class="col-xs-4 pl0">
											<a href="#"><h6><i class="fa fa-star-o"></i>Star</h6></a>
										</div>
									<div class="col-xs-4 pl0">
										<a href="#"><h6><i class="fa fa-comment-o"></i>Comments</h6></a>
									</div>
									<div class="col-xs-4">
										<a href="#"><h6><i class="fa fa-share"></i>Share</h6></a>
									</div>
								</div>
								<div class="clearfix"></div>
								<hr>
								<div class="media">
								<div class="media-left">
								<img src="images/content-img/detail-2.jpg" alt="Lorem Pixel" class="media-object" style="width:60px">
								</div>
								<div class="media-body">
								<h4 class="media-heading e-comments-item-author">John</h4>
								<a href="#" class="e-comment-date"> 5 Feb / 2017</a>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquaLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
								<div class="col-md-6 col-sm-6 pdr0 share-blog  mtb20">
								<div class="col-xs-4 pl0">
								<a href="#"><h6><i class="fa fa-star-o"></i>Star</h6></a>
								</div>
								<div class="col-xs-4 pl0">
								<a href="#"><h6><i class="fa fa-comment-o"></i>Comments</h6></a>
								</div>
								<div class="col-xs-4">
								<a href="#"><h6><i class="fa fa-share"></i>Share</h6></a>
								</div>
								</div>
								<div class="clearfix"></div>
								</div>
								</div>
								</div>

								</div>
  <hr>
  								</div>
  	</div>
	<div class="row summercamp_content" id="3">
		<section class="shop">
			<div class="shop-wrapper m-left m-right infinityselctor-manaul">
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-list style-2 shadow">
							<div class="product-preview">
								<a href="#"><img src="images/product/product-18.jpg" alt=""></a>
							</div>
							<span class="product-flash">sale</span>
							<div class="product-list-content">
								<div class="product-list-content-top">
									<div class="product-name">
										<h4 class="product-title"><a href="#">Sample T-shirt</a></h4>
										<a href="" class="brand">Brand Name</a>
									</div>
									<span class="price">
										<span class="main-price">$22</span>
									</span>
								</div>
								<div class="product-list-content-bottom">
									<div class="product-list-content-bottom-top">
										<div class="product-content-size">
											<h4>sizes</h4>
											<ul>
												<li>xs</li>
												<li>s</li>
												<li>m</li>
												<li>l</li>
												<li>xl</li>
												<li>xxl</li>
											</ul>
										</div>
										<div class="product-content-color">
											<h4>Colors</h4>
											<ul>
												<li class="black"></li>
												<li class="white"></li>
												<li class="blue"></li>
												<li class="yellow"></li>
											</ul>
										</div>
									</div>
									<div class="product-cart-btn text-center">
										<a href="#"><span class="product-cart-btn-label"><i class="fa fa-cart-plus"></i> Add To Cart</span></a>
									</div>
								</div>
							</div>
						</div><!-- product -->
					</div>
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-list style-2 shadow">
							<div class="product-preview">
								<a href="#"><img src="images/product/product-18.jpg" alt=""></a>
							</div>
							<span class="product-flash">sale</span>
							<div class="product-list-content">
								<div class="product-list-content-top">
									<div class="product-name">
										<h4 class="product-title"><a href="#">Sample T-shirt</a></h4>
										<a href="" class="brand">Brand Name</a>
									</div>
									<span class="price">
										<span class="main-price">$22</span>
									</span>
								</div>
								<div class="product-list-content-bottom">
									<div class="product-list-content-bottom-top">
										<div class="product-content-size">
											<h4>sizes</h4>
											<ul>
												<li>xs</li>
												<li>s</li>
												<li>m</li>
												<li>l</li>
												<li>xl</li>
												<li>xxl</li>
											</ul>
										</div>
										<div class="product-content-color">
											<h4>Colors</h4>
											<ul>
												<li class="black"></li>
												<li class="white"></li>
												<li class="blue"></li>
												<li class="yellow"></li>
											</ul>
										</div>
									</div>
									<div class="product-cart-btn text-center">
										<a href="#"><span class="product-cart-btn-label"><i class="fa fa-cart-plus"></i> Add To Cart</span></a>
									</div>
								</div>
							</div>
						</div><!-- product -->
					</div>
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-list style-2 shadow">
							<div class="product-preview">
								<a href="#"><img src="images/product/product-19.jpg" alt=""></a>
							</div>
							<span class="product-flash">sale</span>
							<div class="product-list-content">
								<div class="product-list-content-top">
									<div class="product-name">
										<h4 class="product-title"><a href="#">T-shirt Shaped</a></h4>
										<a href="" class="brand">Brand Name</a>
									</div>
									<span class="price">
										<span class="compare-price">$29</span>
										<span class="main-price">$22</span>
									</span>
								</div>
								<div class="product-list-content-bottom">
									<div class="product-list-content-bottom-top">
										<div class="product-content-size">
											<h4>sizes</h4>
											<ul>
												<li>xs</li>
												<li>s</li>
												<li>m</li>
												<li>l</li>
												<li>xl</li>
												<li>xxl</li>
											</ul>
										</div>
										<div class="product-content-color">
											<h4>Colors</h4>
											<ul>
												<li class="black"></li>
												<li class="white"></li>
												<li class="blue"></li>
												<li class="yellow"></li>
											</ul>
										</div>
									</div>
									<div class="product-cart-btn text-center">
										<a href="#"><span class="product-cart-btn-label"><i class="fa fa-cart-plus"></i> Add To Cart</span></a>
									</div>
								</div>
							</div>
						</div><!-- product -->
					</div>
					<!-- <div class="col-md-3 col-sm-4 infinity-item-manaul">
						<div class="product-list shadow">
							<div class="product-preview">
								<a href="#"><img src="images/product/product-3.jpg" alt=""></a>
							</div>
							<div class="product-list-content">
								<h4 class="product-title"><a href="#">Blue T-shirt <span class="brand">(Brand Name)</span></a></h4>
								<div class="rating-star-area"></div>
								<span class="price">
									<span class="main-price">$25</span>
								</span>
								<div class="product-cart-btn">
									<a href="#"><span class="product-cart-btn-label"><i class="fa fa-shopping-cart"></i> Add To Cart</span></a>
								</div>
							</div>
						</div>
					</div> -->
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-list style-2 shadow">
							<div class="product-preview">
								<a href="#"><img src="images/product/product-18.jpg" alt=""></a>
							</div>
							<span class="product-flash">sale</span>
							<div class="product-list-content">
								<div class="product-list-content-top">
									<div class="product-name">
										<h4 class="product-title"><a href="#">Sample T-shirt</a></h4>
										<a href="" class="brand">Brand Name</a>
									</div>
									<span class="price">
										<span class="main-price">$22</span>
									</span>
								</div>
								<div class="product-list-content-bottom">
									<div class="product-list-content-bottom-top">
										<div class="product-content-size">
											<h4>sizes</h4>
											<ul>
												<li>xs</li>
												<li>s</li>
												<li>m</li>
												<li>l</li>
												<li>xl</li>
												<li>xxl</li>
											</ul>
										</div>
										<div class="product-content-color">
											<h4>Colors</h4>
											<ul>
												<li class="black"></li>
												<li class="white"></li>
												<li class="blue"></li>
												<li class="yellow"></li>
											</ul>
										</div>
									</div>
									<div class="product-cart-btn text-center">
										<a href="#"><span class="product-cart-btn-label"><i class="fa fa-cart-plus"></i> Add To Cart</span></a>
									</div>
								</div>
							</div>
						</div><!-- product -->
					</div>
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-list style-2 shadow">
							<div class="product-preview">
								<a href="#"><img src="images/product/product-18.jpg" alt=""></a>
							</div>
							<span class="product-flash">sale</span>
							<div class="product-list-content">
								<div class="product-list-content-top">
									<div class="product-name">
										<h4 class="product-title"><a href="#">Sample T-shirt</a></h4>
										<a href="" class="brand">Brand Name</a>
									</div>
									<span class="price">
										<span class="main-price">$22</span>
									</span>
								</div>
								<div class="product-list-content-bottom">
									<div class="product-list-content-bottom-top">
										<div class="product-content-size">
											<h4>sizes</h4>
											<ul>
												<li>xs</li>
												<li>s</li>
												<li>m</li>
												<li>l</li>
												<li>xl</li>
												<li>xxl</li>
											</ul>
										</div>
										<div class="product-content-color">
											<h4>Colors</h4>
											<ul>
												<li class="black"></li>
												<li class="white"></li>
												<li class="blue"></li>
												<li class="yellow"></li>
											</ul>
										</div>
									</div>
									<div class="product-cart-btn text-center">
										<a href="#"><span class="product-cart-btn-label"><i class="fa fa-cart-plus"></i> Add To Cart</span></a>
									</div>
								</div>
							</div>
						</div><!-- product -->
					</div>

				</div><!-- shop-wrapper -->
	</section>
	</div>
	

	<!-- gallery code -->
	<div class="row summercamp_content" id="4">
	<div class="e-main-content-area m-l-r-h ">
	
	<button type="button" class="btn btn-primary round_btn" data-toggle="modal" data-target="#album_maker_model"><i class="fa fa-plus"></i></button>
	
			<div class="col-lg-12 pdr0">
			<form id="albumForm" action="" method="post" enctype="multipart/form-data">
			
			<div id="album_list"></div>
			<div class="col-sm-6 pl0 pt20">
			<h4>FILE</h4>
			<input type="file"name="files[]" multiple="multiple" accept="image/*" />
			</div>
			<div class="col-sm-6 pl0 pt20">
			<h4>URL</h4>
			<input type="url" name="url" >
			</div>
			<div class="col-sm-6 pl0 pt20">
			<input type="submit" value="Add" id="upload_button">
				<div id="message"></div>
			</div>
			</form>
			</div>
	

	<br>
<div id="view_album_list" class="col-lg-12 pdr0"></div>

	 <div id="Media_files" class="col-lg-12 pdr0"></div>
			
            </div>
        </div>
	<!-- gallery code -->
	<div class="row" id="5" style="padding:20px;">
		<div class="col-sm-12 article">
		<h3 style="color: #326588">Admission Questions</h3>
		<br>
		<span  style="color: #326588" onclick="getAllAdmissionQuestion()" data-toggle="modal" data-target="#questionModal">Add Question To Form</span> 
		<br>							<span style="color: #326588"  data-toggle="modal" data-target="#questionFormModal">Request Custom Question</span>

		<div id="questions_row"></div>
		</div>
		</div>
	<div class="row" id="6" style="padding:20px;">

						<div class="col-sm-12 article">
							<form action="/action_page.php">
    							<div class="row">
    								
  									<div class="col-sm-12">
										<h3 style="color: #326588">Book An Appoinment</h3>
  											<!-- <hr style="border-top: 2px solid #3b74b8;margin-top: 5px;"> -->
  									</div>
  									<div class="col-sm-6">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa fa-yellow" aria-hidden="true"></i></span>
									<input type="text" class="" placeholder="First Name">
								</div>
    									
    								</div>
    								<div class="col-sm-6">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa fa-yellow" aria-hidden="true"></i></span>
									<input type="text" class="" placeholder="Last Name">
								</div>
    									
    								</div>
    								<div class="col-sm-6">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-phone fa fa-yellow" aria-hidden="true"></i></span>
									<input type="text" class="" placeholder="Phone">
								</div>
    									
    								</div>
    								<div class="col-sm-6">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa  fa-envelope-o fa fa-yellow" aria-hidden="true"></i></span>
									<input type="text" class="" placeholder="Email">
								</div>
    									
    								</div>
    								<div class="col-sm-6">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-map-marker fa fa-yellow" aria-hidden="true"></i></span>
									<input type="time" class="" placeholder="time">
								</div>
    									
    								</div>
    								<div class="col-sm-6">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-map-marker fa fa-yellow" aria-hidden="true"></i></span>
									<input type="Date" class="" placeholder="date">
								</div>
    									
    								</div>
									<div class="col-sm-6">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-map-marker fa fa-yellow" aria-hidden="true"></i></span>
									<input type="text" placeholder="State">
								</div>
    									
    								</div>
    								<div class="col-sm-6">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-map-marker fa fa-yellow" aria-hidden="true"></i></span>
									<input type="text" placeholder="City">
								</div>
    									
    								</div>
    								<div class="col-sm-6">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-map-marker fa fa-yellow" aria-hidden="true"></i></span>
									<input type="text" class="" placeholder="Zip">
								</div>
    									
    								</div>
    								
    								<div class="col-sm-12">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-comments fa fa-yellow" aria-hidden="true"></i></span>
									<textarea row="2" id="subject" name="subject" placeholder="Write something.." style="margin: 0;color: #000"></textarea>
								</div>
    									
    								</div>
    								
    								<div class="col-sm-12" style="margin-top: 20px;">
    									<button style="" class="btn btn1">Submit</button>
    								</div>
    								
    							</div>
	
  							</form>
						</div>
	</div>
	<div class="row summercamp_content" id="7">
		<!-- <div class="col-sm-12">
				<div class="feature-item-wrapper m-right m-left wow zoomIn" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s; animation-name: zoomIn;">
					<div class="col-md-3 col-sm-4">
						<div class="service-item boxed blue" style="padding: 14px 15px !important;margin-top: 10px;">
							<div class="boximg">
								<img src="images/school_logo.png" class="img-responsive" alt="schoollogo">
								<div class="box_hover">
									<img src="images/slide-2.jpg" class="box-1 img-responsive" alt="img">
								</div>
							</div>
								<h3 class="title texthead">Delhi Public School</h3>
						</div>
					</div>
					<div class="col-md-3 col-sm-4">
						<div class="service-item boxed yellow" style="padding: 14px 15px !important;margin-top: 10px;">
							<div class="boximg">
							<img src="images/dav.png" class="img-responsive">
							<div class="box_hover">
									<img src="images/slide-2.jpg" class="box-1 img-responsive" alt="img">

								</div>
							</div>

							<h3 class="title texthead">D.A.V Public School</h3>
							
						</div>
					</div>
					<div class="col-md-3 col-sm-4">
						<div class="service-item boxed skyblue" style="padding: 14px 15px !important;margin-top: 10px;">
							<div class="boximg">
							<img src="images/1.png" class="img-responsive">
							<div class="box_hover">
									<img src="images/slide-2.jpg" class="box-1 img-responsive" alt="img">

								</div>
						</div>
							<h3 class="title texthead">Greenfield Public School</h3>
						</div>
					</div>
					<div class="col-md-3 col-sm-4">
						<div class="service-item boxed green" style="padding: 14px 15px !important;margin-top: 10px;">
							<div class="boximg">
							<img src="images/2.png" class="img-responsive">
							<div class="box_hover">
									<img src="images/slide-2.jpg" class="box-1 img-responsive" alt="img">

								</div>
							</div>
							<h3 class="title texthead">Loreto Convent School</h3>
							
						</div>
					</div>
				</div>
			</div> -->
		<section class="shop">
			<div class="shop-wrapper m-left m-right infinityselctor-manaul">
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-list style-2 shadow">
							<div class="product-preview">
								<a href="#"><img src="images/product/product-18.jpg" alt=""></a>
							</div>
							<span class="product-flash">sale</span>
							<div class="product-list-content">
								<div class="product-list-content-top">
									<div class="product-name">
										<h4 class="product-title"><a href="#">Sample T-shirt</a></h4>
										<a href="" class="brand">Brand Name</a>
									</div>
									<span class="price">
										<span class="main-price">$22</span>
									</span>
								</div>
								<div class="product-list-content-bottom">
									<div class="product-list-content-bottom-top">
										<div class="product-content-size">
											<h4>sizes</h4>
											<ul>
												<li>xs</li>
												<li>s</li>
												<li>m</li>
												<li>l</li>
												<li>xl</li>
												<li>xxl</li>
											</ul>
										</div>
										<div class="product-content-color">
											<h4>Colors</h4>
											<ul>
												<li class="black"></li>
												<li class="white"></li>
												<li class="blue"></li>
												<li class="yellow"></li>
											</ul>
										</div>
									</div>
									<div class="product-cart-btn text-center">
										<a href="#"><span class="product-cart-btn-label"><i class="fa fa-cart-plus"></i> Add To Cart</span></a>
									</div>
								</div>
							</div>
						</div><!-- product -->
					</div>
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-list style-2 shadow">
							<div class="product-preview">
								<a href="#"><img src="images/product/product-19.jpg" alt=""></a>
							</div>
							<span class="product-flash">sale</span>
							<div class="product-list-content">
								<div class="product-list-content-top">
									<div class="product-name">
										<h4 class="product-title"><a href="#">T-shirt Shaped</a></h4>
										<a href="" class="brand">Brand Name</a>
									</div>
									<span class="price">
										<span class="compare-price">$29</span>
										<span class="main-price">$22</span>
									</span>
								</div>
								<div class="product-list-content-bottom">
									<div class="product-list-content-bottom-top">
										<div class="product-content-size">
											<h4>sizes</h4>
											<ul>
												<li>xs</li>
												<li>s</li>
												<li>m</li>
												<li>l</li>
												<li>xl</li>
												<li>xxl</li>
											</ul>
										</div>
										<div class="product-content-color">
											<h4>Colors</h4>
											<ul>
												<li class="black"></li>
												<li class="white"></li>
												<li class="blue"></li>
												<li class="yellow"></li>
											</ul>
										</div>
									</div>
									<div class="product-cart-btn text-center">
										<a href="#"><span class="product-cart-btn-label"><i class="fa fa-cart-plus"></i> Add To Cart</span></a>
									</div>
								</div>
							</div>
						</div><!-- product -->
					</div>
					<!-- <div class="col-md-3 col-sm-4 infinity-item-manaul">
						<div class="product-list shadow">
							<div class="product-preview">
								<a href="#"><img src="images/product/product-3.jpg" alt=""></a>
							</div>
							<div class="product-list-content">
								<h4 class="product-title"><a href="#">Blue T-shirt <span class="brand">(Brand Name)</span></a></h4>
								<div class="rating-star-area"></div>
								<span class="price">
									<span class="main-price">$25</span>
								</span>
								<div class="product-cart-btn">
									<a href="#"><span class="product-cart-btn-label"><i class="fa fa-shopping-cart"></i> Add To Cart</span></a>
								</div>
							</div>
						</div>
					</div> -->
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-list style-2 shadow">
							<div class="product-preview">
								<a href="#"><img src="images/product/product-18.jpg" alt=""></a>
							</div>
							<span class="product-flash">sale</span>
							<div class="product-list-content">
								<div class="product-list-content-top">
									<div class="product-name">
										<h4 class="product-title"><a href="#">Sample T-shirt</a></h4>
										<a href="" class="brand">Brand Name</a>
									</div>
									<span class="price">
										<span class="main-price">$22</span>
									</span>
								</div>
								<div class="product-list-content-bottom">
									<div class="product-list-content-bottom-top">
										<div class="product-content-size">
											<h4>sizes</h4>
											<ul>
												<li>xs</li>
												<li>s</li>
												<li>m</li>
												<li>l</li>
												<li>xl</li>
												<li>xxl</li>
											</ul>
										</div>
										<div class="product-content-color">
											<h4>Colors</h4>
											<ul>
												<li class="black"></li>
												<li class="white"></li>
												<li class="blue"></li>
												<li class="yellow"></li>
											</ul>
										</div>
									</div>
									<div class="product-cart-btn text-center">
										<a href="#"><span class="product-cart-btn-label"><i class="fa fa-cart-plus"></i> Add To Cart</span></a>
									</div>
								</div>
							</div>
						</div><!-- product -->
					</div>
					<div class="col-md-3 col-sm-4 col-xs-6">
						<div class="product-list style-2 shadow">
							<div class="product-preview">
								<a href="#"><img src="images/product/product-18.jpg" alt=""></a>
							</div>
							<span class="product-flash">sale</span>
							<div class="product-list-content">
								<div class="product-list-content-top">
									<div class="product-name">
										<h4 class="product-title"><a href="#">Sample T-shirt</a></h4>
										<a href="" class="brand">Brand Name</a>
									</div>
									<span class="price">
										<span class="main-price">$22</span>
									</span>
								</div>
								<div class="product-list-content-bottom">
									<div class="product-list-content-bottom-top">
										<div class="product-content-size">
											<h4>sizes</h4>
											<ul>
												<li>xs</li>
												<li>s</li>
												<li>m</li>
												<li>l</li>
												<li>xl</li>
												<li>xxl</li>
											</ul>
										</div>
										<div class="product-content-color">
											<h4>Colors</h4>
											<ul>
												<li class="black"></li>
												<li class="white"></li>
												<li class="blue"></li>
												<li class="yellow"></li>
											</ul>
										</div>
									</div>
									<div class="product-cart-btn text-center">
										<a href="#"><span class="product-cart-btn-label"><i class="fa fa-cart-plus"></i> Add To Cart</span></a>
									</div>
								</div>
							</div>
						</div><!-- product -->
					</div>

				</div><!-- shop-wrapper -->
	</section>
	</div>
</div>
			<div class="col-md-2 sidebar pr0 pdl25" style="margin-left:;">
					<div class="m-left m-right sidebar-widget-wrapper" style="margin-top: 80px;">
						
						
						<div class="widget calendar-widget">
							<table id="wp-calendar">
								<caption>JANUARY 2017</caption>
								<thead>
									<tr>
										<th scope="col" title="Monday">S</th>
										<th scope="col" title="Tuesday">S</th>
										<th scope="col" title="Wednesday">M</th>
										<th scope="col" title="Thursday">T</th>
										<th scope="col" title="Friday">W</th>
										<th scope="col" title="Saturday">T</th>
										<th scope="col" title="Sunday">F</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<td>1</td>
										<td>2</td>
										<td>3</td>
										<td>4</td>
										<td>5</td>
										<td>6</td>
									</tr>
									<tr>
										<td id="today">7</td>
										<td>8</td>
										<td>9</td>
										<td><a href="#" aria-label="Posts published on March 4, 2017">10</a></td>
										<td>11</td>
										<td>12</td>
										<td>13</td>
									</tr>
									<tr>
										<td>14</td>
										<td><a href="#" class="highlight test_tool" aria-label="Posts published on March 4, 2017" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Guidance Document for the Provision of a Wireless Network Installation in Primary Schools " aria-describedby="tooltip67542">15</a></td>
										<td>16</td>
										<td>17</td>
										<td>18</td>
										<td>19</td>
										<td>20</td>
									</tr>
									<tr>
										<td>21</td>
										<td>22</td>
										<td>23</td>
										<td>24</td>
										<td>25</td>
										<td>26</td>
										<td>27</td>
									</tr>
									<tr>
										<td>28</td>
										<td>29</td>
										<td>30</td>
										<td>31</td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</tbody>
							</table>
						</div>
						
						<div class="widget recent-comment-widget">
							<h3 class="widget-title">Recent Blog</h3>
							<ul class="f-widget-recent-entries">
									<li><a href="#">What’s The Theory of Design ?</a></li>
									<li><a href="#">Latest News For Design 2017</a></li>
									<li><a href="#">Design Trend 2017</a></li>
									<li><a href="#">Latest News For Design</a></li>
									<li><a href="#">Principle &amp; The Theory of Design</a></li>
									<li><a href="#">Recent Design Trend &amp; Ideas</a></li>
								</ul>
						</div>
						<div class="widget recent-comment-widget">
							<h3 class="widget-title">News Feeds</h3>
							<ul class="recentcomments">
								<li><a href="#">The standard Lorem Ipsum passage</a> By <span class="comment-author-link"><a href="http://example.org/" rel="external nofollow" class="url">Excite</a></span></li>
								<li><a href="#">The standard Lorem Ipsum used since</a> By <span class="comment-author-link"><a href="http://example.org/" rel="external nofollow" class="url">The 1500s</a></span></li>
								<li><span class="comment-author-link"><a href="http://example.org/" rel="external nofollow" class="url">Standard</a></span> By <a href="#">Lorem written by Cicero in 45 Bc</a></li>
							</ul>
						</div>
					</div><!-- sidebar-widget-wrapper -->
			</div>
		</div>
	</div>

	


</div>
<?php include("footer.php"); ?>



 <!-- modal for login -->
 
			
		
		
     <div class="modal fade" id="questionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <form action="" method="post" id="signinform">
        <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Question List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
							<div class="modal-body">
							<div id="allAdmissionQuestionList"></div>

							</div>

                </div>
        </form>
        </div>
        </div>

         <div class="modal fade" id="questionFormModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <form action="" method="post" id="questionForm">
        <div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Question Form</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
					</div>
                <div class="modal-body">
                <textarea class="form-control" id="question" name="question" placeholder=" Your Question" required></textarea>      
            </div>

			<div class="modal-footer">
			<div id="QUESTION_ADD_MSG"></div>
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary">ADD</button>
			</div>

             
            
        </div>
        </form>
        </div>
        </div>
		
		
		
		
		<div class="modal fade" id="album_maker_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <form action="" method="post" id="album_maker_form">
        <div class="modal-content">
					<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Album Maker</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
					</div>
                <div class="modal-body">
                

<input type="text" class="form-control" name="album_name"  placeholder=" Your Album Name" required>				
            </div>

			<div class="modal-footer">
			<div id="ALBUM_ADD_MSG"></div>
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary" id="ALBUM_ADD_BUTTON">ADD</button>
			</div>

             
            
        </div>
        </form>
        </div>
        </div>

		<!-- modal for login -->
<a href="#" class="scroll-top"><i class="fa fa-arrow-up"></i></a>
	<script src="js/jquery-2.2.3.min.js">
	</script>
	<script src="js/bootstrap.min.js">
	</script> 
	<script src="js/waypoints.min.js">
	</script>
	<script src="js/jquery.easing.1.3.js">
	</script> 
	<script src="js/validator.min.js">
	</script>
	<script src="js/owl.carousel.js">
	</script> 
	<script src="js/owl.carousel2.thumbs.js">
	</script>
	<script src="js/jquery.nav.js">
	</script> 
	<script src="js/jquery.stellar.min.js">
	</script>
	<script src="js/wow.min.js">
	</script> 
	<script src="js/lightcase.js">
	</script>
	<script src="js/scrolloverflow.min.js">
	</script> 
	<script src="js/smooth-scroll.min.js">
	</script>
	<script src="js/jquery.fullpage.min.js">
	</script> 
	<script src="js/jquery.events.touch.js">
	</script>
	<script src="js/jquery.infinitescroll.min.js">
	</script> 
	<script src="js/jquery.lazyload.min.js">
	</script>
	<script src="js/swiper.min.js">
	</script> 
	<script src="js/parallax.min.js">
	</script>
	<script src="js/masonry.pkgd.min.js">
	</script> 
	<script src="js/shuffle.min.js">
	</script>
	<script src="js/animsition.min.js">
	</script> 
	<script src="js/swiper.min.js">
	</script>
	<script src="js/jquery.nstSlider.js">
	</script> 
	<script src="js/jquery.countdown.min.js">
	</script>
	<script src="js/jquery.counterup.min.js">
	</script> 
	<script src="js/bootsnav.js">
	</script>
	<script src="js/tilt.jquery.min.js">
	</script> 
	<script src="js/custom.js">
	</script>


	<script type="text/javascript">
	var infinitymanualmesonary=$(".infinityselctor-manaul-mesonary");infinitymanualmesonary.infinitescroll({navSelector:".infinity-manaul-links",nextSelector:".infinity-manaul-links a:first",itemSelector:".infinity-item-manaul",loading:{msgText:"Loading more posts...",finishedMsg:"Sorry, no more posts.",},errorCallback:function(){$(".post-load").css("display","none")}},function(a){var b=$(a).css("opacity",0);b.imagesLoaded(function(){b.animate({opacity:1});infinitymanualmesonary.masonry("appended",b,true)})});$(window).unbind(".infscr");$(".post-load").click(function(){infinitymanualmesonary.infinitescroll("retrieve");return false});
	</script> 
	<script type="text/javascript">
	$(".js-tilt").tilt({maxTilt:20,perspective:1000,easing:"cubic-bezier(.03,.98,.52,.99)",scale:1,speed:300,transition:true,axis:null,reset:true,glare:false,maxGlare:1,});
	</script>
	<script>
	$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>


	<script type="text/javascript">
		$(document).ready(function(){

			$("#1").show();
            $("#2").hide();
            $("#3").hide();
            $("#4").hide();
            $("#5").hide();
            $("#6").hide();
            $("#7").hide();
       
        $("#timeline").click(function(){
            $("#1").show();
            $("#2").hide();
            $("#3").hide();
            $("#4").hide();
            $("#5").hide();
            $("#6").hide();
            $("#7").hide();
        });

        $("#details").click(function(){
        	$("#1").hide();
            $("#2").show();
            $("#3").hide();
            $("#4").hide();
            $("#5").hide();
            $("#6").hide();
            $("#7").hide();
        });
        $("#products").click(function(){
        	$("#1").hide();
            $("#2").hide();
            $("#3").show();
            $("#4").hide();
            $("#5").hide();
            $("#6").hide();
            $("#7").hide();
        });
        $("#admission").click(function(){
        	$("#1").hide();
            $("#2").hide();
            $("#3").hide();
            $("#4").hide();
            $("#5").show();
            $("#6").hide();
            $("#7").hide();
        });
        $("#appoinment").click(function(){
        	$("#1").hide();
            $("#2").hide();
            $("#3").hide();
            $("#4").hide();
            $("#5").hide();
            $("#6").show();
            $("#7").hide();
        });
         $("#photos").click(function(){
        	$("#1").hide();
            $("#2").hide();
            $("#3").hide();
            $("#4").show();
            $("#5").hide();
            $("#6").hide();
            $("#7").hide();

        });
            $("#summercamp").click(function(){
        	$("#1").hide();
            $("#2").hide();
            $("#3").hide();
            $("#4").hide();
            $("#5").hide();
            $("#6").hide();
            $("#7").show();

        });

    });
	</script>
	<script>
$(document).ready(function() {
$(".tablinks").click(function () {
    $(".tablinks").removeClass("active");
    // $(".tab").addClass("active"); // instead of this do the below 
    $(this).addClass("active");   
});
});
</script>
</body>
</html>
