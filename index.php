<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="UTF-8">
	<title>LuminoSoul</title>
	<meta content="" name="keywords">
	<meta content="" name="description">
	<meta content="" name="author">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	
	<link href="css/lightcase.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="fonts/font.css">
	<link rel="stylesheet" type="text/css" href="palyfair_Dispaly/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="amaranth/stylesheet.css">
	<link rel="stylesheet" type="text/css" href="gentium_Basic/stylesheet.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="css/icofont.css" rel="stylesheet">
	<link href="css/animsition.min.css" rel="stylesheet">
	<link href="css/jquery.nstSlider.min.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/swiper.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/animate.min.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootsnav.css" rel="stylesheet">
	<link href="css/shortcode.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    
	
</head>
<body class="demo-landing-page p-load-animation">

	
	
<?php include("header.php"); ?>
<script src="respjs/contactUs.js"></script>
<section class="demo-banner overlay-black" data-overlay="8">
		<div class="demo-banner-content">
			<div class="demo-banner-content-inner js-tilt">
				<h1 style="color: #ffffff">LUMINOSOUL</h1>
				
					<div class="container">
						<div class="row" style="margin-top: 20px;">

  							<div class="col-sm-12" style="margin-top: 20px;">
  								<button style="" class="btn">Locate Me<i class="fa fa-crosshairs"></i></button>

    							<input class="email" type="text" name="FirstName" placeholder="Enter Your Location">
    						 	<button style="" class="btn"><i style="margin-right: 10px;" class="fa fa-search"></i>Search</button>
    						 
							</div>
							
  						</div>
  					
					</div>
			</div>
		</div>

		<div class="angular-area">
			<svg height="100" preserveaspectratio="none" viewbox="0 0 100 102" width="100%">
			<path d="M0 0 L50 90 L100 0 V100 H0" fill="#FFF"></path></svg>
		</div>
	</section>
	
	<section class="recent-project s-padding" id="projects">
		<div class="container">
			<div class="row">
				<div class="section-title">
					<h2>Recent Project</h2>
					<p> Ut enim ad minim veniam quis nostrud exercitation ullamco Duis aute irure dolor in<br> reprehenderit in velit esse cillum dolore eu nulla pariatur</p>
				</div>
				<div class="mesonary-wrapper mesonary-3-col gutter-30 zoom" style="position: relative; height: 855.078px;">
					<div class="grid-sizer"></div>
					<div class="mesonary-item width-2 height-1" style="position: absolute; left: 0%; top: 0px;">
						<div class="portfolio-item style-6 yellow ">
							<a class="portfolio-anchor" href="#">
							<div class="portfolio-img">
								<img src="images/banner/img2.jpg" alt="">
							</div>
							</a><div class="portfolio-item-meta"><a class="portfolio-anchor" href="#">
								</a><div class="portfolio-item-meta-inner"><a class="portfolio-anchor" href="#">
									</a><div class="Portfolio-txt center"><a class="portfolio-anchor" href="#">
										</a><a href="#">
											<div class="p-text-wrapper text-center">
												<h3>DONE PROJECT 02</h3>
												<span>2ND JANUARY, 2017</span>
											</div></a>
										<a href="images/banner/img2.jpg" data-rel="project:portfolio">
											<div class="sin-icon">
												<i class="fa fa-search"></i>
											</div>
										</a>
									</div>
								</div>
							</div><!-- portfolio-item-meta -->
						<!-- portfolio-anchor -->
						</div>
					</div><!-- portfolio-item  -->
					<div class="mesonary-item width-1 height-1" style="position: absolute; left: 66.5833%; top: 0px;">
						<div class="portfolio-item style-6 blue ">
							<div class="portfolio-img">
								<img src="images/project2.jpg" alt="">
							</div>
							<div class="portfolio-item-meta">
								<div class="portfolio-item-meta-inner">
									<div class="Portfolio-txt center">
										<a href="#">
											<div class="p-text-wrapper text-center">
												<h3>DONE PROJECT 01</h3>
												<span>2ND JANUARY, 2017</span>
											</div>
										</a>
										<a href="images/project2.jpg" data-rel="project:portfolio">
											<div class="sin-icon">
												<i class="fa fa-search"></i>
											</div>
										</a>
									</div>
								</div>
							</div><!-- portfolio-item-meta -->
						</div>
					</div><!-- portfolio-item  -->
					<div class="mesonary-item width-1 height-1" style="position: absolute; left: 66.5833%; top: 427px;">
						<div class="portfolio-item style-6 green ">
							<a class="portfolio-anchor" href="#">
								<div class="portfolio-img">
									<img src="images/project4.jpg" alt="">
								</div>
								</a><div class="portfolio-item-meta"><a class="portfolio-anchor" href="#">
								</a><div class="portfolio-item-meta-inner"><a class="portfolio-anchor" href="#">
									</a><div class="Portfolio-txt center"><a class="portfolio-anchor" href="#">
										</a><a href="#">
											<div class="p-text-wrapper text-center">
												<h3>DONE PROJECT 03</h3>
												<span>2ND JANUARY, 2017</span>
											</div>
										</a>
										<a href="images/project4.jpg" data-rel="project:portfolio">
											<div class="sin-icon">
												<i class="fa fa-search"></i>
											</div>
										</a>
									</div>
								</div>
							</div><!-- portfolio-item-meta -->
							<!-- portfolio-anchor -->
						</div>
					</div><!-- portfolio-item  -->
					<div class="mesonary-item width-1 height-1" style="position: absolute; left: 0%; top: 427px;">
						<div class="portfolio-item style-6 skyblue ">
							<a class="portfolio-anchor" href="#">
								<div class="portfolio-img">
									<img src="images/project5.jpg" alt="">
								</div>
								</a><div class="portfolio-item-meta"><a class="portfolio-anchor" href="#">
								</a><div class="portfolio-item-meta-inner"><a class="portfolio-anchor" href="#">
									</a><div class="Portfolio-txt center"><a class="portfolio-anchor" href="#">
										</a><a href="#">
											<div class="p-text-wrapper text-center">
												<h3>DONE PROJECT 04</h3>
												<span>2ND JANUARY, 2017</span>
											</div>
										</a>
										<a href="images/project5.jpg" data-rel="project:portfolio">
											<div class="sin-icon">
												<i class="fa fa-search"></i>
											</div>
										</a>
									</div>
								</div>
							</div><!-- portfolio-item-meta -->
							<!-- portfolio-anchor -->
						</div>
					</div><!-- portfolio-item  -->
					<div class="mesonary-item width-1 height-1" style="position: absolute; left: 33.25%; top: 427px;">
						<div class="portfolio-item style-6 blue ">
							<a class="portfolio-anchor" href="#">
								<div class="portfolio-img">
									<img src="images/project3.jpg" alt="">
								</div>
								</a><div class="portfolio-item-meta"><a class="portfolio-anchor" href="#">
								</a><div class="portfolio-item-meta-inner"><a class="portfolio-anchor" href="#">
									</a><div class="Portfolio-txt center"><a class="portfolio-anchor" href="#">
										</a><a href="#">
											<div class="p-text-wrapper text-center">
												<h3>DONE PROJECT 05</h3>
												<span>2ND JANUARY, 2017</span>
											</div>
										</a>
										<a href="images/project3.jpg" data-rel="project:portfolio">
											<div class="sin-icon">
												<i class="fa fa-search"></i>
											</div>
										</a>
									</div>
								</div>
							</div><!-- portfolio-item-meta -->
							<!-- portfolio-anchor -->
						</div>
					</div><!-- portfolio-item  -->
				</div>
				<div class="text-center one-page-prjoect-btn">
					<a href="#" class="btn">Show All <i class="fa fa-arrow-right"></i></a>
				</div>
			</div>
		</div>
	</section>
	<!-- services -->
	<section class="service bottom-padding-80" id="service">
		<div class="container">
			<div class="row">
				<div class="section-title">
					<h2>Service We Offer</h2>
					<p> Ut enim ad minim veniam quis nostrud exercitation ullamco Duis aute irure dolor in<br> reprehenderit in velit esse cillum dolore eu nulla pariatur</p>
				</div>
				<div class="feature-item-wrapper m-right m-left">
                    <div class="col-md-4 col-sm-4 wow slideInUp" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-name: slideInUp;">
                        <div class="service-item boxed" data-aos="fade-up">
                            <div class="icon-only c-main">
                                <i class="fa fa-folder-o"></i>
                            </div>
                            <h4 class="title c-blue">UI/UX DESIGN</h4>
                            <p>There are many variations of it’s passages of Lorem fered  in some jected umour</p>
                        </div><!-- service-item -->
                    </div>
                    <div class="col-md-4 col-sm-4 wow slideInUp" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: slideInUp;">
                        <div class="service-item boxed" data-aos="fade-up">
                            <div class="icon-only c-main">
                                <i class="fa fa-cog"></i>
                            </div>
                            <h4 class="title c-blue">BRANDING</h4>
                            <p>There are many variations of it’s passages of Lorem fered  in some jected umour</p>
                        </div><!-- service-item -->
                    </div>
                    <div class="col-md-4 col-sm-4 wow slideInUp" data-wow-duration="1s" data-wow-delay=".1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.1s; animation-name: slideInUp;">
                        <div class="service-item boxed" data-aos="fade-up">
                            <div class="icon-only c-main">
                                <i class="fa fa-picture-o"></i>
                            </div>
                            <h4 class="title c-blue">PHOTOGRAPHY</h4>
                            <p>There are many variations of it’s passages of Lorem fered  in some jected umour</p>
                        </div><!-- service-item -->
                    </div>
                    <div class="col-md-4 col-sm-4 wow slideInUp" data-wow-duration="1s" data-wow-delay=".2s" style="visibility: hidden; animation-duration: 1s; animation-delay: 0.2s; animation-name: none;">
                        <div class="service-item boxed" data-aos="fade-up">
                            <div class="icon-only c-main">
                                <i class="fa fa-cogs"></i>
                            </div>
                            <h4 class="title c-blue">DEVELOPMENT</h4>
                            <p>There are many variations of it’s passages of Lorem fered  in some jected umour</p>
                        </div><!-- service-item -->
                    </div>
                    <div class="col-md-4 col-sm-4 wow slideInUp" data-wow-duration="1s" data-wow-delay=".2s" style="visibility: hidden; animation-duration: 1s; animation-delay: 0.2s; animation-name: none;">
                        <div class="service-item boxed" data-aos="fade-up">
                            <div class="icon-only c-main">
                                <i class="fa fa-cog"></i>
                            </div>
                            <h4 class="title c-blue">MARKETING</h4>
                            <p>There are many variations of it’s passages of Lorem fered  in some jected umour</p>
                        </div><!-- service-item -->
                    </div>
                    <div class="col-md-4 col-sm-4 wow slideInUp" data-wow-duration="1s" data-wow-delay=".2s" style="visibility: hidden; animation-duration: 1s; animation-delay: 0.2s; animation-name: none;">
                        <div class="service-item boxed" data-aos="fade-up">
                            <div class="icon-only c-main">
                                <i class="fa fa-cogs"></i>
                            </div>
                            <h4 class="title c-blue">SEO OPTIMIZATION</h4>
                            <p>There are many variations of it’s passages of Lorem fered  in some jected umour</p>
                        </div><!-- service-item -->
					</div>

						<a href="services.php" class="sp-btn">Read More</a>
					
                </div><!-- feature-item-wrapper -->
			</div>
		</div>
	</section>
	<!-- services -->

	<!-- we are awesome -->
	<section class="sp-about s-padding bg-12 overlay" data-overlay="7.5" id="about">
		<div class="container">
			<div class="row">
				<div class="sp-about-content-wrapper">
					<div class="column-align-middle m-left m-right">
						<div class="col-md-5 col-sm-5">
							<div class="sp-about-img">
								<img src="images/team-22.jpg" alt="" class="img-responsive">
							</div>
						</div>
						<div class="col-md-6 col-md-offset-1 col-sm-7">
							<div class="sp-about-content">
								<div class="text-container-title">
									<h2 class="light">We Are Awesome</h2>
								</div>
								<p>consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea consequat.Duis iscing elit sed do eiusmod.</p>
								<p>Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								<div class="sp-about-btn">
									<a href="#" class="sp-btn">view our team</a>
									<a href="#" class="sp-btn">view feedback</a>
									<a href="aboutus.php" class="sp-btn">Read More</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="angular-area">
        	<svg width="100%" height="100" viewBox="0 0 100 102" preserveAspectRatio="none">
        	    <path d="M0 0 L50 90 L100 0 V100 H0" fill="#FFF"></path>
        	</svg>
        </div><!-- angular-area -->
	</section>
	<!-- three special things -->
	<section class="special-thing s-padding">
		<div class="container">
			<div class="row">
				<div class="section-title">
					<h2>Three Special Things</h2>
					<p> Ut enim ad minim veniam quis nostrud exercitation ullamco Duis aute irure dolor in<br> reprehenderit in velit esse cillum dolore eu nulla pariatur</p>
				</div>
				<div class="special-items m-left m-right">
					<div class="col-md-4 col-sm-4">
						<div class="special-item">
							<div class="special-item-icon">
								<div class="hex">
									<div class="hex-inner">
										<i class="fa fa-rocket"></i>
									</div>
								</div>
							</div>
							<div class="special-item-txt">
                                <h4>zero to one</h4>
                                <p>Sed do eiusmod tempor incididunt labore et dolore magna aliqua Ut enim ad minim veniam.</p>
                            </div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="special-item">
							<div class="special-item-icon">
								<div class="hex">
									<div class="hex-inner">
										<i class="fa fa-bolt"></i>
									</div>
								</div>
							</div>
							<div class="special-item-txt">
                                <h4>our level up</h4>
                                <p>Sed do eiusmod tempor incididunt labore et dolore magna aliqua Ut enim ad minim veniam.</p>
                            </div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4">
						<div class="special-item">
							<div class="special-item-icon">
								<div class="hex">
									<div class="hex-inner">
										<i class="fa fa-smile-o"></i>
									</div>
								</div>
							</div>
							<div class="special-item-txt">
                                <h4>Your satisfaction</h4>
                                <p>Sed do eiusmod tempor incididunt labore et dolore magna aliqua Ut enim ad minim veniam.</p>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- testimonial -->

	<section class="team s-padding bg-sky-blue" id="team">

		 <div class="section-title">
					<h2>Awesome Team</h2>
					<p> Ut enim ad minim veniam quis nostrud exercitation ullamco Duis aute irure dolor in<br> reprehenderit in velit esse cillum dolore eu nulla pariatur</p>
				</div>
		<div id="textCarousel" class="carousel slide" data-ride="carousel" data-interval="5000" data-pause="">
    <!-- Indicators -->
    
    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      <div class="item active">

      	<div class="col-sm-4">
        <div class="team-item sp-style">
                        		<img src="images/team-3.jpg" alt="">
			                        <div class="team-caption">
			                            <div class="caption-triangle"></div>
			                            <div class="caption-inner">
			                                <div class="caption-content">
			                                    <h3>Kevin Monoro</h3>
			                                    <span class="deg">Designer</span>
			                                </div>
			                            </div>
			                        </div><!-- team-caption -->
                    		</div>
    </div>
    <div class="col-sm-4">
        <div class="team-item sp-style">
                        		<img src="images/team-2.jpg" alt="">
			                        <div class="team-caption">
			                            <div class="caption-triangle"></div>
			                            <div class="caption-inner">
			                                <div class="caption-content">
			                                    <h3>Kevin Monoro</h3>
			                                    <span class="deg">Designer</span>
			                                </div>
			                            </div>
			                        </div><!-- team-caption -->
                    		</div>
    </div>
    <div class="col-sm-4">
       <div class="team-item sp-style">
                        		<img src="images/team-1.jpg" alt="">
			                        <div class="team-caption">
			                            <div class="caption-triangle"></div>
			                            <div class="caption-inner">
			                                <div class="caption-content">
			                                    <h3>Kevin Monoro</h3>
			                                    <span class="deg">Designer</span>
			                                </div>
			                            </div>
			                        </div><!-- team-caption -->
                    		</div>
    </div>
    </div>

      <div class="item">

      	<div class="col-sm-4">
        <div class="team-item sp-style">
                        		<img src="images/team-3.jpg" alt="">
			                        <div class="team-caption">
			                            <div class="caption-triangle"></div>
			                            <div class="caption-inner">
			                                <div class="caption-content">
			                                    <h3>Kevin Monoro</h3>
			                                    <span class="deg">Designer</span>
			                                </div>
			                            </div>
			                        </div><!-- team-caption -->
                    		</div>
    </div>
    <div class="col-sm-4">
        <div class="team-item sp-style">
                        		<img src="images/team-2.jpg" alt="">
			                        <div class="team-caption">
			                            <div class="caption-triangle"></div>
			                            <div class="caption-inner">
			                                <div class="caption-content">
			                                    <h3>Kevin Monoro</h3>
			                                    <span class="deg">Designer</span>
			                                </div>
			                            </div>
			                        </div><!-- team-caption -->
                    		</div>
    </div>
    <div class="col-sm-4">
       <div class="team-item sp-style">
                        		<img src="images/team-1.jpg" alt="">
			                        <div class="team-caption">
			                            <div class="caption-triangle"></div>
			                            <div class="caption-inner">
			                                <div class="caption-content">
			                                    <h3>Kevin Monoro</h3>
			                                    <span class="deg">Designer</span>
			                                </div>
			                            </div>
			                        </div><!-- team-caption -->
                    		</div>
    </div>
    </div>
    
      <div class="item">

      	<div class="col-sm-4">
        <div class="team-item sp-style">
                        		<img src="images/team-3.jpg" alt="">
			                        <div class="team-caption">
			                            <div class="caption-triangle"></div>
			                            <div class="caption-inner">
			                                <div class="caption-content">
			                                    <h3>Kevin Monoro</h3>
			                                    <span class="deg">Designer</span>
			                                </div>
			                            </div>
			                        </div><!-- team-caption -->
                    		</div>
    </div>
    <div class="col-sm-4">
        <div class="team-item sp-style">
                        		<img src="images/team-2.jpg" alt="">
			                        <div class="team-caption">
			                            <div class="caption-triangle"></div>
			                            <div class="caption-inner">
			                                <div class="caption-content">
			                                    <h3>Kevin Monoro</h3>
			                                    <span class="deg">Designer</span>
			                                </div>
			                            </div>
			                        </div><!-- team-caption -->
                    		</div>
    </div>
    <div class="col-sm-4">
       <div class="team-item sp-style">
                        		<img src="images/team-1.jpg" alt="">
			                        <div class="team-caption">
			                            <div class="caption-triangle"></div>
			                            <div class="caption-inner">
			                                <div class="caption-content">
			                                    <h3>Kevin Monoro</h3>
			                                    <span class="deg">Designer</span>
			                                </div>
			                            </div>
			                        </div><!-- team-caption -->
                    		</div>
    </div>
    </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#textCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#textCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
	</section>
	
	<!-- clients says -->


	<section class="testimonial">

		 <div class="s-padding text-center">
					<h2>What Our Client Say</h2>
				</div><!-- section-title -->
		<div id="boxCarousel" class="carousel slide" data-ride="carousel" data-interval="5000" data-pause="">
    <!-- Indicators -->
    
    <!-- Wrapper for slides -->
    <div class="carousel-inner ">

    <div class="item active s-padding">

      	<div class="col-sm-4">
        <div class="testimonial-item sp-style">
                        <div class="hex">
							<div class="hex-inner">
								<span class="fa fa-quote-left"></span>
							</div>
						</div>
                        <p>sed do eiusmod tempor incididunt labore et dolore magna aliqua Ut enim ad minim veniam.</p>
                        <h4>james bond, ceo</h4>
                    </div>
    </div>
    <div class="col-sm-4">
        <div class="testimonial-item sp-style">
                        <div class="hex">
							<div class="hex-inner">
								<span class="fa fa-quote-left"></span>
							</div>
						</div>
                        <p>sed do eiusmod tempor incididunt labore et dolore magna aliqua Ut enim ad minim veniam.</p>
                        <h4>james bond, ceo</h4>
                    </div>
    </div>
    <div class="col-sm-4">
      <div class="testimonial-item sp-style">
                        <div class="hex">
							<div class="hex-inner">
								<span class="fa fa-quote-left"></span>
							</div>
						</div>
                        <p>sed do eiusmod tempor incididunt labore et dolore magna aliqua Ut enim ad minim veniam.</p>
                        <h4>james bond, ceo</h4>
                    </div>
    </div>
    </div>
	<div class="item s-padding">

      	<div class="col-sm-4">
        <div class="testimonial-item sp-style">
                        <div class="hex">
							<div class="hex-inner">
								<span class="fa fa-quote-left"></span>
							</div>
						</div>
                        <p>sed do eiusmod tempor incididunt labore et dolore magna aliqua Ut enim ad minim veniam.</p>
                        <h4>james bond, ceo</h4>
                    </div>
    </div>
    <div class="col-sm-4">
        <div class="testimonial-item sp-style">
                        <div class="hex">
							<div class="hex-inner">
								<span class="fa fa-quote-left"></span>
							</div>
						</div>
                        <p>sed do eiusmod tempor incididunt labore et dolore magna aliqua Ut enim ad minim veniam.</p>
                        <h4>james bond, ceo</h4>
                    </div>
    </div>
    <div class="col-sm-4">
      <div class="testimonial-item sp-style">
                        <div class="hex">
							<div class="hex-inner">
								<span class="fa fa-quote-left"></span>
							</div>
						</div>
                        <p>sed do eiusmod tempor incididunt labore et dolore magna aliqua Ut enim ad minim veniam.</p>
                        <h4>james bond, ceo</h4>
                    </div>
    </div>
    </div>
<div class="item s-padding">

      	<div class="col-sm-4">
        <div class="testimonial-item sp-style">
                        <div class="hex">
							<div class="hex-inner">
								<span class="fa fa-quote-left"></span>
							</div>
						</div>
                        <p>sed do eiusmod tempor incididunt labore et dolore magna aliqua Ut enim ad minim veniam.</p>
                        <h4>james bond, ceo</h4>
                    </div>
    </div>
    <div class="col-sm-4">
        <div class="testimonial-item sp-style">
                        <div class="hex">
							<div class="hex-inner">
								<span class="fa fa-quote-left"></span>
							</div>
						</div>
                        <p>sed do eiusmod tempor incididunt labore et dolore magna aliqua Ut enim ad minim veniam.</p>
                        <h4>james bond, ceo</h4>
                    </div>
    </div>
    <div class="col-sm-4">
      <div class="testimonial-item sp-style">
                        <div class="hex">
							<div class="hex-inner">
								<span class="fa fa-quote-left"></span>
							</div>
						</div>
                        <p>sed do eiusmod tempor incididunt labore et dolore magna aliqua Ut enim ad minim veniam.</p>
                        <h4>james bond, ceo</h4>
                    </div>
    </div>
    </div>

     
      
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#boxCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#boxCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
	</section>
	
<!-- recent post -->
<section class="recent-blog s-padding bg-sky-blue" id="blog">
		<div class="container">
			<div class="row">
				<div class="section-title">
					<h2>Recent Blog Post</h2>
					<p> Ut enim ad minim veniam quis nostrud exercitation ullamco Duis aute irure dolor in<br> reprehenderit in velit esse cillum dolore eu nulla pariatur</p>
				</div>
				<div class="blog-g-wrapper m-left m-right">
					<div class="col-md-4 col-sm-6">
						<article class="e-blog-post card-layout bg-white">
							<div class="e-blog-post-inner">
								<div class="e-blog-post-thumb">
									<a href="#">
										<img src="images/posts/post-8.jpg" alt="">
									</a>
								</div><!-- blog-post-thumb -->
								<div class="e-blog-post-des">
									<h3 class="e-blog-post-title"><a href="#">Highly Experienced Team</a></h3>
									<div class="e-blog-post-meta">
										<span class="e-blog-post-meta-author">POSTED BY: <a href="#">ADMIN</a></span>
										<span class="e-blog-post-meta-time">26 JANUARY - 2017</span>
										<span class="e-blog-post-meta-tags">
											<i class="icofont icofont-tag"></i>
											<a href="#">Business</a>,
											<a href="#">Design</a>,
											<a href="#">social</a>
										</span>
									</div><!-- blog-post-meta -->
									<div class="e-blog-post-content gardient-end">
										<p>Here are many variations of passages of Lorem Ipsum available, but the majority have suffered in some form by injected humour</p>
									</div><!-- blog-post-content -->
									<a class="e-blog-post-more" href="blogs.php">READ MORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</div><!-- blog-post-des -->
							</div><!-- blog-post-inner -->
						</article><!-- blog-post -->
					</div>
					<div class="col-md-4 col-sm-6">
						<article class="e-blog-post card-layout bg-white">
							<div class="e-blog-post-inner">
								<div class="e-blog-post-thumb">
									<a href="#">
										<img src="images/posts/post-5.jpg" alt="">
									</a>
								</div><!-- blog-post-thumb -->
								<div class="e-blog-post-des">
									<h3 class="e-blog-post-title"><a href="#">This Is Example Post</a></h3>
									<div class="e-blog-post-meta">
										<span class="e-blog-post-meta-author">POSTED BY: <a href="#">ADMIN</a></span>
										<span class="e-blog-post-meta-time">26 JANUARY - 2017</span>
										<span class="e-blog-post-meta-tags">
											<i class="icofont icofont-tag"></i>
											<a href="#">Business</a>,
											<a href="#">Design</a>,
											<a href="#">social</a>
										</span>
									</div><!-- blog-post-meta -->
									<div class="e-blog-post-content gardient-end">
										<p>Here are many variations of passages of Lorem Ipsum available, but the majority have suffered in some form by injected humour</p>
									</div><!-- blog-post-content -->
									<a class="e-blog-post-more" href="blogs.php">READ MORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</div><!-- blog-post-des -->
							</div><!-- blog-post-inner -->
						</article><!-- blog-post -->
					</div>
					<div class="col-md-4 col-sm-6">
						<article class="e-blog-post card-layout bg-white">
							<div class="e-blog-post-inner">
								<div class="e-blog-post-thumb">
									<a href="#">
										<img src="images/posts/post-6.jpg" alt="">
									</a>
								</div><!-- blog-post-thumb -->
								<div class="e-blog-post-des">
									<h3 class="e-blog-post-title"><a href="#">Exclusive Blog Post</a></h3>
									<div class="e-blog-post-meta">
										<span class="e-blog-post-meta-author">POSTED BY: <a href="#">ADMIN</a></span>
										<span class="e-blog-post-meta-time">26 JANUARY - 2017</span>
										<span class="e-blog-post-meta-tags">
											<i class="icofont icofont-tag"></i>
											<a href="#">Business</a>,
											<a href="#">Design</a>,
											<a href="#">social</a>
										</span>
									</div><!-- blog-post-meta -->
									<div class="e-blog-post-content gardient-end">
										<p>Here are many variations of passages of Lorem Ipsum available, but the majority have suffered in some form by injected humour</p>
									</div><!-- blog-post-content -->
									<a class="e-blog-post-more" href="blogs.php">READ MORE <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
								</div><!-- blog-post-des -->
							</div><!-- blog-post-inner -->
						</article><!-- blog-post -->
					</div>
				</div>
			</div>
		</div>
	</section>
<!-- social blog -->

<section>
	<div class="sp-social-counter">
		<div class="container">
			<div class="row">
				<div class="m-left m-right column-align-middle">
					<div class="col-md-5 col-sm-5">
						<ul class="social-profile sp-lg-social">
			                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
			                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
			                <li><a href="#"><i class="fa fa-behance"></i></a></li>
			                <li><a href="#"><i class="fa fa-dribbble"></i></span></a></li>
			                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
			            </ul>
					</div>
					<div class="col-md-7 col-sm-7">
						<div class="m-left m-right counter-wrapper column-align-middle sp-counter">
							<div class="col-md-7 col-sm-6">
								<div class="counter-item full-width float-left active" data-aos="zoom-out-down">
									<span class="counter">350</span>
									<h4>satisfied clients</h4>
								</div>
							</div>
							<div class="col-md-5 col-sm-6">
								<div class="counter-item full-width float-right" data-aos="zoom-out-down">
									<span class="counter">865</span>
									<h4>Projects completed</h4>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- keep in touch -->
<section class="contact top-padding-80 bg-sky-blue" id="contact">
		<div class="contact-box-area">
			<div class="container">
				<div class="row">
					<div class="section-title">
						<h2>Keep In Touch</h2>
						<p>There are many variations of passages of Lorem Ipsum available but there<br> alteration in some form by injected humoures</p>
					</div><!-- section-title -->
					<div class="contact-box-wrapper m-left m-right">
						<div class="col-md-4 col-sm-4">
							<div class="contact-box boxed">
								<h3 class="title">Email :</h3>
								<span>omendrapratap25@gmail.com</span>
								<span>skillscandy@gmail.com</span>
							</div><!-- contact-box -->
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="contact-box boxed">
								<h3 class="title">PHONE :</h3>
								<span>+919454332797 </span>
								<span>+917017734526</span>
							</div><!-- contact-box -->
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="contact-box boxed">
								<h3 class="title">ADDRESS :</h3>
								<span>B Block, sector 62 NOIDA </span>
								<span>INDIA</span>
							</div><!-- contact-box -->
						</div>
					</div>
				</div>
			</div>
		</div><!-- contact-box-area -->
		<div class="contact-form-area s-padding bg-white">
			<div class="container">
				<div class="contact-form-wrapper m-right m-left">
                 

                   <form action="" method="post" id="contactform">
    							<div class="row">
    								
  									<!-- <div class="col-sm-12">
										<h3 style="color: #326588">Book An Appoinment</h3>
  											
  									</div> -->
  									<div class="col-sm-6">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa fa-yellow" aria-hidden="true"></i></span>
									<input type="text" class="" name="fname" placeholder="First Name" required >
								</div>
    									
    								</div>
    								<div class="col-sm-6">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa fa-yellow" aria-hidden="true"></i></span>
									<input type="text" class="" name="lname" placeholder="Last Name" required >
								</div>
    									
    								</div>
    								<div class="col-sm-6">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-phone fa fa-yellow" aria-hidden="true"></i></span>
									<input type="text" class="" name="phone" placeholder="Phone" required >
								</div>
    									
    								</div>
    								<div class="col-sm-6">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa  fa-envelope-o fa fa-yellow" aria-hidden="true"></i></span>
									<input type="email" class="" name="email" placeholder="email" required >
								</div>
    									
    								</div>
    								
    								<div class="col-sm-12">

    									<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-comments fa fa-yellow" aria-hidden="true"></i></span>
									<textarea row="2" id="subject" name="subject" placeholder="Write something.." style="margin: 0;color: #000" required></textarea>
								</div>
    									
    								</div>
    								
    								<div class="col-sm-12" style="margin-top: 20px;">
									<div id="contactMSG"></div>
										<button style="" class="btn btn1">Submit</button>
										<a href="contactus.php" class="sp-btn">Read More</a>
    								</div>
    								
    							</div>
	
  							</form>
               </div>
			</div>
		</div><!-- contact-form-area -->
		<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14011.171323662391!2d77.0473905!3d28.6059912!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc15d793aacb72697!2sDelhi+Public+School!5e0!3m2!1sen!2sin!4v1523018963347" width="100%" height="430" frameborder="0" style="border:0" allowfullscreen></iframe>
	</section>


	
		
	</section>

	<?php include("footer.php"); ?>
	
<a href="#" class="scroll-top"><i class="fa fa-arrow-up"></i></a>
	<script src="js/jquery-2.2.3.min.js">
	</script>
	<script src="js/bootstrap.min.js">
	</script> 
	<script src="js/waypoints.min.js">
	</script>
	<script src="js/jquery.easing.1.3.js">
	</script> 
	<script src="js/validator.min.js">
	</script>
	<script src="js/owl.carousel.js">
	</script> 
	<script src="js/owl.carousel2.thumbs.js">
	</script>
	<script src="js/jquery.nav.js">
	</script> 
	<script src="js/jquery.stellar.min.js">
	</script>
	<script src="js/wow.min.js">
	</script> 
	<script src="js/lightcase.js">
	</script>
	<script src="js/scrolloverflow.min.js">
	</script>
	<script src="js/smooth-scroll.min.js">
	</script>
	<script src="js/jquery.fullpage.min.js">
	</script> 
	<script src="js/jquery.events.touch.js">
	</script>
	<script src="js/jquery.infinitescroll.min.js">
	</script> 
	<script src="js/jquery.lazyload.min.js">
	</script>
	<script src="../assets/js/swiper.min.js">
	</script> 
	<script src="js/parallax.min.js">
	</script>
	<script src="js/masonry.pkgd.min.js">
	</script> 
	<script src="js/shuffle.min.js">
	</script>
	<script src="js/animsition.min.js">
	</script> 
	<script src="js/swiper.min.js">
	</script>
	<script src="js/jquery.nstSlider.js">
	</script> 
	<script src="js/jquery.countdown.min.js">
	</script>
	<script src="js/jquery.counterup.min.js">
	</script> 
	<script src="js/bootsnav.js">
	</script>
/*<script src="js/tilt.jquery.min.js">
	</script> 
	<script src="js/custom.js">
	</script>
	<script type="text/javascript">
	var infinitymanualmesonary=$(".infinityselctor-manaul-mesonary");infinitymanualmesonary.infinitescroll({navSelector:".infinity-manaul-links",nextSelector:".infinity-manaul-links a:first",itemSelector:".infinity-item-manaul",loading:{msgText:"Loading more posts...",finishedMsg:"Sorry, no more posts.",},errorCallback:function(){$(".post-load").css("display","none")}},function(a){var b=$(a).css("opacity",0);b.imagesLoaded(function(){b.animate({opacity:1});infinitymanualmesonary.masonry("appended",b,true)})});$(window).unbind(".infscr");$(".post-load").click(function(){infinitymanualmesonary.infinitescroll("retrieve");return false});
	</script> 
	<script type="text/javascript">
	$(".js-tilt").tilt({maxTilt:20,perspective:1000,easing:"cubic-bezier(.03,.98,.52,.99)",scale:1,speed:300,transition:true,axis:null,reset:true,glare:false,maxGlare:1,});
	</script>


	<script>
		

    $(document).ready(function() {
     
      $("#owl-demo").owlCarousel({
     
          autoPlay: 3000, //Set AutoPlay to 3 seconds
     
          items : 4,
          itemsDesktop : [1199,3],
          itemsDesktopSmall : [979,3]
     
      });
     
    });


	</script>
</body>
</html>