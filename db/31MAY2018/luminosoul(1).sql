-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 31, 2018 at 07:33 PM
-- Server version: 5.6.34-log
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `luminosoul`
--

-- --------------------------------------------------------

--
-- Table structure for table `icontact`
--

CREATE TABLE IF NOT EXISTS `icontact` (
  `ID` int(11) NOT NULL,
  `fname` mediumtext,
  `lname` mediumtext,
  `email` mediumtext,
  `phone` mediumtext,
  `msg` mediumtext,
  `type` mediumtext,
  `dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `icontact`
--

INSERT INTO `icontact` (`ID`, `fname`, `lname`, `email`, `phone`, `msg`, `type`, `dt`) VALUES
(6, 'varsha', 'kumari', 'varsha@gmail.com', '7017451093', 'this is the query  submitted by me.', 'QUERY', '2018-05-21 14:43:50'),
(7, 'yogendra', 'verma', 'yogendraverma325@gmail.com', '7017734526', 'this is the custom message', 'QUERY', '2018-05-21 14:47:16'),
(8, 'yogendra', 'verma', 'yogendraverma325@gmail.coom', '7017734526', 'this is the message given to admin from the email', 'QUERY', '2018-05-21 14:51:04'),
(9, NULL, NULL, 'varsha@gmail.com', NULL, NULL, 'SUBSCRIPTION', '2018-05-21 15:08:34'),
(10, NULL, NULL, 'subscription@test1.com', NULL, NULL, 'SUBSCRIPTION', '2018-05-21 21:33:41'),
(12, NULL, NULL, 'yogendrav@infocular.com', NULL, NULL, 'SUBSCRIPTION', '2018-05-23 11:45:49');

-- --------------------------------------------------------

--
-- Table structure for table `iquestion`
--

CREATE TABLE IF NOT EXISTS `iquestion` (
  `ID` int(11) NOT NULL,
  `question` longtext NOT NULL,
  `type` mediumtext NOT NULL,
  `usr_ip` mediumtext NOT NULL,
  `usr_id` int(11) NOT NULL,
  `dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sts` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iquestion`
--

INSERT INTO `iquestion` (`ID`, `question`, `type`, `usr_ip`, `usr_id`, `dt`, `sts`) VALUES
(1, 'What are the qualifications of the teachers?', 'ADMISSION', '127.0.0.1', 1, '2018-05-24 17:55:11', 1),
(2, 'How much experience do they have?', 'ADMISSION', '127.0.0.1', 1, '2018-05-24 17:55:27', 1),
(3, 'Do they get refresher training every year?', 'ADMISSION', '127.0.0.1', 1, '2018-05-24 17:55:45', 1),
(4, 'Does learning happen outside the classroom? ', 'ADMISSION', '127.0.0.1', 1, '2018-05-24 17:55:56', 1),
(5, 'What is the breakup of learning within the classroom and outside?', 'ADMISSION', '127.0.0.1', 1, '2018-05-24 17:56:06', 1),
(10, 'this is question for admission', 'ADMISSION', '127.0.0.1', 38, '2018-05-30 23:40:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `iuser`
--

CREATE TABLE IF NOT EXISTS `iuser` (
  `u_id` bigint(20) NOT NULL,
  `usr_nme` text,
  `usr_pass` text NOT NULL,
  `usr_email` text NOT NULL,
  `usr_ph` varchar(20) NOT NULL,
  `usr_img` text,
  `usr_role` enum('ADMIN','CLIENT','NOT-DEFINED') DEFAULT 'NOT-DEFINED',
  `usr_sts` varchar(5) NOT NULL,
  `usr_ver` varchar(25) NOT NULL,
  `usr_ip` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `iuser`
--

INSERT INTO `iuser` (`u_id`, `usr_nme`, `usr_pass`, `usr_email`, `usr_ph`, `usr_img`, `usr_role`, `usr_sts`, `usr_ver`, `usr_ip`) VALUES
(1, 'admin', '650d8c9c89896c6372aff467a0335d2fb3c4273618521b2fa7ce2554145c64b609a60ee4919a2c74c81a4c5f0cdd21bf0d6855a1fe1b01047f9a7a554801ea9c', 'admin@luminosoul.com', '7017734528', '3fd9fd137cb62a734a619e9bc38840582017AugTue06595915040079998f14e45fceea167a5a36dedd4bea25431ad23afbab38c0cf22cec1e2c3a7e1daJellyfish.jpg', 'ADMIN', '1', '1', '192.168.1.1'),
(15, 'varha1', 'varsha1@1234', 'varsha1@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(16, 'varha2', 'varsha2@1234', 'varsha2@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(17, 'varha3', 'varsha3@1234', 'varsha3@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(18, 'varha4', 'varsha4@1234', 'varsha4@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(19, 'varha5', 'varsha5@1234', 'varsha5@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(20, 'varha6', 'varsha6@1234', 'varsha6@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(21, 'varha7', 'varsha7@1234', 'varsha7@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(22, 'sameena1', 'sameena1@1234', 'sameena1@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(23, 'sameena2', 'sameena2@1234', 'sameena2@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(24, 'sameena3', 'sameena3@1234', 'sameena3@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(25, 'sameena4', 'sameena4@1234', 'sameena4@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(26, 'sameena5', 'sameena5@1234', 'sameena5@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(27, 'sameena6', 'sameena6@1234', 'sameena6@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(28, 'sameena7', 'sameena7@1234', 'sameena7@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(29, 'sameena8', 'sameena8@1234', 'sameena8@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(30, 'sameena9', 'sameena9@1234', 'sameena9@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(31, 'sameena10', 'sameena10@1234', 'sameena10@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(32, 'sameena11', 'sameena11@1234', 'sameena11@gmail.com', '123465789', NULL, 'CLIENT', '0', '1', '127.0.0.1'),
(38, 'yogendra325', '650d8c9c89896c6372aff467a0335d2fb3c4273618521b2fa7ce2554145c64b609a60ee4919a2c74c81a4c5f0cdd21bf0d6855a1fe1b01047f9a7a554801ea9c', 'yogendraverma325@gmail.com', '7017734526', NULL, 'CLIENT', '1', '1', '106.202.53.224');

-- --------------------------------------------------------

--
-- Table structure for table `mail_sts`
--

CREATE TABLE IF NOT EXISTS `mail_sts` (
  `ID` int(11) NOT NULL,
  `mail` mediumtext NOT NULL,
  `sts` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `school_details`
--

CREATE TABLE IF NOT EXISTS `school_details` (
  `ID` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `name` mediumtext,
  `about` longtext,
  `type` mediumtext,
  `area` mediumtext,
  `bedroom` mediumtext,
  `food` mediumtext,
  `rent_pyament` mediumtext,
  `available_for` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school_details`
--

INSERT INTO `school_details` (`ID`, `usr_id`, `name`, `about`, `type`, `area`, `bedroom`, `food`, `rent_pyament`, `available_for`) VALUES
(1, 37, 'YOGI PUBLIC SCHOOL', 'this is school made from stone and bricks', 'Girls', '50 sq feets', '5', 'yes', '5 ', 'all'),
(2, 38, 'infocular', 'this is school ', 'girls', '5000', '38', 'yes', 'rent', 'For All');

-- --------------------------------------------------------

--
-- Table structure for table `usr_act`
--

CREATE TABLE IF NOT EXISTS `usr_act` (
  `ID` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `usr_email` mediumtext NOT NULL,
  `usr_mail_code` mediumtext NOT NULL,
  `usr_ph` mediumtext NOT NULL,
  `usr_ph_code` mediumtext NOT NULL,
  `usr_reg_dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usr_act`
--

INSERT INTO `usr_act` (`ID`, `u_id`, `usr_email`, `usr_mail_code`, `usr_ph`, `usr_ph_code`, `usr_reg_dt`) VALUES
(12, 38, 'yogendraverma325@gmail.com', '7328', '7017734526', '6651', '2018-05-23 12:28:58');

-- --------------------------------------------------------

--
-- Table structure for table `usr_album`
--

CREATE TABLE IF NOT EXISTS `usr_album` (
  `ID` int(11) NOT NULL,
  `alm_nme` mediumtext NOT NULL,
  `usr_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usr_album`
--

INSERT INTO `usr_album` (`ID`, `alm_nme`, `usr_id`) VALUES
(1, 'PERSONAL', 1),
(2, 'HOME', 38),
(3, 'OFFICE', 38),
(4, 'TRIP', 38);

-- --------------------------------------------------------

--
-- Table structure for table `usr_info`
--

CREATE TABLE IF NOT EXISTS `usr_info` (
  `ID` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `usr_f_nme` mediumtext,
  `usr_m_nme` mediumtext,
  `usr_l_nme` mediumtext,
  `usr_gender` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usr_info`
--

INSERT INTO `usr_info` (`ID`, `u_id`, `usr_f_nme`, `usr_m_nme`, `usr_l_nme`, `usr_gender`) VALUES
(6, 14, 'null', 'null', 'null', 'null'),
(7, 15, 'varsha1 fname', 'varsha1 m name', 'varsha1 lname', 'Female'),
(8, 16, 'varsha2 fname', 'varsha2 m name', 'varsha2 lname', 'Female'),
(9, 17, 'varsha3 fname', 'varsha3 m name', 'varsha3 lname', 'Female'),
(10, 18, 'varsha4 fname', 'varsha4 m name', 'varsha4 lname', 'Female'),
(11, 19, 'varsha5 fname', 'varsha5 m name', 'varsha5 lname', 'Female'),
(12, 20, 'varsha6 fname', 'varsha6 m name', 'varsha6 lname', 'Female'),
(13, 21, 'varsha7 fname', 'varsha7 m name', 'varsha7 lname', 'Female'),
(14, 22, 'sameena1 f name', 'sameena1 m name', 'sameena1 l name', 'Female'),
(15, 23, 'sameena2 f name', 'sameena2 m name', 'sameena2 l name', 'Female'),
(16, 24, 'sameena3 f name', 'sameena3 m name', 'sameena1 3 name', 'Female'),
(17, 25, 'sameena4 f name', 'sameena4 m name', 'sameena4 l name', 'Female'),
(18, 26, 'sameena5 f name', 'sameena5 m name', 'sameena5 l name', 'Female'),
(19, 27, 'sameena6 f name', 'sameena7 m name', 'sameena7 l name', 'Female'),
(20, 28, 'sameena7 f name', 'sameena7 m name', 'sameena7 l name', 'Female'),
(21, 29, 'sameena8 f name', 'sameena8 m name', 'sameena8 l name', 'Female'),
(22, 30, 'sameena9 f name', 'sameena9 m name', 'sameena9 l name', 'Female'),
(23, 31, 'sameena10 f name', 'sameena10 m name', 'sameena10 l name', 'Female'),
(24, 32, 'sameena11 f name', 'sameena11 m name', 'sameena11 l name', 'Female'),
(30, 38, 'null', 'null', 'null', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `usr_post`
--

CREATE TABLE IF NOT EXISTS `usr_post` (
  `ID` int(11) NOT NULL,
  `usr_id` int(11) DEFAULT NULL,
  `POST_TEXT` longtext NOT NULL,
  `POST_VIDEO` longtext NOT NULL,
  `POST_DATE` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usr_post`
--

INSERT INTO `usr_post` (`ID`, `usr_id`, `POST_TEXT`, `POST_VIDEO`, `POST_DATE`) VALUES
(15, 38, 'this is a timeline', 'http://localhost/luminosoul/schoolPage2.php', '2018-05-31 10:20:53'),
(16, 38, 'Here are many variations of passages often Lorem Ipsum available but majoritie have suffer from ino some.', 'http://localhost/luminosoul/schoolPage2.php', '2018-05-31 10:21:33'),
(17, 38, 'It''s not the face, but the expressions on it. It''s not the voice, but what you say. It''s not how you look in that body, but the thing you do with it. You are beautiful.- Stephenie Meyer, The Host  Read more: http://latestsms.in/thoughts-on-life.htm#ixzz5H6nyP3D5', '', '2018-05-31 12:20:22'),
(18, 38, 'The parameter in the query is escaped and wrapped with single quotes, other than using BIG-5 encoding or anything else obscure wouldn''t make it vulnerable against an injection, would it? ', '', '2018-05-31 12:24:36');

-- --------------------------------------------------------

--
-- Table structure for table `usr_question`
--

CREATE TABLE IF NOT EXISTS `usr_question` (
  `ID` int(11) NOT NULL,
  `q_id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usr_question`
--

INSERT INTO `usr_question` (`ID`, `q_id`, `usr_id`) VALUES
(22, 1, 38),
(23, 2, 38),
(24, 3, 38),
(25, 4, 38),
(26, 5, 38);

-- --------------------------------------------------------

--
-- Table structure for table `usr_upload`
--

CREATE TABLE IF NOT EXISTS `usr_upload` (
  `ID` int(11) NOT NULL,
  `usr_id` int(11) DEFAULT NULL,
  `file_nme` mediumtext,
  `file_type` tinytext,
  `album` int(11) DEFAULT NULL,
  `up_for` mediumtext
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usr_upload`
--

INSERT INTO `usr_upload` (`ID`, `usr_id`, `file_nme`, `file_type`, `album`, `up_for`) VALUES
(24, 38, 'o 007.jpg', 'IMAGE', 3, NULL),
(25, 38, 'o 005 - Copy.jpg', 'IMAGE', 3, NULL),
(26, 38, 'o 005.jpg', 'IMAGE', 3, NULL),
(27, 38, 'hanuman jayanti wishes wallpaper.jpg', 'IMAGE', 4, NULL),
(42, 38, 'th7.jpg', 'IMAGE', 15, 'POST'),
(43, 38, 'th8.jpg', 'IMAGE', 15, 'POST'),
(44, 38, 'th9.jpg', 'IMAGE', 1, 'POST'),
(45, 38, 'th4.jpg', 'IMAGE', 16, 'POST'),
(46, 38, 'th5.jpg', 'IMAGE', 16, 'POST'),
(47, 38, 'th6.jpg', 'IMAGE', 16, 'POST'),
(48, 38, 'th10.jpg', 'IMAGE', 16, 'POST'),
(49, 38, 'th2.jpg', 'IMAGE', 17, 'POST');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `icontact`
--
ALTER TABLE `icontact`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `iquestion`
--
ALTER TABLE `iquestion`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `iuser`
--
ALTER TABLE `iuser`
  ADD PRIMARY KEY (`u_id`),
  ADD KEY `u_id` (`u_id`);

--
-- Indexes for table `mail_sts`
--
ALTER TABLE `mail_sts`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `school_details`
--
ALTER TABLE `school_details`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `usr_act`
--
ALTER TABLE `usr_act`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `usr_album`
--
ALTER TABLE `usr_album`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `usr_info`
--
ALTER TABLE `usr_info`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `u_id` (`u_id`);

--
-- Indexes for table `usr_post`
--
ALTER TABLE `usr_post`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `usr_question`
--
ALTER TABLE `usr_question`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `usr_upload`
--
ALTER TABLE `usr_upload`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `icontact`
--
ALTER TABLE `icontact`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `iquestion`
--
ALTER TABLE `iquestion`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `iuser`
--
ALTER TABLE `iuser`
  MODIFY `u_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `mail_sts`
--
ALTER TABLE `mail_sts`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `school_details`
--
ALTER TABLE `school_details`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `usr_act`
--
ALTER TABLE `usr_act`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `usr_album`
--
ALTER TABLE `usr_album`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `usr_info`
--
ALTER TABLE `usr_info`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `usr_post`
--
ALTER TABLE `usr_post`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `usr_question`
--
ALTER TABLE `usr_question`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `usr_upload`
--
ALTER TABLE `usr_upload`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
