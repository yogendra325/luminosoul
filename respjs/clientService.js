if (localStorage.ID==undefined || localStorage.ID=="null") { 
    alert("Login First To Access This");
    window.location.href = 'index.php';
    
    
   } 
   ///////////////////////////////

function getAdmissionQuestion()
{
          var url = "resp/clientService.php/getAdmissionQuestion"; // the script where you handle the form input.
          var dataPost = { "USR_ID": localStorage.ID};
          var dataString = JSON.stringify(dataPost);
    $.ajax({
         type: "POST",
         url: url,
       data:  dataString, 
       datatype: 'JSON',
         success: function(data)
         {

       var JSON_DATA = JSON.parse(data);
          if(JSON_DATA.ERROR=="YES")
        { document.getElementById("questions_row").innerHTML = "No  Question Found";
        }
        else
        {
            document.getElementById("questions_row").innerHTML =JSON_DATA.DATA;
        }
        
         }
       });
        
      
}
/////////////////////////////////////////////
////////////////////////////////////////////
function removeadmissionQuestion(ID)
{


var url = "resp/clientService.php/removeAdmissionQuestion"; // the script where you handle the form input.
var dataPost = { "USR_ID": localStorage.ID, "Q_ID":ID};
var dataString = JSON.stringify(dataPost);
$.ajax({
type: "POST",
url: url,
data:  dataString, 
datatype: 'JSON',
success: function(data)
{

var JSON_DATA = JSON.parse(data);
if(JSON_DATA.ERROR=="YES"){ 
}
else{
  $( "#admissionQuestion"+ID ).fadeOut( "slow" ); 
}

}
});
}
/////////////////////////////////////////////////
//////////////////////////////////////////////////
function getAllAdmissionQuestion()
{
    var url = "resp/clientService.php/getAllAdmissionQuestion"; // the script where you handle the form input.
var dataPost = { "USR_ID": localStorage.ID};
var dataString = JSON.stringify(dataPost);
$.ajax({
type: "POST",
url: url,
data:  dataString, 
datatype: 'JSON',
success: function(data)
{

    var JSON_DATA = JSON.parse(data);
    if(JSON_DATA.ERROR=="YES")
  { document.getElementById("allAdmissionQuestionList").innerHTML = "No  Question Found";
  }
  else
  {
      document.getElementById("allAdmissionQuestionList").innerHTML =JSON_DATA.DATA;
  }

}
});
}
/////////////////////////////////////////////////////
/////////////////////////////////////////////////////
function addThisToForm(ID)
{
var url = "resp/clientService.php/addThisQuestionToMyFOrm"; // the script where you handle the form input.
var dataPost = { "USR_ID": localStorage.ID, "Q_ID":ID};
var dataString = JSON.stringify(dataPost);
$.ajax({
type: "POST",
url: url,
data:  dataString, 
datatype: 'JSON',
success: function(data)
{

var JSON_DATA = JSON.parse(data);
if(JSON_DATA.ERROR=="NONE"){ 
    getAdmissionQuestion();
$( "#admissionQuestionList"+ID ).fadeOut( "slow" ); 
}


}
});
}

/////////////////////////////////////////////////////
/////////////////////////////////////////////////////

$(document).ready(function(){
$("#questionForm").submit(function(e) {

    var url = "resp/clientService.php/addThisQuestionToDatabase"; // the script where you handle the form input.
    var dataPost = { "USR_ID": localStorage.ID, "QUESTION": $("#question").val() };
    var dataString = JSON.stringify(dataPost);
    $.ajax({
    type: "POST",
    url: url,
    data:  dataString, 
    datatype: 'JSON',
    success: function(data)
    {
    
        var JSON_DATA = JSON.parse(data);
        document.getElementById("QUESTION_ADD_MSG").innerHTML = JSON_DATA.MSG;
           if(JSON_DATA.ERROR=="NONE")
         {
            setTimeout(function () {
                hideDiv("QUESTION_ADD_MSG");
                }, 3000);
                $("#questionForm").trigger('reset');
         } 
    
    }
    });
    
    e.preventDefault(); // avoid to execute the actual submit of the form.
    });
});
//////////////////////////////////////////////
/////////////////////////////////////////////
function getSchoolDetails () {

        var url = "resp/clientService.php/getSchoolDetails"; // the script where you handle the form input.
        var dataPost = { "USR_ID": localStorage.ID};
        var dataString = JSON.stringify(dataPost);
        $.ajax({
        type: "POST",
        url: url,
        data:  dataString, 
        datatype: 'JSON',
        success: function(data)
        {

        var JSON_DATA = JSON.parse(data);
        document.getElementById("school_details").innerHTML = JSON_DATA.DATA;

        }
        });
  }
  //////////////////////////////////////////////
/////////////////////////////////////////////
function editSchoolDetails () {

    var url = "resp/clientService.php/editSchoolDetails"; // the script where you handle the form input.
    var dataPost = { "USR_ID": localStorage.ID};
    var dataString = JSON.stringify(dataPost);
    $.ajax({
    type: "POST",
    url: url,
    data:  dataString, 
    datatype: 'JSON',
    success: function(data)
    {

    var JSON_DATA = JSON.parse(data);
    document.getElementById("school_details").innerHTML = JSON_DATA.DATA;

    }
    });
}
//////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

    function saveSchoolDetails()  {
        var url = "resp/clientService.php/saveSchoolDetail"; // the script where you handle the form input.
        var dataPost = { "USR_ID": localStorage.ID, 
                         "ABOUT": $("#s_about").val(),
                         "NAME": $("#s_name").val(),
                         "TYPE": $("#s_type").val(),
                         "AREA": $("#s_area").val(),
                         "FOOD": $("#s_food").val(),
                         "RENT": $("#s_rent").val(),
                         "FOR": $("#s_for").val()
                         };
        var dataString = JSON.stringify(dataPost);
        $.ajax({
        type: "POST",
        url: url,
        data: dataString,
        datatype: 'JSON',
        success: function(data)
        {
        var JSON_DATA = JSON.parse(data);
               if(JSON_DATA.ERROR=="NONE")
             {
                getSchoolDetails();
             } 
        
        }
        });
        
    }

    /////////////////////////////////////////////////////
/////////////////////////////////////////////////////
    $(document).ready(function (e) {
        $("#albumForm").on('submit',(function(e) {
        e.preventDefault();

        var url = "resp/clientService.php/addMedia"; // the script where you handle the form input.
        var formData=new FormData(this);
        
        formData.append("USR_ID", localStorage.ID);
        formData.append("ALBUM", $("#allAlbumList").val());
        $("#message").empty();
        $("#upload_button").prop("disabled", true);
        $.ajax({
        url: url, // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,        // To send DOMDocument or non processed data file it is set to false
        success: function(data)   // A function to be called if request succeeds
        {

                $("#message").html("Uploaded");
                $("#upload_button").prop("disabled", false);
                $("#albumForm").trigger('reset');
                setTimeout(function () {
                    hideDiv("message");
                    }, 3000);
        }
        });
        }));
    });
/////////////////////////////////////////
///////////////////////////////////
    $(document).ready(function (e) {
        $("#album_maker_form").on('submit',(function(e) {
        e.preventDefault();
        var url = "resp/clientService.php/addAlbum"; // the script where you handle the form input.
        var formData=new FormData(this);
       formData.append("USR_ID", localStorage.ID);
        // $("#ALBUM_ADD_BUTTON").prop("disabled", true);
        $.ajax({
        url: url, // Url to which the request is send
        type: "POST",             // Type of request to be send, called as method
        data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
        contentType: false,       // The content type used when sending data to the server.
        cache: false,             // To unable request pages to be cached
        processData:false,        // To send DOMDocument or non processed data file it is set to false
        success: function(data)   // A function to be called if request succeeds
        {
        
         var JSON_DATA = JSON.parse(data);
            $("#ALBUM_ADD_MSG").html(JSON_DATA.MSG);
               if(JSON_DATA.ERROR=="NONE")
             {
                
                setTimeout(function () {
                    hideDiv("ALBUM_ADD_MSG");
                    }, 3000);
                    $("#album_maker_form").trigger('reset');
                    getAlbum();
                
             } 
        
        }
        });
        }));
    });
//////////////////////////////////////////
/////////////////////////////////////////
function getAlbum()
{

    var url = "resp/clientService.php/getAlbum"; // the script where you handle the form input.
        var dataPost = { "USR_ID": localStorage.ID
                         };
        var dataString = JSON.stringify(dataPost);
        $.ajax({
        type: "POST",
        url: url,
        data: dataString,
        datatype: 'JSON',
        success: function(data)
        {
        var JSON_DATA = JSON.parse(data);
               
        $("#album_list").html(JSON_DATA.DATA);
        $("#view_album_list").html(JSON_DATA.DATA2);
        $("#Media_files").html(JSON_DATA.DATA3);
        
        
        }
        });

}
    

/////////////////////////////////
////////////////////////////////
function deleteMedia(ID,USR_ID)
{


    var url = "resp/clientService.php/deleteFile"; // the script where you handle the form input.
        var dataPost = { "USR_ID": USR_ID,
                            "ID":ID
                         };
        var dataString = JSON.stringify(dataPost);
        $.ajax({
        type: "POST",
        url: url,
        data: dataString,
        datatype: 'JSON',
        success: function(data)
        {
        var JSON_DATA = JSON.parse(data);
        if(JSON_DATA.ERROR=="NONE")
        {
          $("#AL"+ID).fadeOut( "slow" ); 
           
        }
        
        }
        });
}

/////////////////////////////////////////
///////////////////////////////////////////
function viewMoreAlbum()
{

    var url = "resp/clientService.php/viewMoreAlbum"; // the script where you handle the form input.
    var dataPost = { "USR_ID": localStorage.ID,
                      "ID" : $("#viewAlbumList").val()
                     };
    var dataString = JSON.stringify(dataPost);
    $.ajax({
    type: "POST",
    url: url,
    data: dataString,
    datatype: 'JSON',
    success: function(data)
    {
    var JSON_DATA = JSON.parse(data);
    if(JSON_DATA.ERROR=="NONE")
        {
            $("#Media_files").html(JSON_DATA.DATA);
           
        }
    
    
    }
    });
}

 /////////////////////////////////////////////////////
/////////////////////////////////////////////////////
$(document).ready(function (e) {
    $("#postform").on('submit',(function(e) {
    e.preventDefault();

    var url = "resp/clientService.php/makeAPost"; 
    var formData=new FormData(this);
    
    formData.append("USR_ID", localStorage.ID);

    // $("#message").empty();
    $("#POST_BUTTON").prop("disabled", true);
    $.ajax({
    url: url, // Url to which the request is send
    type: "POST",             // Type of request to be send, called as method
    data: formData, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
    contentType: false,       // The content type used when sending data to the server.
    cache: false,             // To unable request pages to be cached
    processData:false,        // To send DOMDocument or non processed data file it is set to false
    success: function(data)   // A function to be called if request succeeds
    {


        var JSON_DATA = JSON.parse(data);
            $("#POST_MSG").html(JSON_DATA.MSG);
            $("#POST_BUTTON").prop("disabled", false);
            $("#postform").trigger('reset');
            setTimeout(function () {
                hideDiv("POST_MSG");
                }, 3000);
    }
    });
    }));
});

/////////////////////////////////
////////////////////////////////
function getTimeline() {


    var url = "resp/clientService.php/getTimeline"; // the script where you handle the form input.
    var dataPost = { "USR_ID": localStorage.ID
                     };
    var dataString = JSON.stringify(dataPost);
    $.ajax({
    type: "POST",
    url: url,
    data: dataString,
    datatype: 'JSON',
    success: function(data)
    {
    var JSON_DATA = JSON.parse(data);
    if(JSON_DATA.ERROR=="NONE")
        {
            $("#timelineWall").html(JSON_DATA.DATA);
           
        }
    
    
    }
    });
}