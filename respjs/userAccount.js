


////////////////////////////////////////////
///////////////////////////////////////////

$(document).ready(function(){
    $("#signinform").submit(function(e) {

var url = "resp/userAccount.php/login"; // the script where you handle the form input.

$.ajax({
	   type: "POST",
	   url: url,
	   data: $("#signinform").serialize(), // serializes the form's elements.
	   success: function(data)
	   {
    var JSON_DATA = JSON.parse(data);
    document.getElementById("LOGIN_MSG").innerHTML = JSON_DATA.MSG;

    setTimeout(function () {
      hideDiv("LOGIN_MSG");
      }, 3000);
     if(JSON_DATA.ERROR=="NONE")
     {
        localStorage.ID = JSON_DATA.DATA.u_id;
        localStorage.USR_NAME=JSON_DATA.DATA.usr_nme;
        localStorage.ROLE=JSON_DATA.DATA.usr_role;
        localStorage.NAME = JSON_DATA.DATA.usr_nme;
        window.location.href = 'schoolPage.php';
     }  
	   }
	 });

e.preventDefault(); // avoid to execute the actual submit of the form.
});
});


////////////////////////////////////////////
///////////////////////////////////////////

  if(typeof(Storage) !== "undefined") {
      if (localStorage.ID) { 
      } else {
          localStorage.ID = "null";
          localStorage.USR_NAME="null";
          localStorage.ROLE="null";
          localStorage.NAME = "null";
      }
      
  } else {
     alert("your browser do not Java Script  Enable Before visiting website");
  }

  ////////////////////////////////////////////
///////////////////////////////////////////
  function forgetPassword() {
    
    
    if(document.getElementById("text").value=="") {
      document.getElementById("LOGIN_MSG").innerHTML = "Enter Your Email";
    }
    else{
        var url = "resp/userAccount.php/forgetPassword"; // the script where you handle the form input.
        var dataPost = { "email": document.getElementById("text").value };
        var dataString = JSON.stringify(dataPost);
  $.ajax({
	   type: "POST",
	   url: url,
     data: {myData: dataString} , 
     datatype: 'JSON',
	   success: function(data)
	   {
      var JSON_DATA = JSON.parse(data);
      document.getElementById("LOGIN_MSG").innerHTML = JSON_DATA.MSG;
	   }
	 });
      //document.getElementById("LOGIN_MSG").innerHTML = "You have An Email To Reset The Password Check It"; 
    }
    
}
////////////////////////////////////////////
///////////////////////////////////////////

$(document).ready(function(){
  
  $("#forgetPaswwordform").submit(function(e) {
var url = "resp/userAccount.php/resetPassword"; // the script where you handle the form input.

var dataPost = { "newPass": $("#newPass").val() ,"confPass":$("#confPass").val(),"USER_ID":$("#USER_ID").val() };
var dataString = JSON.stringify(dataPost);

$.ajax({
   type: "POST",
   url: url,
   data: {myData: dataString} , // serializes the form's elements.
   success: function(data)
   {
  var JSON_DATA = JSON.parse(data);
   if(JSON_DATA.ERROR=="NONE")
   {
      alert( JSON_DATA.MSG);
      $("#forgetPaswwordform").trigger('reset');
      window.location.href = 'index.php';
   }  
   }
 });

e.preventDefault(); // avoid to execute the actual submit of the form.
});
});


////////////////////////////////////////
////////////////////////////////////////
////////////////////////////////////////////
///////////////////////////////////////////

$(document).ready(function(){
  
  $("#registerationform").submit(function(e) {
var url = "resp/userAccount.php/register"; // the script where you handle the form input.

var dataPost = { "email": $("#email").val() ,"username":$("#username").val(),"phone":$("#phone").val(),"password":$("#Password").val() };
var dataString = JSON.stringify(dataPost);

$.ajax({
   type: "POST",
   url: url,
   data: {myData: dataString} , // serializes the form's elements.
   success: function(data)
   {
    
  var JSON_DATA = JSON.parse(data);
   if(JSON_DATA.ERROR=="NONE")
   {
      localStorage.VER_EMAIL =JSON_DATA.DATA.EMAIL;
      localStorage.VER_PHONE=JSON_DATA.DATA.PHONE;
    document.getElementById("registerationform").style.display = 'none';
    document.getElementById("otpVerificationform").style.display = 'block';
   
   }
   else{
  
    document.getElementById("REGISTER_MSG").innerHTML =JSON_DATA.MSG; 
    setTimeout(function () {
      hideDiv("REGISTER_MSG");
      }, 3000);
      
   }
   

   }
 });

e.preventDefault(); // avoid to execute the actual submit of the form.
});
});
//////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
function sendEmailOTPAgain ()
{

  var url = "resp/userAccount.php/sendEmailOTPAgain"; 
var dataPost = { "email": localStorage.VER_EMAIL };
  var dataString = JSON.stringify(dataPost);
  
  $.ajax({
     type: "POST",
     url: url,
     data: {myData: dataString} , // serializes the form's elements.
     success: function(data)
     {
       
    var JSON_DATA = JSON.parse(data);
    document.getElementById("email_OTP_MSG").innerHTML =JSON_DATA.MSG; 
    setTimeout(function () {
      hideDiv("email_OTP_MSG");
      }, 3000);
  
     }
   });
}

//////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////
function sendPhoneOTPAgain ()
{

  var url = "resp/userAccount.php/sendPhoneOTPAgain"; 
var dataPost = { "phone": localStorage.VER_PHONE };
  var dataString = JSON.stringify(dataPost);
  
  $.ajax({
     type: "POST",
     url: url,
     data: {myData: dataString} , // serializes the form's elements.
     success: function(data)
     {
       
    var JSON_DATA = JSON.parse(data);
    document.getElementById("phone_OTP_MSG").innerHTML =JSON_DATA.MSG;  
    setTimeout(function () {
      hideDiv("phone_OTP_MSG");
      }, 3000);
     
  
     }
   });
}


////////////////////////////////////////////
///////////////////////////////////////////

$(document).ready(function(){
  
  $("#otpVerificationform").submit(function(e) {
var url = "resp/userAccount.php/verification"; // the script where you handle the form input.

var dataPost = { "email": localStorage.VER_EMAIL ,"emailOTP":$("#email_OTP").val(),"phone":localStorage.VER_PHONE,"phoneOTP":$("#phone_OTP").val() };
var dataString = JSON.stringify(dataPost);

$.ajax({
   type: "POST",
   url: url,
   data: {myData: dataString} , // serializes the form's elements.
   success: function(data)
   {
     
    var JSON_DATA = JSON.parse(data);
    document.getElementById("OTP_MSG").innerHTML =JSON_DATA.MSG;
    setTimeout(function () {
    hideDiv("OTP_MSG");
    }, 3000);

  if(JSON_DATA.ERROR=="NONE") {
      $("#otpVerificationform").trigger('reset');
      setTimeout(function () {
        $("#VER_MODAL1").click();
        $("#VER_MODAL2").click();
        }, 3000);
      }


   
  }
 });

e.preventDefault(); // avoid to execute the actual submit of the form.
});
});